#include "Drop.h"
#include "Unit.h"

Nm2DSurface DropSurf1;
Nm2DSurface DropSurf2;
Nm2DSurface DropSurf3;
int DropInt1;
int DropInt2;
int DropInt3;
int DropF;
extern Nm2DSurface goblinMSurf;				//�S�u�������p�t
extern Nm2DSurface goblinSSurf;				//�S�u�������m
extern Nm2DSurface WolfSurf;					//�T
extern Nm2DSurface ShermanSurf;				//�V���[�}��
extern Nm2DSurface ZombieSurf;					//�]���r
extern Nm2DSurface SukeletonSurf;				//�X�P���g��
extern int monF[4][9];

void DropInit(){
	Nm2DInit(&DropSurf1);
	Nm2DInit(&DropSurf2);
	Nm2DInit(&DropSurf3);
	int DropInt1 = DropInt2 = DropInt3 = 0;
}
void DropUpdata(int DropF){
	switch (DropF)
	{
	case 1:
		DropSurf1 = WolfSurf;
		DropSurf2 = goblinMSurf;
		if (monF[1][4] != 1){
			monF[1][4] = 1;
		}
		if (monF[0][3] != 1){
			monF[0][3] = 1;
		}
		getUnitSave("res/UnitGetNow.dat");
		break;
	case 2:
		DropSurf1 = ShermanSurf;
		DropSurf2 = goblinSSurf;
		if (monF[3][3] != 1){
			monF[3][3] = 1;
		}
		if (monF[0][4] != 1){
			monF[0][4] = 1;
		}
		getUnitSave("res/UnitGetNow.dat");
		break;
	case 3:
		DropSurf1 = ZombieSurf;
		DropSurf2 = SukeletonSurf;
		if (monF[3][6] != 1){
			monF[3][6] = 1;
		}
		if (monF[2][7] != 1){
			monF[2][7] = 1;
		}
		getUnitSave("res/UnitGetNow.dat");
		break;
	}
}
void DropRender(int nextF){
	if (nextF == 0){
		Nm2DRender(&DropSurf1);
	}
	if (nextF == 1){
		Nm2DRender(&DropSurf2);
	}
}
void DropDelete(){
	Nm2DDeleteSurface(&DropSurf1);
	Nm2DDeleteSurface(&DropSurf2);
	Nm2DDeleteSurface(&DropSurf3);
}

