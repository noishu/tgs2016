/********************************************************/
/* DirectX  ゲームプログラミング						*/
/*						ゲームライブラリー				*/
/*														*/
/* NmGraphic.h - DirectXGraphics関連処理				*/
/*														*/
/*					沼津情報専門学校　Atsushi Ohba		*/
/*						Copyright (c) 2005-2005			*/
/*						All Rights Reserved.			*/
/********************************************************/
#ifndef __NMGRAPHIC_H_
#define __NMGRAPHIC_H_

//	ヘッダファイルのインクルード
#include <d3d9.h>								// DirectXGraphics ヘッダファイル
#include <d3dx9.h>								// D3DX ヘッダファイル


// 外部参照宣言

// ３Ｄデバイス取得
extern LPDIRECT3DDEVICE9  GetD3DDevice(void);

// ダイレクトグラフィックス初期化関数
extern BOOL GrpInit(HWND hwnd);
// ダイレクトグラフィックス開放関数
extern void GrpRelease(void);

// アルファブレンドの設定
extern void NmSetAlpha( float alpha );
extern void NmAddAlpha( float alpha );
extern float NmGetAlpha(  );



#endif

