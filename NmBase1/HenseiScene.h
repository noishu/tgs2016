// GameScene.cppファイル内の関数のうち、他のファイルから呼び出される関数のプロトタイプ宣言を記述する

BOOL initHenseiScene(void);
void moveHenseiScene(DWORD dwTickDiff);
void renderHenseiScene(void);
void releaseHenseiScene(void);
void HenseiSceneCollideCallback(int nSrc, int nTarget, int nCollideID);