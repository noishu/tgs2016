#include "NmBase1.h"
#include "GameMain.h"
#include "Game2Scene.h"
#include "Game3Scene.h"
#include "Unit.h"
#include "Player.h"
#include "Drop.h"

#define PlayerUnitCollid 100;

extern SCENE_NO menuB[1] = { SCENE_GAME2 };

int nowtime = 0;
int next = 0;
Nm2DSurface BackSurf;
Nm2DSurface WindowSurf;
Nm2DSurface StopSurf;
Nm2DSurface StartSurf;
Nm2DSurface ClearSurf;
Nm2DSurface OverSurf;
Nm2DSurface ExitSurf;

Nm2DSurface corasol3Surf;
extern player player1;
extern player enemy;
extern int UnitK[8];
extern unit units[MonsterMax];
extern unit UnitE[MonsterMax];
extern int playerHpMax;
extern int enemyHpMax;
extern int DropF;
NmMusic BattolMusic;
enum CollideKind{
	EnemyUnit=101,
	EnemyBace,
	PlayerUnit,
	PlayerBace
};

enum Gamemode{
	GamePlay = 151,
	GameStop,
	GameOver,
	GameClear,
	GameDrop
};

int gameFlag;

static NmFont font;
static char buf[128];

static NmFont font2;
static char buf2[128];
static char buf3[128];

// シーン開始前の初期化を行う
BOOL initGame3Scene(void){
	gameFlag = GamePlay;
	playerInit();
	UnitInit();
	enemyInit();
	NmFontInit(&font);
	NmFontCreate(&font,40,"");
	NmFontInit(&font2);
	NmFontCreate(&font2, 36, "");
	Nm2DInit(&BackSurf);
	Nm2DOpen(&BackSurf, "res/Back.png", 1280, 720, 1, 1);
	Nm2DInit(&WindowSurf);
	Nm2DOpen(&WindowSurf, "res/window.png", 1180, 620, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 100));
	Nm2DInit(&StopSurf);
	Nm2DOpen(&StopSurf, "res/stop.png", 100, 100, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 100));
	Nm2DInit(&StartSurf);
	Nm2DOpen(&StartSurf, "res/start.png", 100, 100, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 100));
	Nm2DInit(&ClearSurf);
	Nm2DOpen(&ClearSurf, "res/clear.png", 486, 93, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 100));
	Nm2DInit(&OverSurf);
	Nm2DOpen(&OverSurf, "res/over.png", 447, 93, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 100));
	Nm2DInit(&ExitSurf);
	Nm2DOpen(&ExitSurf, "res/Exit.png", 210, 100, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 100));
	NmMusicInit(&BattolMusic);
	NmMusicOpen(&BattolMusic, "res/MOUSIC/BATOL/game_maoudamashii_1_battle34.mid", 0);
	NmMusicPlay(&BattolMusic);

	Nm2DInit(&corasol3Surf);
	Nm2DOpen(&corasol3Surf, "res/crasol.png", 50, 50, D3DCOLOR_RGBA(0, 255, 0, 0));


	return TRUE;
}

//	フレーム処理
void moveGame3Scene(DWORD dwTickDiff){
	if (gameFlag == GamePlay){
		nowtime++;
		PlayerUpdata(nowtime);
		UnitUpdata(PlayerM);
		UnitMoveUpdata();
		PlayerSumon();
		enemyUpdata(nowtime);
		if(GetMouseButtonLWait()){
			if (GetMouseX() > 0 && GetMouseX()<0 + 100 &&
				GetMouseY()>0 && GetMouseY() < 0 + 100){
				gameFlag = GameStop;
			}
		}
		if (player1.PlayerHp <= 0){
			player1.PlayerHp = 0;
			gameFlag = GameOver;
		}
		if (enemy.PlayerHp <= 0){
			enemy.PlayerHp = 0;
			DropUpdata(DropF);
			gameFlag = GameClear;
		}
	}
	else if (gameFlag == GameStop){
		if (GetMouseButtonLWait()){
			if (GetMouseX() > 320 && GetMouseX()<320 + 100 &&
				GetMouseY()>310 && GetMouseY() < 310 + 100){
				gameFlag = GamePlay;
			}
		}
		if (GetMouseButtonLWait()){
			if (GetMouseX() > 860 && GetMouseX()<860 + 210 &&
				GetMouseY()>310 && GetMouseY() < 310 + 100){
				changeScene(menuB[SCENE_GAME1]);
				gameFlag = GamePlay;
			}
		}
	}
	else if (gameFlag == GameClear){
		if (GetMouseButtonLWait()){
			if (GetMouseX() > 620 && GetMouseX()<620 + 210 &&
				GetMouseY()>460 && GetMouseY() < 460 + 100){
				changeScene(menuB[SCENE_GAME1]);
				gameFlag = GamePlay;
			}
		}
	}
	else if (gameFlag == GameDrop){
		if (GetMouseButtonLWait()){
			next++;
			if (next>2){
				gameFlag = GameClear;
			}
		}
	}
	else if (gameFlag == GameOver){
		if (GetMouseButtonLWait()){
			if (GetMouseX() > 620 && GetMouseX()<620 + 210 &&
				GetMouseY()>460 && GetMouseY() < 460 + 100){
				changeScene(menuB[SCENE_GAME1]);
				gameFlag = GamePlay;
			}
		}
	}
	if (nowtime == 4001){
		nowtime = 1;
	}
	
}

//	レンダリング処理
void renderGame3Scene(void){
	Nm2DRender(&BackSurf);
	PlayerRenderUnder();
	enemyRender();
	UnitRender(PlayerM);
	PlayerRenderUp();
	sprintf_s(buf, "現在MP\n      %d", player1.PlayerMp);
	NmFontDrawText(&font, buf, 35, 565, D3DCOLOR_RGBA(0, 0, 0, 255));
	sprintf_s(buf2, "%d / %d", player1.PlayerHp, playerHpMax);
	NmFontDrawText(&font2, buf2, 1035, 180, D3DCOLOR_RGBA(0, 0, 0, 255));
	sprintf_s(buf3, "%d / %d", enemy.PlayerHp, enemyHpMax);
	NmFontDrawText(&font2, buf3, 75,180, D3DCOLOR_RGBA(0, 0, 0, 255));
	if (gameFlag == GamePlay){
		Nm2DSet(&StopSurf, 0, 0);
		Nm2DRender(&StopSurf);
	}
	if (gameFlag == GameStop){
		Nm2DSet(&WindowSurf, 50, 50);
		Nm2DRender(&WindowSurf);
		Nm2DSet(&StartSurf, 320, 310);
		Nm2DRender(&StartSurf);
		Nm2DSet(&ExitSurf, 860, 310);
		Nm2DRender(&ExitSurf);
	}
	if (gameFlag == GameClear){
		Nm2DSet(&WindowSurf, 50, 50);
		Nm2DRender(&WindowSurf);
		Nm2DSet(&ClearSurf, 450, 100);
		Nm2DRender(&ClearSurf);
		Nm2DSet(&ExitSurf, 560, 460);
		Nm2DRender(&ExitSurf);
	}
	if (gameFlag == GameDrop){
		Nm2DSet(&WindowSurf, 50, 50);
		Nm2DRender(&WindowSurf);
		Nm2DSet(&ClearSurf, 450, 100);
		Nm2DRender(&ClearSurf);
		DropRender(next);
	}
	if (gameFlag == GameOver){
		Nm2DSet(&WindowSurf, 50, 50);
		Nm2DRender(&WindowSurf);
		Nm2DSet(&OverSurf, 450, 100);
		Nm2DRender(&OverSurf);
		Nm2DSet(&ExitSurf, 560, 460);
		Nm2DRender(&ExitSurf);
	}

	Nm2DSet(&corasol3Surf, GetMouseX(), GetMouseY());
	Nm2DRender(&corasol3Surf);

	
}

//	シーン終了時の後処理
void releaseGame3Scene(void){
	Nm2DSetVisible(&BackSurf, false);
	Nm2DDeleteSurface(&BackSurf);
	Nm2DDeleteSurface(&WindowSurf);
	Nm2DDeleteSurface(&StopSurf);
	Nm2DDeleteSurface(&StartSurf);
	Nm2DDeleteSurface(&ClearSurf);
	Nm2DDeleteSurface(&OverSurf);
	Nm2DDeleteSurface(&ExitSurf);

	PlayerDelete();
	enemyDelete();
	UnitDelete();
	DropDelete();

	next = 0;

	Nm2DDeleteSurface(&corasol3Surf);
	NmMusicDelete(&BattolMusic);
}

// 当り判定コールバック 　　　ここでは要素を削除しないこと！！
void  Game3SceneCollideCallback( int nSrc, int nTarget, int nCollideID ){

}
