#ifndef _UNIT_H_
#define _UNIT_H_



#include "NmBase1.h"
#include "Player.h"
#include "Enemy.h"

//スライム
#define slimeHp 300
#define slimeMp 30
#define slimeSpeed 2
#define slimePower 50
#define slimeAction 10
//ゴブリン
#define goblinHp 450
#define goblinMp 50
#define goblinSpeed 2
#define goblinPower 75
#define goblinAction 15
//ゴブリン魔術師
#define GoblinMHp 400
#define GoblinMMp 60
#define GoblinMSpeed 1
#define GoblinMPower 60
#define GoblinMAction 15
//ゴブリン剣士
#define GoblinSHp 600
#define GoblinSMp 60
#define GoblinSSpeed 2
#define GoblinSPower 75
#define GoblinSAction 15
//ゴブリンライダー
#define GoblinRHp 500
#define GoblinRMp 60
#define GoblinRSpeed 3
#define GoblinRPower 60
#define GoblinRAction 15
//ゴーレム
#define golemHp 600
#define golemMp 60
#define golemSpeed 1
#define golemPower 30
#define golemAction 20
//フレアゴーレム
#define FGolemHp 600
#define FGolemMp 90
#define FGolemSpeed 1
#define FGolemPower 45
#define FGolemAction 20
//ドラゴン
#define dragonHp 1200
#define dragonMp 150
#define dragonSpeed 4
#define dragonPower 150
#define dragonAction 20
//レッサードラゴン
#define TDragonHp 900
#define TDragonMp 100
#define TDragonSpeed 4
#define TDragonPower 100
#define TDragonAction 20
//巨人
#define giantHp 500
#define giantMp 75
#define giantSpeed 2
#define giantPower 1000
#define giantAction 15
//狼
#define GWolfHp 300
#define GWolfMp 30
#define GWolfSpeed 3
#define GWolfPower 40
#define GWolfAction 10
//フレアホース
#define FHorseHp 400
#define FHorseMp 50
#define FHorseSpeed 3 
#define FHorsePower 60
#define FHorseAction 10
//オルトロス
#define OrthrosHp 400
#define OrthrosMp 50
#define OrthrosSpeed 3
#define OrthrosPower 60
#define OrthrosAction 10
//ケルベロス
#define CerberusHp 500
#define CerberusMp 60
#define CerberusSpeed 3
#define CerberusPower 75
#define CerberusAction 10
//べへモス
#define BehemothHp 500
#define BehemothMp 60
#define BehemothSpeed 4
#define BehemothPower 60
#define BehemothAction 10
//白虎
#define BaihuHp 600
#define BaihuMp 75
#define BaihuSpeed 4
#define BaihuPower 75
#define BaihuAction 10
//デーモン
#define DaemonHp 1000
#define DaemonMp 100
#define DaemonSpeed 2
#define DaemonPower 120
#define DaemonAction 20
//闇騎士
#define DarkKnightHp 700
#define DarkKnightMp 100
#define DarkKnightSpeed 1
#define DarkKnightPower 50
#define DarkKnightAction 20
//堕天使（魔術師）
#define FallenMHp 400
#define FallenMMp 80
#define FallenMSpeed 1
#define FallenMPower 60
#define FallenMAction 15
//堕天使（戦士）
#define FallenSHp 600
#define FallenSMp 80
#define FallenSSpeed 2
#define FallenSPower 60
#define FallenSAction 15
//スケルトン
#define SukeletonHp 500
#define SukeletonMp 60
#define SukeletonSpeed 4
#define SukeletonPower 50
#define SukeletonAction  15
//スケルトンスナイパー
#define SukeletonArrowHp 300
#define SukeletonArrowMp 60
#define SukeletonArrowSpeed 3
#define SukeletonArrowPower 40
#define SukeletonArrowAction 10
//スケルトンジェネラル
#define SukeletonGeneralHp 650
#define SukeletonGeneralMp 75
#define SukeletonGeneralSpeed 2
#define SukeletonGeneralPower 70
#define SukeletonGeneralAction 15
//シャーマン
#define ShermanHp 300
#define ShermanMp 75
#define ShermanSpeed 1
#define ShermanPower 75
#define ShermanAction 15
//ウィッチ
#define WitchHp 350
#define WitchMp 75
#define WitchSpeed 1
#define WitchPower 80
#define WitchAction 20
//ヴァンパイア
#define VampireHp 800
#define VampireMp 80
#define VampireSpeed 3
#define VampirePower 80
#define VampireAction 20
//ゾンビ
#define ZombieHp 300
#define ZombieMp 20
#define ZombieSpeed 2
#define ZombiePower 20
#define ZombieAction 10
//ゾンビソードマン
#define SZombieHp 350
#define SZombieMp 30
#define SZombieSpeed 2
#define SZombiePower 30
#define SZombieAction 10


#define NO_DATA_M 0

enum UNIT_WAY{
	UNIT_LEFT = 0,	//左向き
	UNIT_RIGHT		//右向き
};
enum UNIT_KIND{//ユニットの種類
	NoMonster=0,
	Slime,
	goblin,
	goblinM,
	goblinS,
	goblinR,
	golem,
	Fgolem,
	NO_mons,
	dragon,
	Tdragon,
	giant,
	wolf,
	FHorse,
	orthros,
	Cerberus,
	Nomons,
	behemoth,
	baihu,
	Daemon,
	DKnight,
	FallenM,
	FallenS,
	skeleton,
	NO_MONSU,
	skeletonArrow,
	skeletonGeneral,
	Sherman,
	witch,
	vampire,
	zombie,
	zombieS,
	monsClear,
	skeletonGeneral2
};
enum MilitaryKind{	//所属軍勢
	PlayerM = 0,
	EnemyM
};

typedef struct _UNIT_DATA{
	Nm2DSurface UnitSurf;
	int UnitKind;		//ユニットの種類
	int UnitM;			//ユニットの軍勢
	int UnitHp;			//ユニットのHP
	int UnitStepback;	//ユニットが下がる条件
	int UnitStepBack2;
	int UnitMp;			//ユニット召喚コスト
	int UnitSpeed;		//ユニットのスピード
	int UnitPower;		//ユニットの攻撃力
	int UnitWay;		//ユニットの進む向き
	int UnitX;			//ユニットのX座標
	int UnitY;			//ユニットのY座標
	int ActionWeit;		//ユニットの攻撃間隔
	int UnitMoveFlag;
	bool UnitExit;		//ユニットが生きているか
}unit;

typedef struct BattleCount{
	int ActionWeitP;
	int ActionWeitE;
}Count;

void UnitInit();
void UnitUpdata(int Pkind);
void UnitRender(int Pkind);
void UnitDelete();
//モンスタースポーン関数
void Unitspawn(int *unitcnt,int unitkind,int Pkind);
int UnitCollideChaeck(unit Punit,unit Eunit);
int UnitCollideChaeckBace(unit Punit);
int UnitMoveChaek(unit Unit);
void UnitAction(int *Unit1hp, int *Acnt,int UnitSb,int *UnitSbl,int *UnitX,unit Unit,unit Unit2);
void UnitActionBace(int *Bacehp, int *Acnt, unit Unit);
int UnitAliveChack(int UnitM);
void getUnitSave(char* _FilName);
void getUnitLoad(char* _FilName);
void UnitMoveUpdata();


#endif