#ifndef __MENBER_H
#define __MENBER_H
//マップチップの大きさ
#define TIP_SIZE 200
#define TIP_SIZE2 160

//ステージの大きさ
#define SCREEN_W 6
#define SCREEN_H 4
//マップの初期化
void MenberInit();
//マップデータにデータを設定
//データを取り出す
void MenbreSet(int mx, int my, int index);
//データを放り込む
void  MenberTipDraw(int x, int y, int index);
//マップデータのセーブとロード
void menberSave(char* _FileName);
void menberLoad(char* _FileName);

//メンバーの表示
void MenberRender();
//エディットメンバーの表示
void EditMenberRender();
//メンバーデータの取得
int GetMenberData(int mx, int my);
#endif