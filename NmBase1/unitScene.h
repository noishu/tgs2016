// GameScene.cppファイル内の関数のうち、他のファイルから呼び出される関数のプロトタイプ宣言を記述する

BOOL initUnitScene(void);
void moveUnitScene(DWORD dwTickDiff);
void renderUnitScene(void);
void releaseUnitScene(void);
void UnitSceneCollideCallback(int nSrc, int nTarget, int nCollideID);
