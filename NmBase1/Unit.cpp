
#include "Unit.h"
#include "menber.h"

Nm2DSurface SlimeSurf;					//スライム用画像変数
Nm2DSurface goblinSurf;					//ゴブリン用画像変数
Nm2DSurface goblinMSurf;				//ゴブリン魔術師
Nm2DSurface goblinSSurf;				//ゴブリン剣士
Nm2DSurface goblinRSurf;				//ゴブリンライダー
Nm2DSurface golemSurf;					//ゴーレム用画像変数
Nm2DSurface FGolemSurf;					//フレアゴーレム
Nm2DSurface dragonSurf;					//ドラゴン用画像変数
Nm2DSurface TDragonSurf;				//タイラントドラゴン
Nm2DSurface giantSurf;					//巨人
Nm2DSurface WolfSurf;					//狼
Nm2DSurface FHorseSurf;					//フレアホース
Nm2DSurface OrthrosSurf;				//オルトロス
Nm2DSurface CerberusSurf;				//ケルベロス
Nm2DSurface BehemothSurf;				//ベヒモス
Nm2DSurface BaihuSurf;					//白虎
Nm2DSurface DaemonSurf;					//デーモン
Nm2DSurface DKnightSurf;				//闇騎士
Nm2DSurface FallenMSurf;				//堕天使（魔術師）
Nm2DSurface FallenSSurf;				//堕天使（剣士）
Nm2DSurface ZombieSurf;					//ゾンビ
Nm2DSurface ZombieSSurf;				//ゾンビ剣士
Nm2DSurface SukeletonSurf;				//スケルトン
Nm2DSurface SukeletonArrowSurf;			//スケルトンスナイパー
Nm2DSurface SukeletonGeneralSurf;		//スケルトンジェネラル
Nm2DSurface ShermanSurf;				//シャーマン
Nm2DSurface WitchSurf;					//ウィッチ
Nm2DSurface VampireSurf;				//ヴァンパイア

Nm2DSurfaceArray MonstersFaceSurf;		//モンスター召喚時用顔グラフィック
extern player player1;					//
extern player enemy;					//
int enemyMaxNom;

unit units[MonsterMax];
unit UnitE[MonsterMax];
Count unitcount[MonsterMax];
int monF[4][9];
int i;
int j;
int Up;
bool Upn;
int Ue;
bool Uen;
int UCC;
float UpS;
float UeS;
int NoCollideFlag;
enum{
	no=0,
	ok
};

int UnitK[8];		//パーティ管理用変数（仮）

//ユニット準備
void UnitInit(){
	int i;
	int j;
	int GhGap;
	Nm2DInit(&SlimeSurf);
	Nm2DOpen(&SlimeSurf, "res/mob/slime2.png",
		32, 32, 2, 2, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&goblinSurf);
	Nm2DOpen(&goblinSurf, "res/mob/goblin2.png",
		42, 48, 2, 2, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&goblinMSurf);
	Nm2DOpen(&goblinMSurf, "res/mob/magicGoblin.png",
		50, 72, 2, 2, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&goblinSSurf);
	Nm2DOpen(&goblinSSurf, "res/mob/Sgoblin.png",
		58, 48, 2, 2, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&goblinRSurf);
	Nm2DOpen(&goblinRSurf, "res/mob/goblinrider2.png",
		66, 48, 2, 2, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&golemSurf);
	Nm2DOpen(&golemSurf, "res/mob/golem.png",
		74, 72, 2, 2, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&FGolemSurf);
	Nm2DOpen(&FGolemSurf, "res/mob/FGolem.png",
		66, 96, 2, 2, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&dragonSurf);
	Nm2DOpen(&dragonSurf, "res/mob/dragon2.png",
		194, 104, 2, 2, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&TDragonSurf);
	Nm2DOpen(&TDragonSurf, "res/mob/Lesserdragon2.png",
		114, 72, 2, 2, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&giantSurf);
	Nm2DOpen(&giantSurf, "res/mob/Giant2.png",
		82, 80, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&WolfSurf);
	Nm2DOpen(&WolfSurf, "res/mob/GWolf2.png",
		74, 40, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&FHorseSurf);
	Nm2DOpen(&FHorseSurf, "res/mob/FHorse2.png",
		92, 80, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&OrthrosSurf);
	Nm2DOpen(&OrthrosSurf, "res/mob/Orthros2.png",
		90, 48, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&CerberusSurf);
	Nm2DOpen(&CerberusSurf, "res/mob/Cerberus2.png",
		90, 64, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&BehemothSurf);
	Nm2DOpen(&BehemothSurf, "res/mob/behimos2.png",
		100, 48, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&BaihuSurf);
	Nm2DOpen(&BaihuSurf, "res/mob/BTiger2.png",
		66, 96, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&DaemonSurf);
	Nm2DOpen(&DaemonSurf, "res/mob/Daemon2.png",
		74, 88, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&DKnightSurf);
	Nm2DOpen(&DKnightSurf, "res/mob/DarkKnight.png",
		74, 86, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&FallenMSurf);
	Nm2DOpen(&FallenMSurf, "res/mob/FallenM.png",
		90, 105, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&FallenSSurf);
	Nm2DOpen(&FallenSSurf, "res/mob/FallenS.png",
		82, 88, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&SukeletonSurf);
	Nm2DOpen(&SukeletonSurf, "res/mob/Sukeruton2.png",
		50, 64, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&SukeletonArrowSurf);
	Nm2DOpen(&SukeletonArrowSurf, "res/mob/BSukeruton2.png",
		58, 72, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&SukeletonGeneralSurf);
	Nm2DOpen(&SukeletonGeneralSurf, "res/mob/WSukeruton2.png",
		50, 96, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&ZombieSurf);
	Nm2DOpen(&ZombieSurf, "res/mob/zombie.png",
		50, 48, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&ZombieSSurf);
	Nm2DOpen(&ZombieSSurf, "res/mob/Szombie.png",
		58, 64, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&ShermanSurf);
	Nm2DOpen(&ShermanSurf, "res/mob/Sherman2.png",
		66, 48, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&WitchSurf);
	Nm2DOpen(&WitchSurf, "res/mob/witch2.png",
		58, 80, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&VampireSurf);
	Nm2DOpen(&VampireSurf, "res/mob/Vampire1.png",
		82, 88, 2, 2, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DArrayInit(&MonstersFaceSurf);
	Nm2DArrayOpen(&MonstersFaceSurf, "res/kao.png",
		145, 115, 8, 2, D3DCOLOR_RGBA(0, 255, 0, 0));
	for (i = 0,j=208,GhGap=48; i < 5; i++,j+=220,GhGap+=60){
		Nm2DArrayAddSurface(&MonstersFaceSurf, j, 563);
		Nm2DArraySetIndex(&MonstersFaceSurf, i, GetMenberData((j-GhGap) / TIP_SIZE2, 515 / TIP_SIZE2));
		if (GetMenberData((j - GhGap) / TIP_SIZE2, 515 / TIP_SIZE2) == skeletonGeneral2){
			Nm2DArraySetIndex(&MonstersFaceSurf, i, skeletonGeneral);
		}
	}
	for (i = 0; i < MonsterMax; i++){
		unitcount[i].ActionWeitP = 0, unitcount[i].ActionWeitE = 0;
		units[i].UnitX = 1025;
		units[i].UnitY = 320;
	}
	/*for (int i = 0; i < 4; i++){
		for (int j = 0; j < 9; j++){
			monF[i][j] = 1;
		}
	}*/
}
void UnitUpdata(int PlayeraM){
	
	for (i = 0; i < MonsterMax; i++){
		if (units[i].UnitExit == true){
			if (UnitCollideChaeckBace(units[i]) == no){
				if (units[i].UnitMoveFlag == ok){
					UpS = units[i].UnitSpeed;
				}
				else{
					UpS = 0;
				}
			}
			
			else if (UnitCollideChaeckBace(units[i]) == ok){
				UpS = 0;
				if (units[i].UnitExit == true){
					unitcount[i].ActionWeitP++;
					UnitActionBace(&enemy.PlayerHp, &unitcount[i].ActionWeitP, units[i]);
					
				}
			}
		}
		if (UnitE[i].UnitExit == true){
			if (UnitCollideChaeckBace(UnitE[i]) == no){
				if (UnitE[i].UnitMoveFlag == ok){
					UeS = UnitE[i].UnitSpeed;
				}
				else{
					UeS = 0;
				}
			}
			else if (UnitCollideChaeckBace(UnitE[i]) == ok){
				UeS = 0;
				if (UnitE[i].UnitExit == true){
					unitcount[i].ActionWeitE++;
					UnitActionBace(&player1.PlayerHp, &unitcount[i].ActionWeitE, UnitE[i]);

				}
			}
		}
		for (j = 0; j < MonsterMax; j++){
			if (UnitCollideChaeck(units[i], UnitE[j]) == ok){
				if (UnitE[j].UnitExit == true){
					units[i].UnitMoveFlag = no;
					unitcount[j].ActionWeitP++;
					UnitAction(&UnitE[j].UnitHp, &unitcount[i].ActionWeitE, UnitE[j].UnitStepback,
						&UnitE[j].UnitStepBack2, &UnitE[j].UnitX, units[i], UnitE[j]);
					if (UnitE[j].UnitHp <= 0){
						if (UnitE[j].UnitExit != false){
							UnitE[j].UnitMoveFlag = no;
							UnitE[j].UnitExit = false;
							player1.PlayerMp += UnitE[j].UnitMp/2;
							UnitE[j].UnitX = 2000;
							UnitE[j].UnitY = 320;
							enemy.PlayerCnt--;
						}
					}
				}
			} 
			if (UnitCollideChaeck(units[j], UnitE[i]) == ok){
				if (units[j].UnitExit == true){
					UnitE[i].UnitMoveFlag = no;
					unitcount[j].ActionWeitE++;
					UnitAction(&units[j].UnitHp, &unitcount[i].ActionWeitP, units[j].UnitStepback,
						&units[j].UnitStepBack2, &units[j].UnitX, UnitE[i], units[j]);
					if (units[i].UnitHp <= 0){
						if (units[i].UnitExit != false){
							units[i].UnitMoveFlag = no;
							units[i].UnitExit = false;
							units[i].UnitX = 2000;
							units[i].UnitY = 320;
							player1.PlayerCnt--;
						}
					}
				}
			}
		}
		if (units[i].UnitExit == true){
		Nm2DMoveX(&units[i].UnitSurf, -UpS);
		units[i].UnitX = Nm2DGetX(&units[i].UnitSurf);
		}
		if (UnitE[i].UnitExit == true){
			Nm2DMoveX(&UnitE[i].UnitSurf, UeS);
			UnitE[i].UnitX = Nm2DGetX(&UnitE[i].UnitSurf);
		}
	}
}
void UnitRender(int Pkind){
	int i;
	Nm2DArrayRender(&MonstersFaceSurf);
	for (i = 0; i < MonsterMax; i++){
		Nm2DRender(&units[i].UnitSurf);
		Nm2DRender(&UnitE[i].UnitSurf);
		if (units[i].UnitExit==false){
			Nm2DSetVisible(&units[i].UnitSurf, false);
		}if (UnitE[i].UnitExit == false){
			Nm2DSetVisible(&UnitE[i].UnitSurf, false);
		}
	}
	for (i = 0; i < MonsterMax; i++){
		Nm2DSet(&units[i].UnitSurf, units[i].UnitX, units[i].UnitY);
		if (units[i].UnitExit == false){
			Nm2DSetVisible(&units[i].UnitSurf, false);
		}
		Nm2DSet(&UnitE[i].UnitSurf, UnitE[i].UnitX, UnitE[i].UnitY);
		if (UnitE[i].UnitExit == false){
			Nm2DSetVisible(&UnitE[i].UnitSurf, false);
		}
	}
}
void UnitDelete(){
	int i;
	for (i = 0; i < MonsterMax; i++){
		//味方のリセット
		Nm2DSetVisible(&units[i].UnitSurf,false);
		units[i].UnitExit = false;
		units[i].UnitHp = NO_DATA_M;
		units[i].UnitMp = NO_DATA_M;
		units[i].UnitSpeed = NO_DATA_M;
		units[i].UnitPower = NO_DATA_M;
		units[i].UnitWay = NO_DATA_M;
		units[i].UnitX = NO_DATA_M;
		units[i].UnitY = NO_DATA_M;
		Nm2DDeleteSurface(&units[i].UnitSurf);
		//敵のリセット
		Nm2DSetVisible(&UnitE[i].UnitSurf,false);
		UnitE[i].UnitExit = false;
		UnitE[i].UnitHp = NO_DATA_M;
		UnitE[i].UnitMp = NO_DATA_M;
		UnitE[i].UnitSpeed = NO_DATA_M;
		UnitE[i].UnitPower = NO_DATA_M;
		UnitE[i].UnitWay = NO_DATA_M;
		UnitE[i].UnitX = NO_DATA_M;
		UnitE[i].UnitY = NO_DATA_M;
		Nm2DDeleteSurface(&UnitE[i].UnitSurf);
	}
	Nm2DArraySetVisible(&MonstersFaceSurf,false);
	Nm2DArrayDeleteSurface(&MonstersFaceSurf);
}

void Unitspawn(int *unitcnt, int unitkind, int Pkind){
	if (Pkind == PlayerM){
		switch (unitkind)
		{
		case Slime:
			units[*unitcnt].UnitHp = slimeHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = slimeMp;
			units[*unitcnt].UnitSpeed = slimeSpeed;
			units[*unitcnt].UnitPower = slimePower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = slimeAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = SlimeSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 370;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case goblin:
			units[*unitcnt].UnitHp = goblinHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = goblinMp;
			units[*unitcnt].UnitSpeed = goblinSpeed;
			units[*unitcnt].UnitPower = goblinPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = goblinAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = goblinSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 352;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case goblinM:
			units[*unitcnt].UnitHp = GoblinMHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = GoblinMMp;
			units[*unitcnt].UnitSpeed = GoblinMSpeed;
			units[*unitcnt].UnitPower = GoblinMPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = GoblinMAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = goblinMSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 328;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case goblinS:
			units[*unitcnt].UnitHp = GoblinSHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = GoblinSMp;
			units[*unitcnt].UnitSpeed = GoblinSSpeed;
			units[*unitcnt].UnitPower = GoblinSPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = GoblinSAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = goblinSSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 352;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case goblinR:
			units[*unitcnt].UnitHp = GoblinRHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = GoblinRMp;
			units[*unitcnt].UnitSpeed = GoblinRSpeed;
			units[*unitcnt].UnitPower = GoblinRPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = GoblinRAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = goblinRSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 352;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case golem:
			units[*unitcnt].UnitHp = golemHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = golemMp;
			units[*unitcnt].UnitSpeed = golemSpeed;
			units[*unitcnt].UnitPower = golemPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = golemAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = golemSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 324;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case Fgolem:
			units[*unitcnt].UnitHp = FGolemHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = FGolemMp;
			units[*unitcnt].UnitSpeed = FGolemSpeed;
			units[*unitcnt].UnitPower = FGolemPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = FGolemAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = FGolemSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 306;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 2);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case dragon:
			units[*unitcnt].UnitHp = dragonHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = dragonMp;
			units[*unitcnt].UnitSpeed = dragonSpeed;
			units[*unitcnt].UnitPower = dragonPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = dragonAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = dragonSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 297;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 2);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case Tdragon:
			units[*unitcnt].UnitHp = TDragonHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = TDragonMp;
			units[*unitcnt].UnitSpeed = TDragonSpeed;
			units[*unitcnt].UnitPower = TDragonPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = TDragonAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = TDragonSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 326;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 2);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case giant:
			units[*unitcnt].UnitHp = giantHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = giantMp;
			units[*unitcnt].UnitSpeed = giantSpeed;
			units[*unitcnt].UnitPower = giantPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = giantAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = giantSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 318;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case wolf:
			units[*unitcnt].UnitHp = GWolfHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = GWolfMp;
			units[*unitcnt].UnitSpeed = GWolfSpeed;
			units[*unitcnt].UnitPower = GWolfPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = GWolfAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = WolfSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 360;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case FHorse:
			units[*unitcnt].UnitHp = FHorseHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = FHorseMp;
			units[*unitcnt].UnitSpeed = FHorseSpeed;
			units[*unitcnt].UnitPower = FHorsePower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = FHorseAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = FHorseSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 320;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case orthros:
			units[*unitcnt].UnitHp = OrthrosHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = OrthrosMp;
			units[*unitcnt].UnitSpeed = OrthrosSpeed;
			units[*unitcnt].UnitPower = OrthrosPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = OrthrosAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = OrthrosSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 352;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 2);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case Cerberus:
			units[*unitcnt].UnitHp = CerberusHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = CerberusMp;
			units[*unitcnt].UnitSpeed = CerberusSpeed;
			units[*unitcnt].UnitPower = CerberusPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = CerberusAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = CerberusSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 336;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case behemoth:
			units[*unitcnt].UnitHp = BehemothHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = BehemothMp;
			units[*unitcnt].UnitSpeed = BehemothSpeed;
			units[*unitcnt].UnitPower = BehemothPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = BehemothAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = BehemothSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 352;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case baihu:
			units[*unitcnt].UnitHp = BaihuHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = BaihuMp;
			units[*unitcnt].UnitSpeed = BaihuSpeed;
			units[*unitcnt].UnitPower = BaihuPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = BaihuAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = BaihuSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 304;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case Daemon:
			units[*unitcnt].UnitHp = DaemonHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = DaemonMp;
			units[*unitcnt].UnitSpeed = DaemonSpeed;
			units[*unitcnt].UnitPower = DaemonPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = DaemonAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = DaemonSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 312;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case DKnight:
			units[*unitcnt].UnitHp = DarkKnightHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = DarkKnightMp;
			units[*unitcnt].UnitSpeed = DarkKnightSpeed;
			units[*unitcnt].UnitPower = DarkKnightPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = DarkKnightAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = DKnightSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 314;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case FallenM:
			units[*unitcnt].UnitHp = FallenMHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = FallenMMp;
			units[*unitcnt].UnitSpeed = FallenMSpeed;
			units[*unitcnt].UnitPower = FallenMPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = FallenMAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = FallenMSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 294;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case FallenS:
			units[*unitcnt].UnitHp = FallenSHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = FallenSMp;
			units[*unitcnt].UnitSpeed = FallenSSpeed;
			units[*unitcnt].UnitPower = FallenSPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = FallenSAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = FallenSSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 312;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case skeleton:
			units[*unitcnt].UnitHp = SukeletonHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = SukeletonMp;
			units[*unitcnt].UnitSpeed = SukeletonSpeed;
			units[*unitcnt].UnitPower = SukeletonPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = SukeletonAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = SukeletonSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 336;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 2);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case skeletonArrow:
			units[*unitcnt].UnitHp = SukeletonArrowHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = SukeletonArrowMp;
			units[*unitcnt].UnitSpeed = SukeletonArrowSpeed;
			units[*unitcnt].UnitPower = SukeletonArrowPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = SukeletonArrowAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = SukeletonArrowSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 328;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case skeletonGeneral2:
			units[*unitcnt].UnitHp = SukeletonGeneralHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = SukeletonGeneralMp;
			units[*unitcnt].UnitSpeed = SukeletonGeneralSpeed;
			units[*unitcnt].UnitPower = SukeletonGeneralPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = SukeletonGeneralAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = SukeletonGeneralSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 304;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case zombie:
			units[*unitcnt].UnitHp = ZombieHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = ZombieMp;
			units[*unitcnt].UnitSpeed = ZombieSpeed;
			units[*unitcnt].UnitPower = ZombiePower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = ZombieAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = ZombieSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 352;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case zombieS:
			units[*unitcnt].UnitHp = SZombieHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = SZombieMp;
			units[*unitcnt].UnitSpeed = SZombieSpeed;
			units[*unitcnt].UnitPower = SZombiePower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = SZombieAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = ZombieSSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 336;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case Sherman:
			units[*unitcnt].UnitHp = ShermanHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = ShermanMp;
			units[*unitcnt].UnitSpeed = ShermanSpeed;
			units[*unitcnt].UnitPower = ShermanPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = ShermanAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = ShermanSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 352;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case witch:
			units[*unitcnt].UnitHp = WitchHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = WitchMp;
			units[*unitcnt].UnitSpeed = WitchSpeed;
			units[*unitcnt].UnitPower = WitchPower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = WitchAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = WitchSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 320;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		case vampire:
			units[*unitcnt].UnitHp = VampireHp;
			units[*unitcnt].UnitStepback = units[*unitcnt].UnitHp / 3;
			units[*unitcnt].UnitMp = VampireMp;
			units[*unitcnt].UnitSpeed = VampireSpeed;
			units[*unitcnt].UnitPower = VampirePower;
			units[*unitcnt].UnitWay = UNIT_LEFT;
			units[*unitcnt].ActionWeit = VampireAction;
			units[*unitcnt].UnitMoveFlag = ok;
			if (player1.PlayerMp >= units[player1.PlayerCnt].UnitMp&&player1.PlayerCnt < MonsterMax){
				units[*unitcnt].UnitSurf = VampireSurf;
				units[*unitcnt].UnitM = PlayerM;
				units[*unitcnt].UnitX = 1025;
				units[*unitcnt].UnitY = 312;
				units[*unitcnt].UnitExit = true;
				Nm2DSet(&units[*unitcnt].UnitSurf,
					units[*unitcnt].UnitX, units[*unitcnt].UnitY);
				Nm2DSetIndex(&units[*unitcnt].UnitSurf, 0);
				player1.PlayerMp -= units[*unitcnt].UnitMp;
				player1.PlayerCnt++;
			}
			break;
		default:
			break;
		}
	}
	else if (Pkind == EnemyM){
		switch (unitkind)
		{
		case Slime:
			UnitE[*unitcnt].UnitHp = slimeHp;
			UnitE[*unitcnt].UnitStepback = UnitE[*unitcnt].UnitHp / 3;
			UnitE[*unitcnt].UnitSpeed = slimeSpeed;
			UnitE[*unitcnt].UnitPower = slimePower;
			UnitE[*unitcnt].UnitMp = slimeMp;
			UnitE[*unitcnt].UnitWay = UNIT_LEFT;
			UnitE[*unitcnt].ActionWeit = slimeAction;
			UnitE[*unitcnt].UnitMoveFlag = ok;
			if (enemy.PlayerCnt <= MonsterMax){
				UnitE[*unitcnt].UnitSurf = SlimeSurf;
				UnitE[*unitcnt].UnitM = EnemyM;
				UnitE[*unitcnt].UnitX = 175;
				UnitE[*unitcnt].UnitY = 370;
				UnitE[*unitcnt].UnitExit = true;
				Nm2DSet(&UnitE[*unitcnt].UnitSurf,
					UnitE[*unitcnt].UnitX, UnitE[*unitcnt].UnitY);
				Nm2DSetIndex(&UnitE[*unitcnt].UnitSurf, 2);
				enemy.PlayerCnt++;
			}
			break;
		case goblin:
			UnitE[*unitcnt].UnitHp = goblinHp;
			UnitE[*unitcnt].UnitStepback = UnitE[*unitcnt].UnitHp / 3;
			UnitE[*unitcnt].UnitSpeed = goblinSpeed;
			UnitE[*unitcnt].UnitPower = goblinPower;
			UnitE[*unitcnt].UnitMp = goblinMp;
			UnitE[*unitcnt].UnitWay = UNIT_LEFT;
			UnitE[*unitcnt].ActionWeit = goblinAction;
			UnitE[*unitcnt].UnitMoveFlag = ok;
			if (enemy.PlayerCnt <= MonsterMax){
				UnitE[*unitcnt].UnitSurf = goblinSurf;
				UnitE[*unitcnt].UnitM = EnemyM;
				UnitE[*unitcnt].UnitX = 175;
				UnitE[*unitcnt].UnitY = 348;
				UnitE[*unitcnt].UnitExit = true;
				Nm2DSet(&UnitE[*unitcnt].UnitSurf,
					UnitE[*unitcnt].UnitX, UnitE[*unitcnt].UnitY);
				Nm2DSetIndex(&UnitE[*unitcnt].UnitSurf, 2);
				enemy.PlayerCnt++;
			}
			break;
		case golem:
			UnitE[*unitcnt].UnitHp = golemHp;
			UnitE[*unitcnt].UnitStepback = UnitE[*unitcnt].UnitHp / 3;
			UnitE[*unitcnt].UnitSpeed = golemSpeed;
			UnitE[*unitcnt].UnitPower = golemPower;
			UnitE[*unitcnt].UnitMp = golemMp;
			UnitE[*unitcnt].UnitWay = UNIT_LEFT;
			UnitE[*unitcnt].ActionWeit = golemAction;
			UnitE[*unitcnt].UnitMoveFlag = ok;
			if (enemy.PlayerCnt <= MonsterMax){
				UnitE[*unitcnt].UnitSurf = golemSurf;
				UnitE[*unitcnt].UnitM = EnemyM;
				UnitE[*unitcnt].UnitX = 175;
				UnitE[*unitcnt].UnitY = 324;
				UnitE[*unitcnt].UnitExit = true;
				Nm2DSet(&UnitE[*unitcnt].UnitSurf,
					UnitE[*unitcnt].UnitX, UnitE[*unitcnt].UnitY);
				Nm2DSetIndex(&UnitE[*unitcnt].UnitSurf, 2);
				enemy.PlayerCnt++;
			}
			break;
		case dragon:
			UnitE[*unitcnt].UnitHp = dragonHp;
			UnitE[*unitcnt].UnitStepback = UnitE[*unitcnt].UnitHp / 3;
			UnitE[*unitcnt].UnitSpeed = dragonSpeed;
			UnitE[*unitcnt].UnitPower = dragonPower;
			UnitE[*unitcnt].UnitMp = dragonMp;
			UnitE[*unitcnt].UnitWay = UNIT_LEFT;
			UnitE[*unitcnt].ActionWeit = dragonAction;
			UnitE[*unitcnt].UnitMoveFlag = ok;
			if (enemy.PlayerCnt <= MonsterMax){
				UnitE[*unitcnt].UnitSurf = dragonSurf;
				UnitE[*unitcnt].UnitM = EnemyM;
				UnitE[*unitcnt].UnitX = 175;
				UnitE[*unitcnt].UnitY = 292;
				UnitE[*unitcnt].UnitExit = true;
				Nm2DSet(&UnitE[*unitcnt].UnitSurf,
					UnitE[*unitcnt].UnitX, UnitE[*unitcnt].UnitY);
				Nm2DSetIndex(&UnitE[*unitcnt].UnitSurf, 0);
				enemy.PlayerCnt++;
			}
			break;
		default:
			break;
		}
	}
}

int UnitCollideChaeck(unit Punit, unit Eunit){
	if (Punit.UnitExit == true && Eunit.UnitExit == true){
		if (Punit.UnitX <=Eunit.UnitX + Nm2DGetWidth(&Eunit.UnitSurf)){
			return ok;
		}
		else{
			return no;
		}
	}
}
int UnitCollideChaeckBace(unit Punit){
	if (Punit.UnitM == PlayerM){
		if (Punit.UnitX <= Nm2DGetX(&enemy.PlayerBaceSurf) + (Nm2DGetWidth(&enemy.PlayerBaceSurf)*2)){
			return ok;
		}
		else{
			return no;
		}
	}
	else{
		if (Punit.UnitX >= Nm2DGetX(&player1.PlayerBaceSurf)-25){
			return ok;
		}
		else{
			return no;
		}
	}
}
int UnitMoveChaek(unit Unit){
	NoCollideFlag = 0;
	int i;
	if (Unit.UnitM == PlayerM){
		for (i = 0; i < MonsterMax; i++){
			if (UnitCollideChaeck(Unit, UnitE[i]) == no){
				NoCollideFlag++;
			}
		}
		if (NoCollideFlag - enemy.PlayerCnt == MonsterMax - enemy.PlayerCnt){
			return ok;
		}
		else
		{
			NoCollideFlag;
			return no;
		}
	}
	else if(Unit.UnitM==EnemyM){
		for (i = 0; i < MonsterMax; i++){
			if (UnitCollideChaeck(units[i], Unit) == no){
				NoCollideFlag++;
			}
		}
		if (NoCollideFlag - player1.PlayerCnt == MonsterMax - player1.PlayerCnt){
			return ok;
		}
		else
		{
			return no;
		}
	}
	
}
void UnitAction(int *Unit1hp, int *Acnt, int UnitSb, int *UnitSbl, int *UnitX, unit Unit, unit Unit2){
	if (Unit.ActionWeit - *Acnt <= 0){
		*Unit1hp -= Unit.UnitPower;
		if (Unit1hp <= 0){
			*Acnt = 0;
		}
		*UnitSbl += Unit.UnitPower;
		if (Unit2.UnitM == PlayerM){
			if (UnitSb - *UnitSbl <= 0){
				*UnitX += 30;
				*UnitSbl = 0;
			}
		}
		if (Unit2.UnitM == EnemyM){
			if (UnitSb - *UnitSbl <= 0){
				*UnitX -= 30;
				*UnitSbl = 0;
			}
		}
		*Acnt = 0;
	}
}
void UnitActionBace(int *Bacehp, int *Acnt, unit Unit){
	if (Unit.ActionWeit - *Acnt <= 0){
		*Bacehp -= Unit.UnitPower;
		*Acnt = 0;
	}
}
int UnitAliveChack(int UnitM){
	int i;
	for (i = 0; i < MonsterMax; i++){
		if (UnitM == PlayerM){
			if (units[i].UnitExit == false){
				return i;
			}
		}
		if(UnitM==EnemyM){
			if (UnitE[i].UnitExit == false){
				return i;
			}
		}
	}
}
void getUnitSave(char* _FileName){
	//ファイルを開く
	FILE *fp = fopen(_FileName, "w");
	//ファイルの書き込み
	fwrite(monF, sizeof(int), 9*4, fp);
	//ファイルを閉じる
	fclose(fp);
}
void getUnitLoad(char* _FileName){
	//ファイルを開く
	FILE *fp = fopen(_FileName, "r");
	//ファイルの読み込み
	fread(monF, sizeof(int), 9*4, fp);
	//ファイルを閉じる
	fclose(fp);
}
void UnitMoveUpdata(){
	int i;
	for (i = 0; i < MonsterMax; i++){
		if (units[i].UnitExit == true){
			units[i].UnitMoveFlag = UnitMoveChaek(units[i]);
		}
		if (UnitE[i].UnitExit == true){
			UnitE[i].UnitMoveFlag = UnitMoveChaek(UnitE[i]);
		}
	}
}