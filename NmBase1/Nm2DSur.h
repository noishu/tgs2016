/********************************************************/
/* DirectX  ゲームプログラミング						*/
/*						ゲームライブラリー				*/
/*														*/
/* Nm2DSur.h - ２Ｄサーフェイス処理						*/
/*														*/
/*					沼津情報専門学校　Atsushi Ohba		*/
/*						Copyright (c) 2005-2005			*/
/*						All Rights Reserved.			*/
/********************************************************/

#ifndef __NM2DSUR_H_
#define __NM2DSUR_H_


struct MY_VERTEX
{
	D3DXVECTOR3 p;
	float	rhw;								 
	DWORD		color;
	D3DXVECTOR2 t;
};

#define MY_VERTEX_FVF ( D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX1)


typedef struct _FPoint
{
	float x,y;
}FPoint;

void setTexture(LPDIRECT3DTEXTURE9 tex);

/////////////////////////////////////////////////////////////////////////////////
// NmFont構造体
/////////////////////////////////////////////////////////////////////////////////
typedef struct  _NmFont
{
	LPD3DXFONT 			m_pFontObject;	// フォントオブジェクト
}NmFont;
// フォントの初期化
void NmFontInit( NmFont *pFont );
// フォントを作成する
void NmFontCreate( NmFont *pFont, int h, char *font_name );
// フォントオブジェクトを得る
LPD3DXFONT NmFontGet(NmFont *pFont);
// フォントの削除
void NmFontDelete(NmFont *pFont);
// テキスト描画左揃え
void NmFontDrawText(NmFont *pFont, char *str, int x, int y, D3DCOLOR color);
// テキスト描画中央揃え
void NmFontDrawTextCenter(NmFont *pFont, char *str, int x, int y, D3DCOLOR color);
// テキスト描画右揃え
void NmFontDrawTextRight(NmFont *pFont, char *str, int x, int y, D3DCOLOR color);


// ＦＰＳの表示
extern void NmFpsDisp(NmFont *pFont, int x, int y);


/////////////////////////////////////////////////////////////////////////////////
// NmSurfaceData
/////////////////////////////////////////////////////////////////////////////////

typedef struct _Nm2DSurfaceData
{
public:
	FPoint					m_poPos;		// 位置
	bool					m_bVisible;		// 表示非表示
	float					m_fAngle;		// 回転角度
	float					m_fScale_x;		// 拡大・縮小率
	float					m_fScale_y;		// 拡大・縮小率
	D3DCOLOR				m_Color;		// カラー
	int						m_nIndex;		// インデックス
}Nm2DSurfaceData;

void Nm2DSurfaceDataInit( Nm2DSurfaceData *pSurfaceData  );


/////////////////////////////////////////////////////////////////////////////////
// Nm2DSurfaceクラス
/////////////////////////////////////////////////////////////////////////////////
typedef struct _Nm2DSurface 
{
//	LPDIRECT3DVERTEXBUFFER9 m_pMyVB;
	LPDIRECT3DTEXTURE9		m_pTexture;
	MY_VERTEX v[4];

	Nm2DSurfaceData			m_SurfaceData;
	CSize					m_szTextureSize;		// サイズ
	CSize					m_szSize;		// サイズ
	CSize					m_szDivision;	// 分割数
	CPoint					m_poOffset;		// オフセット
}Nm2DSurface;

// サーフェイスの初期化
void Nm2DInit( Nm2DSurface *pSurface ); 

// サーフェイスの読込(サーフェイス変数、ファイルパス、1画像の横幅、１画像の縦幅、横分割数、縦分割数、透過色)
BOOL Nm2DOpen(  Nm2DSurface *pSurface, char  *filename,  int nWidth,  int nHeight,  int nWidthNum = 1,  int nHeightNum = 1 , D3DCOLOR rgba = 0 );
//サーフェイスを画像読み込みしない形で作る(サーフェイス変数、画像の横幅、画像の縦幅、色指定
BOOL Nm2DCreateSurface(  Nm2DSurface *pSurface, int nWidth, int nHeight , D3DCOLOR rgba );
//サーフェイスの削除
void  Nm2DDeleteSurface( Nm2DSurface *pSurface );

// サーフェイスの描画
void  Nm2DRender( Nm2DSurface *pSurface );
//サーフェイスの早い描画(回転、拡大縮小を適応しない場合は若干こっちの方が早いです)
void  Nm2DRenderFast( Nm2DSurface *pSurface );

// サーフェイスの当たり判定(画像A、画像B　戻り値bool true：当たった)
BOOL Nm2DIsCollide( Nm2DSurface *pSurface, Nm2DSurface *pTarget );
BOOL IsRectIn(  D3DXVECTOR3 *rect, D3DXVECTOR3 pt );	//内部関数

// サーフェイスの横幅取得
int   Nm2DGetWidth( Nm2DSurface *pSurface );       
// サーフェイスの縦幅取得
int   Nm2DGetHeight( Nm2DSurface *pSurface );      

// サーフェイスの原点設定
void  Nm2DSetOrigin( Nm2DSurface *pSurface, int x, int y );
//サーフェイスの原点設定(X座標のみ）
void  Nm2DSetOriginX( Nm2DSurface *pSurface, int x );       
//サーフェイスの原点設定(Y座標のみ）
void  Nm2DSetOriginY( Nm2DSurface *pSurface, int y );       
//サーフェイスの原点取得(X座標のみ）
int   Nm2DGetOriginX( Nm2DSurface *pSurface );    
//サーフェイスの原点取得(Y座標のみ）
int   Nm2DGetOriginY( Nm2DSurface *pSurface );       

// サーフェイスの位置設定
void  Nm2DSet( Nm2DSurface *pSurface, float x, float y );   
// サーフェイスの位置設定(X座標のみの指定）
void  Nm2DSetX( Nm2DSurface *pSurface, float x );     
// サーフェイスの位置設定(Y座標のみの指定）
void  Nm2DSetY( Nm2DSurface *pSurface, float y );  
// サーフェイスの位置取得(X座標のみの指定）
float Nm2DGetX( Nm2DSurface *pSurface );    
// サーフェイスの位置取得(Y座標のみの指定）
float Nm2DGetY( Nm2DSurface *pSurface );

// サーフェイスの移動設定（1フレーム前の座標から移動します）
void  Nm2DMove( Nm2DSurface *pSurface, float x, float y );
// サーフェイスの移動設定（X座標のみ指定）
void  Nm2DMoveX( Nm2DSurface *pSurface, float x );       
// サーフェイスの移動設定（Y座標のみ指定）
void  Nm2DMoveY( Nm2DSurface *pSurface, float y );       
// サーフェイスの左上座標(X座標)指定
void  Nm2DSetX1( Nm2DSurface *pSurface, float x );   
// サーフェイスの左上座標(Y座標)指定
void  Nm2DSetY1( Nm2DSurface *pSurface, float y );  
// サーフェイスの右上座標(X座標)指定
void  Nm2DSetX2( Nm2DSurface *pSurface, float x ); 
// サーフェイスの左下座標(Y座標)指定
void  Nm2DSetY2( Nm2DSurface *pSurface, float y );  

// サーフェイスの左上座標(X座標)取得
float Nm2DGetX1( Nm2DSurface *pSurface );
// サーフェイスの左上座標(Y座標)指定
float Nm2DGetY1( Nm2DSurface *pSurface );
// サーフェイスの右上座標(X座標)指定
float Nm2DGetX2( Nm2DSurface *pSurface );
// サーフェイスの左下座標(Y座標)指定
float Nm2DGetY2( Nm2DSurface *pSurface );


// サーフェイスの画像表示設定(サーフェイス変数、表示設定bool型true：Visible有効)
void  Nm2DSetVisible( Nm2DSurface *pSurface, bool bVisible );
//サーフェイスの画像表示取得(戻り値bool true：Visible有効)
bool  Nm2DIsVisible( Nm2DSurface *pSurface );       


// サーフェイスの回転設定(サーフェイス変数、角度）
void  Nm2DSetAngle( Nm2DSurface *pSurface, float ang );  
//サーフェイスの回転加算(サーフェイス変数、加算する角度)
void  Nm2DAddAngle( Nm2DSurface *pSurface, float ang );
//サーフェイスの回転取得
float Nm2DGetAngle( Nm2DSurface *pSurface );       

// サーフェイスの拡大縮小率設定(X,Y座標両方指定)
void  Nm2DSetScale( Nm2DSurface *pSurface, float sc );
//サーフェイスの拡大縮小設定(X座標のみ)
void  Nm2DSetScaleX( Nm2DSurface *pSurface, float sc );
//サーフェイスの拡大縮小取得(X座標のみ)
float Nm2DGetScaleX( Nm2DSurface *pSurface );  
//サーフェイスの拡大縮小設定(Y座標のみ)
void  Nm2DSetScaleY( Nm2DSurface *pSurface, float sc );  
//サーフェイスの拡大縮小取得(Y座標のみ)
float Nm2DGetScaleY( Nm2DSurface *pSurface );      

// サーフェイスのインデックス番号指定(サーフェイス変数、インデックス番号)
void Nm2DSetIndex( Nm2DSurface *pSurface, int n);
//サーフェイスのインデックス番号取得
int Nm2DGetIndex( Nm2DSurface *pSurface);


// サーフェイスの頂点カラー指定(サーフェイス変数、D3DCOLORカラー指定
void Nm2DSetColor( Nm2DSurface *pSurface, D3DCOLOR color );
//サーフェイスの頂点カラー取得(戻り値：D3DCOLOR型)
D3DCOLOR Nm2DGetColor(Nm2DSurface *pSurface );






/////////////////////////////////////////////////////////////////////////////////
// Nm2DSurfaceArray
/////////////////////////////////////////////////////////////////////////////////

typedef struct _Nm2DSurfaceArray
{
//	LPDIRECT3DVERTEXBUFFER9 m_pMyVB;
	LPDIRECT3DTEXTURE9		m_pTexture;

	CSize					m_szTextureSize;	// サイズ
	CSize					m_szSize;			// サイズ
	CSize					m_szDivision;		// 分割数
	CPoint					m_poOffset;			// オフセット
	
	CObArray	m_SurfaceData;
	bool		m_bVisible;
	MY_VERTEX v[4];
} Nm2DSurfaceArray;

// サーフェイス配列の初期化
void Nm2DArrayInit( Nm2DSurfaceArray *pSurface );

// サーフェイス配列の読込(サーフェイス変数、ファイルパス、1画像の横幅、１画像の縦幅、横分割数、縦分割数、透過色)
BOOL Nm2DArrayOpen(  Nm2DSurfaceArray *pSurface, char  *filename,  int nWidth,  int nHeight,  int nWidthNum = 1,  int nHeightNum = 1 , D3DCOLOR rgba = 0 );
//サーフェイス配列を画像読み込みしない形で作る(サーフェイス変数、画像の横幅、画像の縦幅、色指定
BOOL Nm2DArrayCreateSurface(  Nm2DSurfaceArray *pSurface, int nWidth, int nHeight , D3DCOLOR rgba );
//サーフェイス配列の削除
void  Nm2DArrayDeleteSurface( Nm2DSurfaceArray *pSurface );

// サーフェイス配列の描画
void Nm2DArrayRender( Nm2DSurfaceArray *pSurface );

// サーフェイス配列の横幅取得
int   Nm2DArrayGetWidth( Nm2DSurfaceArray *pSurface );    
// サーフェイス配列の縦幅取得
int   Nm2DArrayGetHeight( Nm2DSurfaceArray *pSurface );      

// サーフェイス配列の原点設定
void  Nm2DArraySetOrigin( Nm2DSurfaceArray *pSurface, int x, int y );  
//サーフェイス配列の原点設定(X座標のみ）
void  Nm2DArraySetOriginX( Nm2DSurfaceArray *pSurface, int x );      
//サーフェイス配列の原点設定(Y座標のみ）
void  Nm2DArraySetOriginY( Nm2DSurfaceArray *pSurface, int y );  
//サーフェイス配列の原点取得(X座標のみ）
int   Nm2DArrayGetOriginX( Nm2DSurfaceArray *pSurface ); 
//サーフェイス配列の原点取得(Y座標のみ）
int   Nm2DArrayGetOriginY( Nm2DSurfaceArray *pSurface );       

// サーフェイス配列の画像表示設定(サーフェイス変数、表示設定bool型true：Visible有効)
void  Nm2DArraySetVisible( Nm2DSurfaceArray *pSurface, bool bVisible );   
//サーフェイスの画像表示取得(戻り値bool true：Visible有効)
bool  Nm2DArrayIsVisible( Nm2DSurfaceArray *pSurface );       

// サーフェイス配列の追加　戻り値：サーフェイス配列番号
int Nm2DArrayAddSurface( Nm2DSurfaceArray *pSurface );
//サーフェイス配列の追加(座標指定版）　戻り値：サーフェイス配列番号
int Nm2DArrayAddSurface( Nm2DSurfaceArray *pSurface, float x, float y );
//サーフェイス配列の全削除
void Nm2DArrayRemoveAll( Nm2DSurfaceArray *pSurface );
//サーフェイス配列の削除（サーフェイス配列変数、削除したい配列番号)
void Nm2DArrayRemove( Nm2DSurfaceArray *pSurface, int n );
//サーフェイス配列の中から、消えているサーフェイスを検索し、削除する
void Nm2DArrayRemoveNonVisible( Nm2DSurfaceArray *pSurface );

//サーフェイス配列のサーフェイスデータを取得する(戻り値Nm2DSurfaceData)
Nm2DSurfaceData * Nm2DArrayGetSurfaceData( Nm2DSurfaceArray *pSurface, int n );

//サーフェイス配列の配列要素数を取得する
int Nm2DArrayGetSize( Nm2DSurfaceArray *pSurface );

// サーフェイス配列の位置設定(指定した画像全ての座標移動）
void Nm2DArraySetAll( Nm2DSurfaceArray *pSurface, float x, float y );
// サーフェイス配列の位置設定（サーフェイス配列変数、配列番号、X座標、Y座標）
void Nm2DArraySet( Nm2DSurfaceArray *pSurface, int n, float x, float y );
// サーフェイス配列の位置設定（サーフェイス配列変数、配列番号、X座標）
void Nm2DArraySetX( Nm2DSurfaceArray *pSurface, int n, float x );
// サーフェイス配列の位置設定（サーフェイス配列変数、配列番号、Y座標）
void Nm2DArraySetY( Nm2DSurfaceArray *pSurface, int n, float y );
// サーフェイス配列の位置取得（サーフェイス配列変数、配列番号）戻り値float X座標
float Nm2DArrayGetX( Nm2DSurfaceArray *pSurface, int n );
// サーフェイス配列の位置取得（サーフェイス配列変数、配列番号）戻り値float Y座標
float Nm2DArrayGetY( Nm2DSurfaceArray *pSurface, int n );
// サーフェイス配列の位置設定（サーフェイス配列変数、配列番号、左上X座標）
void Nm2DArraySetX1( Nm2DSurfaceArray *pSurface, int n, float x );
// サーフェイス配列の位置設定（サーフェイス配列変数、配列番号、左上Y座標）
void Nm2DArraySetY1( Nm2DSurfaceArray *pSurface, int n, float y );
// サーフェイス配列の位置設定（サーフェイス配列変数、配列番号、右上X座標）
void Nm2DArraySetX2( Nm2DSurfaceArray *pSurface, int n, float x );
// サーフェイス配列の位置設定（サーフェイス配列変数、配列番号、左下Y座標）
void Nm2DArraySetY2( Nm2DSurfaceArray *pSurface, int n, float y );
// サーフェイス配列の位置取得（サーフェイス配列変数、配列番号)戻り値float 左上X座標
float Nm2DArrayGetX1( Nm2DSurfaceArray *pSurface, int n );
// サーフェイス配列の位置取得（サーフェイス配列変数、配列番号)戻り値float 左上Y座標
float Nm2DArrayGetY1( Nm2DSurfaceArray *pSurface, int n );
// サーフェイス配列の位置取得（サーフェイス配列変数、配列番号)戻り値float 右上X座標
float Nm2DArrayGetX2( Nm2DSurfaceArray *pSurface, int n );
// サーフェイス配列の位置取得（サーフェイス配列変数、配列番号)戻り値float 左下Y座標
float Nm2DArrayGetY2( Nm2DSurfaceArray *pSurface, int n );

// サーフェイス配列の画像移動量の設定（サーフェイス配列変数、X座標、Y座標）
void Nm2DArrayMoveAll( Nm2DSurfaceArray *pSurface, float x, float y );
// サーフェイス配列の画像移動量の設定（サーフェイス配列変数、X座標）
void Nm2DArrayMoveXAll( Nm2DSurfaceArray *pSurface, float x );
// サーフェイス配列の画像移動量の設定（サーフェイス配列変数、Y座標）
void Nm2DArrayMoveYAll( Nm2DSurfaceArray *pSurface, float y );
// サーフェイス配列の単体移動量の設定（サーフェイス配列変数、配列番号、X座標、Y座標）戻り値FPoint(X座標、Y座標)
FPoint Nm2DArrayMove( Nm2DSurfaceArray *pSurface, int n, float x, float y );
// サーフェイス配列の単体移動量の設定（サーフェイス配列変数、配列番号、X座標）戻り値：移動後X座標
float  Nm2DArrayMoveX( Nm2DSurfaceArray *pSurface, int n, float x );
// サーフェイス配列の単体移動量の設定（サーフェイス配列変数、配列番号、Y座標）戻り値：移動後Y座標
float  Nm2DArrayMoveY( Nm2DSurfaceArray *pSurface, int n, float y );

// サーフェイス配列画像の表示設定(サーフェイス配列変数、配列番号、表示指定）
void Nm2DArraySetVisible( Nm2DSurfaceArray *pSurface, int n, bool bVisible );
// サーフェイス配列画像の表示取得(サーフェイス配列変数、配列番号）
bool Nm2DArrayIsVisible( Nm2DSurfaceArray *pSurface, int n );

// サーフェイス配列の回転設定
void Nm2DArraySetAngle( Nm2DSurfaceArray *pSurface, int n, float angle );
// サーフェイス配列の回転加算設定
void Nm2DArrayAddAngle( Nm2DSurfaceArray *pSurface, int n, float angle );
// サーフェイス配列の回転取得
float Nm2DArrayGetAngle( Nm2DSurfaceArray *pSurface, int n );

// サーフェイス配列の拡大縮小率設定
void Nm2DArraySetScale( Nm2DSurfaceArray *pSurface, int n, float sc );
// サーフェイス配列の拡大縮小率設定(X座標倍率)
void Nm2DArraySetScaleX( Nm2DSurfaceArray *pSurface, int n, float sc );
// サーフェイス配列の拡大縮小率設定(Y座標倍率)
void Nm2DArraySetScaleY( Nm2DSurfaceArray *pSurface, int n, float sc );
// サーフェイス配列の拡大縮小率取得(X座標倍率)
float Nm2DArrayGetScaleX( Nm2DSurfaceArray *pSurface, int n );
// サーフェイス配列の拡大縮小率取得(Y座標倍率)
float Nm2DArrayGetScaleY( Nm2DSurfaceArray *pSurface, int n );

// サーフェース配列の頂点カラー設定
void Nm2DArraySetColor( Nm2DSurfaceArray *pSurface, int n, D3DCOLOR color );
//サーフェイス配列の頂点カラー取得　戻り値D3DCOLOR型
D3DCOLOR Nm2DArrayGetColor( Nm2DSurfaceArray *pSurface, int n );

// サーフェイス配列のインデックス番号設定
void Nm2DArraySetIndex( Nm2DSurfaceArray *pSurface, int n, int index );
// サーフェイス配列のインデックス番号取得
int Nm2DArrayGetIndex( Nm2DSurfaceArray *pSurface, int n );

// サーフェイス配列の当り判定(サーフェイス配列とサーフェースの指定、当たり判定ID）
BOOL  Nm2DArrayCollideCheck( Nm2DSurfaceArray *surSource, Nm2DSurface *surTarget, int nCollideID );
// サーフェイス配列の当り判定(サーフェイス配列とサーフェース配列の指定、当たり判定ID）
BOOL  Nm2DArrayCollideCheck( Nm2DSurfaceArray *surSource, Nm2DSurfaceArray *surTarget, int nCollideID );




#endif