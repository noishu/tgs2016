#include "Player.h"
#include "menber.h"


Nm2DSurface BmenuSurf;
Nm2DSurfaceArray wakuSurf;
player player1;
int playerHpMax;
extern unit units[MonsterMax];
extern int DropF;

//召喚後cooltimeカウント用変数
int cooltime1 = 0;
int cooltime2 = 0;
int cooltime3 = 0;
int cooltime4 = 0;
int cooltime5 = 0;
int cooltime6 = 0;
int cooltime7 = 0;
int cooltime8 = 0;

//プレイヤーの準備
void playerInit(){
	MenberInit();
	menberLoad("TimeData.dat");
	player1.PlayerHp = HpStart;
	player1.PlayerMp = MpStart;
	if (DropF == 1){
		player1.PlayerMp = MpStart;
	}
	if (DropF == 2){
		player1.PlayerMp = MpStart;
		player1.PlayerMp += (MpStart / 2);
	}
	if (DropF == 3){
		player1.PlayerMp = MpStart;
		player1.PlayerMp += MpStart;
	}
	playerHpMax = player1.PlayerHp;
	Nm2DInit(&player1.PlayerBaceSurf);
	Nm2DOpen(&player1.PlayerBaceSurf, "res/BattlWall.png", 100, 100, 1, 1, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DSetIndex(&player1.PlayerBaceSurf, 1);
	Nm2DInit(&BmenuSurf);
	Nm2DOpen(&BmenuSurf, "res/Bmenu.png", 1280, 210, 1, 1, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DSetScale(&player1.PlayerBaceSurf, 2);
	Nm2DArrayInit(&wakuSurf);
	Nm2DArrayOpen(&wakuSurf, "res/waku.png", 160, 130, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
}

//プレイヤー更新
void PlayerUpdata(int time){
	if (time % 5 == 0){
		player1.PlayerMp++;
	}
}
//プレイヤーの表示（下枠）
void PlayerRenderUnder(){
	for (int i = 0,j=200; i < 5;i++,j+=220){
		Nm2DArrayAddSurface(&wakuSurf,j,555);
	}		
	Nm2DSet(&BmenuSurf,0,520);
	Nm2DRender(&BmenuSurf);
	Nm2DSet(&player1.PlayerBaceSurf, 1030, 200);
	Nm2DRender(&player1.PlayerBaceSurf);
}

//プレイヤー関連描画（上枠）
void PlayerRenderUp(){
	Nm2DArrayRender(&wakuSurf);
}

//プレイヤー(砦・MP・その他枠)の消去
void PlayerDelete(){
	Nm2DSetVisible(&player1.PlayerBaceSurf, false);
	Nm2DSetVisible(&BmenuSurf, false);
	Nm2DArraySetVisible(&wakuSurf, false);
	Nm2DDeleteSurface(&player1.PlayerBaceSurf);
	Nm2DArrayDeleteSurface(&wakuSurf);
	Nm2DDeleteSurface(&BmenuSurf);
	player1.PlayerCnt = NO_MONSTER;
	player1.PlayerHp = HpStart;
	player1.PlayerMp = MpStart;
}

//プレイヤー用ユニット召喚関連
void PlayerSumon(){
	if (GetMouseButtonLWait()){
		if (GetMouseX() > 200 && GetMouseX() < 200 + 160 &&
			GetMouseY() > 555 && GetMouseY() < 555 + 130){
			player1.PlayerCnt = UnitAliveChack(PlayerM);
			if (units[player1.PlayerCnt].UnitExit == false){
				Unitspawn(&player1.PlayerCnt, GetMenberData((GetMouseX()-40)/TIP_SIZE2,(GetMouseY() - 40)/TIP_SIZE2), PlayerM);
			}
		}
		if (GetMouseX() > 420 && GetMouseX() < 420 + 160 &&
			GetMouseY() > 555 && GetMouseY() < 555 + 130){
			player1.PlayerCnt = UnitAliveChack(PlayerM);
			if (units[player1.PlayerCnt].UnitExit == false){
				Unitspawn(&player1.PlayerCnt, GetMenberData((GetMouseX()-100) / TIP_SIZE2, (GetMouseY() - 40) / TIP_SIZE2), PlayerM);
			}
		}
		if (GetMouseX() > 640 && GetMouseX() < 640 + 160 &&
			GetMouseY() > 555 && GetMouseY() < 555 + 130){
			player1.PlayerCnt = UnitAliveChack(PlayerM);
			if (units[player1.PlayerCnt].UnitExit == false){
				Unitspawn(&player1.PlayerCnt, GetMenberData((GetMouseX()-160) / TIP_SIZE2, (GetMouseY() - 40) / TIP_SIZE2), PlayerM);
			}
		}
		if (GetMouseX() > 860 && GetMouseX() < 860 + 160 &&
			GetMouseY() > 555 && GetMouseY() < 555 + 130){
			player1.PlayerCnt = UnitAliveChack(PlayerM);
			if (units[player1.PlayerCnt].UnitExit == false){
				Unitspawn(&player1.PlayerCnt, GetMenberData((GetMouseX()-220) / TIP_SIZE2, (GetMouseY() - 40) / TIP_SIZE2), PlayerM);
			}
		}
		if (GetMouseX() > 1080 && GetMouseX() < 1080 + 160 &&
			GetMouseY() > 555 && GetMouseY() < 555 + 130){
			player1.PlayerCnt = UnitAliveChack(PlayerM);
			if (units[player1.PlayerCnt].UnitExit == false){
				int x1 = GetMenberData((GetMouseX() - 280) / TIP_SIZE2, (GetMouseY() - 40) / TIP_SIZE2);
				Unitspawn(&player1.PlayerCnt, x1, PlayerM);
				x1;
			}
		}
	}
}

//プレイヤーのモンスター数確認
int PlayerCheckMax(int unit){
	if (unit < MonsterMax){
		return true;
	}
	else{
		return false;
	}
}
