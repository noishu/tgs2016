/********************************************************/
/* DirectX  ゲームプログラミング						*/
/*						ゲームライブラリー				*/
/*														*/
/* Nm2DSur.cpp - ２Ｄサーフェイス処理					*/
/*														*/
/*					沼津情報専門学校　Atsushi Ohba		*/
/*						Copyright (c) 2005-2006			*/
/*						All Rights Reserved.			*/
/********************************************************/
#define STRICT
#define DIRECTINPUT_VERSION 0x0800
#include "NmBase1.h"
#include <commctrl.h>
#include <commdlg.h>
#include <basetsd.h>
#include <d3dx9core.h>								//	DirectX のバージョンの違いによって起こるエラー20100720追加  D3DXCreateFont用
#include <d3dx9.h>
//#include <dxerr9.h>								//	DirectX のバージョンの違いによって起こるエラー20100720コメントアウト
#include <DxErr.h>									//	DirectX のバージョンの違いによって起こるエラー20100720追加
#include <tchar.h>
#include <dinput.h>

#include "Nm2DSur.h"
#include "NmGraphic.h"
#include "GameMain.h"
#include "resource.h"

extern LPDIRECT3DDEVICE9			g_pD3DDevice;	// ダイレクト３Ｄデバイス

static LPDIRECT3DTEXTURE9 oldTex = NULL;
void setTexture(LPDIRECT3DTEXTURE9 tex){
	if(tex != oldTex){
		oldTex = tex;
		g_pD3DDevice->SetTexture(0,tex);
	}
}

/////////////////////////////////////////////////////////////////////////////////
// CNmFontクラス
/////////////////////////////////////////////////////////////////////////////////

// フォントの初期化
void NmFontInit( NmFont *pFont )
{ 
	pFont->m_pFontObject = NULL; 
}



// フォントを作成する
void NmFontCreate( NmFont *pFont, int h, char *font_name )
{
	RELEASE(pFont->m_pFontObject);
		
	D3DXCreateFont( g_pD3DDevice,          // D3D device
						 h,               // Height
						 0,                     // Width
						 FW_NORMAL,               // Weight
						 0,                     // MipLevels, 0 = autogen mipmaps
						 FALSE,                 // Italic
						 DEFAULT_CHARSET,       // CharSet
						 OUT_DEFAULT_PRECIS,    // OutputPrecision
						 DEFAULT_QUALITY,       // Quality
						 DEFAULT_PITCH | FF_DONTCARE, // PitchAndFamily
						 font_name,              // pFaceName
						 &pFont->m_pFontObject);              // ppFont
}

// フォントオブジェクトを得る
LPD3DXFONT NmFontGet(NmFont *pFont)
{ 
	return pFont->m_pFontObject; 
}
	
// フォントオの削除
void NmFontDelete(NmFont *pFont) 
{ 
	RELEASE(pFont->m_pFontObject); 
}			
	

// テキスト描画
void NmFontDrawText(NmFont *pFont, char *str, int x, int y, D3DCOLOR color)
{
	RECT rect;
	rect.left	= x;
	rect.right	= SCREEN_WIDTH;
	rect.top	= y;
	rect.bottom	= SCREEN_HEIGHT;
	pFont->m_pFontObject->DrawText(NULL,str, -1, &rect, DT_LEFT | DT_EXPANDTABS, color);
}

// テキスト描画中央揃え
void NmFontDrawTextCenter(NmFont *pFont, char *str, int x, int y, D3DCOLOR color)
{
	RECT rect;
	rect.left	= x;
	rect.right	= SCREEN_WIDTH;
	rect.top	= y;
	rect.bottom	= SCREEN_HEIGHT;
	//中央揃え
	pFont->m_pFontObject->DrawText(NULL,str, -1, &rect, DT_CENTER | DT_EXPANDTABS, color);
}

// テキスト描画右揃え
void NmFontDrawTextRight(NmFont *pFont, char *str, int x, int y, D3DCOLOR color)
{
	RECT rect;
	rect.left	= x;
	rect.right	= SCREEN_WIDTH;
	rect.top	= y;
	rect.bottom	= SCREEN_HEIGHT;
	//右揃え
	pFont->m_pFontObject->DrawText(NULL,str, -1, &rect, DT_RIGHT | DT_EXPANDTABS, color);
}


/////////////////////////////////////////////////////////////////////////////////
// Nm2DSurfaceData
/////////////////////////////////////////////////////////////////////////////////
// サーフェイスデータの初期化
void Nm2DSurfaceDataInit( Nm2DSurfaceData *pSurfaceData  )
{
	pSurfaceData->m_poPos.x = pSurfaceData->m_poPos.y = 0;
	pSurfaceData->m_nIndex = 0;
	pSurfaceData->m_fAngle = 0.0f;
	pSurfaceData->m_fScale_x = 1.0f;
	pSurfaceData->m_fScale_y = 1.0f;
	pSurfaceData->m_bVisible = TRUE;
	pSurfaceData->m_Color = D3DCOLOR_RGBA(255,255,255,255);
}


/////////////////////////////////////////////////////////////////////////////////
// Nm2DSurface
/////////////////////////////////////////////////////////////////////////////////

void Nm2DInit( Nm2DSurface *pSurface )  // サーフェイスの初期化
{
	Nm2DSurfaceDataInit( &(pSurface->m_SurfaceData) );
	pSurface->m_szDivision = CSize( 1, 1 );
	pSurface->m_pTexture = NULL;

}


// 画像オープン
BOOL Nm2DOpen(  Nm2DSurface *pSurface, char  *filename,  int nWidth,  int nHeight,  int nWidthNum,  int nHeightNum , D3DCOLOR rgba )
{

	Nm2DDeleteSurface( pSurface );

	Nm2DSurfaceDataInit( &(pSurface->m_SurfaceData) );
	pSurface->m_szSize = CSize( nWidth, nHeight );

	MY_VERTEX *v = pSurface->v;

	v[0].p = D3DXVECTOR3( 0.0f, (float)nHeight, 0.0f );
	v[1].p = D3DXVECTOR3( (float)nWidth, (float)nHeight, 0.0f );
	v[2].p = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	v[3].p = D3DXVECTOR3( (float)nWidth, 0.0f, 0.0f );
	v[0].rhw = 1.0f;
	v[1].rhw = 1.0f;
	v[2].rhw = 1.0f;
	v[3].rhw = 1.0f;
	v[0].t = D3DXVECTOR2( 0.0f, 1.0f );
	v[1].t = D3DXVECTOR2( 1.0f, 1.0f );
	v[2].t = D3DXVECTOR2( 0.0f, 0.0f );
	v[3].t = D3DXVECTOR2( 1.0f, 0.0f );
	v[0].color = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
	v[1].color = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
	v[2].color = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
	v[3].color = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );

	// テクスチャ画像の読み込み
 //   int a = D3DXCreateTextureFromFile(g_pD3DDevice, filename, &m_pTexture);
	D3DXIMAGE_INFO ImageInfo;
	D3DXCreateTextureFromFileEx(g_pD3DDevice,		// D3Dデバイス
								filename,				// ファイル名
								D3DX_DEFAULT,		// テクスチャ幅　（ファイルから取得）
								D3DX_DEFAULT,		// テクスチャ高さ（ファイルから取得）
								D3DX_DEFAULT,		
								0,					
								D3DFMT_UNKNOWN,
								D3DPOOL_MANAGED,
								D3DX_DEFAULT,		
								D3DX_DEFAULT,		
								rgba,				// カラーキー
								&ImageInfo,				 
								NULL,				
								&pSurface->m_pTexture);			// テクスチャ

	pSurface->m_szTextureSize = CSize( ImageInfo.Width, ImageInfo.Height );
	pSurface->m_szDivision = CSize( nWidthNum, nHeightNum );
	pSurface->m_SurfaceData.m_nIndex = 0;

	return TRUE;
}

// 空のサーフェイスを作成する
BOOL Nm2DCreateSurface(  Nm2DSurface *pSurface, int nWidth, int nHeight , D3DCOLOR rgba )
{

	Nm2DDeleteSurface( pSurface );

	Nm2DSurfaceDataInit( &(pSurface->m_SurfaceData) );
	pSurface->m_szSize = CSize( nWidth, nHeight );
	pSurface->m_szTextureSize = CSize( nWidth, nHeight );

	MY_VERTEX *v = pSurface->v;

	v[0].p = D3DXVECTOR3( 0.0f, (float)nHeight, 0.0f );
	v[1].p = D3DXVECTOR3( (float)nWidth, (float)nHeight, 0.0f );
	v[2].p = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	v[3].p = D3DXVECTOR3( (float)nWidth, 0.0f, 0.0f );
	v[0].rhw = 1.0f;
	v[1].rhw = 1.0f;
	v[2].rhw = 1.0f;
	v[3].rhw = 1.0f;
	v[0].t = D3DXVECTOR2( 0.0f, 1.0f );
	v[1].t = D3DXVECTOR2( 1.0f, 1.0f );
	v[2].t = D3DXVECTOR2( 0.0f, 0.0f );
	v[3].t = D3DXVECTOR2( 1.0f, 0.0f );
	v[0].color = rgba;
	v[1].color = rgba;
	v[2].color = rgba;
	v[3].color = rgba;


	pSurface->m_szDivision = CSize( 1, 1 );
	pSurface->m_SurfaceData.m_Color = rgba;
	pSurface->m_SurfaceData.m_nIndex = 0;

	return 1;

}



// サーフェイスの削除
void  Nm2DDeleteSurface( Nm2DSurface *pSurface )
{
	if(pSurface->m_pTexture!=NULL) pSurface->m_pTexture ->Release();
	pSurface->m_pTexture = NULL;
	
}



// 幅、高さを得る
int   Nm2DGetWidth( Nm2DSurface *pSurface ){ return pSurface->m_szSize.cx; }
int   Nm2DGetHeight( Nm2DSurface *pSurface ) { return pSurface->m_szSize.cy; }       

// 画像の原点
void  Nm2DSetOrigin( Nm2DSurface *pSurface, int x, int y ) { pSurface->m_poOffset.x = x; pSurface->m_poOffset.y = y; }
void  Nm2DSetOriginX( Nm2DSurface *pSurface, int x ) { pSurface->m_poOffset.x = x; }
void  Nm2DSetOriginY( Nm2DSurface *pSurface, int y ) { pSurface->m_poOffset.y = y;  }       
int   Nm2DGetOriginX( Nm2DSurface *pSurface ) { return pSurface->m_poOffset.x; }       
int   Nm2DGetOriginY( Nm2DSurface *pSurface ) { return pSurface->m_poOffset.y; } 

// 位置
void  Nm2DSet( Nm2DSurface *pSurface, float x, float y ) { pSurface->m_SurfaceData.m_poPos.x = x; pSurface->m_SurfaceData.m_poPos.y = y; }       
void  Nm2DSetX( Nm2DSurface *pSurface, float x ) { pSurface->m_SurfaceData.m_poPos.x = x; }       
void  Nm2DSetY( Nm2DSurface *pSurface, float y ) { pSurface->m_SurfaceData.m_poPos.y = y; }       
float Nm2DGetX( Nm2DSurface *pSurface ) { return pSurface->m_SurfaceData.m_poPos.x; }       
float Nm2DGetY( Nm2DSurface *pSurface ) { return pSurface->m_SurfaceData.m_poPos.y; }
void  Nm2DMove( Nm2DSurface *pSurface, float x, float y ) { pSurface->m_SurfaceData.m_poPos.x += x; pSurface->m_SurfaceData.m_poPos.y += y; }       
void  Nm2DMoveX( Nm2DSurface *pSurface, float x ) { pSurface->m_SurfaceData.m_poPos.x += x; }       
void  Nm2DMoveY( Nm2DSurface *pSurface, float y ) { pSurface->m_SurfaceData.m_poPos.y += y; }       
// 回転・縮小前の上下左右の位置
void  Nm2DSetX1( Nm2DSurface *pSurface, float x ) { pSurface->m_SurfaceData.m_poPos.x = x; }       
void  Nm2DSetY1( Nm2DSurface *pSurface, float y ) { pSurface->m_SurfaceData.m_poPos.y = y; }       
void  Nm2DSetX2( Nm2DSurface *pSurface, float x ) { pSurface->m_SurfaceData.m_poPos.x = x - pSurface->m_szSize.cx; }       
void  Nm2DSetY2( Nm2DSurface *pSurface, float y ) { pSurface->m_SurfaceData.m_poPos.y = y - pSurface->m_szSize.cy; }       
float Nm2DGetX1( Nm2DSurface *pSurface ) { return pSurface->m_SurfaceData.m_poPos.x; }       
float Nm2DGetY1( Nm2DSurface *pSurface ) { return pSurface->m_SurfaceData.m_poPos.y; }
float Nm2DGetX2( Nm2DSurface *pSurface ) { return pSurface->m_SurfaceData.m_poPos.x + pSurface->m_szSize.cx; }       
float Nm2DGetY2( Nm2DSurface *pSurface ) { return pSurface->m_SurfaceData.m_poPos.y + pSurface->m_szSize.cy; }


// 表示・非表示
void  Nm2DSetVisible( Nm2DSurface *pSurface, bool bVisible ) { pSurface->m_SurfaceData.m_bVisible = bVisible; }       
bool  Nm2DIsVisible( Nm2DSurface *pSurface ) { return pSurface->m_SurfaceData.m_bVisible; }       


// 回転
void  Nm2DSetAngle( Nm2DSurface *pSurface, float ang ) { pSurface->m_SurfaceData.m_fAngle = ang; }       
void  Nm2DAddAngle( Nm2DSurface *pSurface, float ang ) { pSurface->m_SurfaceData.m_fAngle += ang; }       
float Nm2DGetAngle( Nm2DSurface *pSurface ) { return pSurface->m_SurfaceData.m_fAngle; }       

// 拡大縮小率
void  Nm2DSetScale( Nm2DSurface *pSurface, float sc ) { pSurface->m_SurfaceData.m_fScale_x = pSurface->m_SurfaceData.m_fScale_y = sc; }       
void  Nm2DSetScaleX( Nm2DSurface *pSurface, float sc ) { pSurface->m_SurfaceData.m_fScale_x = sc; }       
float Nm2DGetScaleX( Nm2DSurface *pSurface ) { return pSurface->m_SurfaceData.m_fScale_x; }       
void  Nm2DSetScaleY( Nm2DSurface *pSurface, float sc ) { pSurface->m_SurfaceData.m_fScale_y = sc; }       
float Nm2DGetScaleY( Nm2DSurface *pSurface ) { return pSurface->m_SurfaceData.m_fScale_y; }       

// 頂点カラー
void Nm2DSetColor( Nm2DSurface *pSurface, D3DCOLOR color ) { pSurface->m_SurfaceData.m_Color = color; }
D3DCOLOR Nm2DGetColor(Nm2DSurface *pSurface ) { return pSurface->m_SurfaceData.m_Color; }



// 描画
void  Nm2DRender( Nm2DSurface *pSurface )
{
	if( !pSurface->m_SurfaceData.m_bVisible ) return;

	// テクスチャをパイプラインにセット
	setTexture(pSurface->m_pTexture);

	D3DXMATRIX matTrans;
	D3DXMatrixTranslation( &matTrans, pSurface->m_SurfaceData.m_poPos.x, pSurface->m_SurfaceData.m_poPos.y, 0.0f );
	g_pD3DDevice->SetTransform( D3DTS_WORLD, &matTrans );

	D3DXVECTOR3 d[4];
	d[0] = D3DXVECTOR3( 0.0f, (float)pSurface->m_szSize.cy, 0.0f );
	d[1] = D3DXVECTOR3( (float)pSurface->m_szSize.cx, (float)pSurface->m_szSize.cy, 0.0f );
	d[2] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	d[3] = D3DXVECTOR3( (float)pSurface->m_szSize.cx, 0.0f, 0.0f );

	MY_VERTEX *v = pSurface->v;

	double fang = RAD(pSurface->m_SurfaceData.m_fAngle);
	float fcos = cosf(fang);
	float fsin = sinf(fang);		

	// 回転、拡大縮小 処理
	for (int i=0; i<4; i++) {
		float xxx, yyy;

		// 回転、拡縮 中心補正
		d[i].x -= pSurface->m_poOffset.x;
		d[i].y -= pSurface->m_poOffset.y;
		
		// 拡大
		d[i].x *= pSurface->m_SurfaceData.m_fScale_x;
		d[i].y *= pSurface->m_SurfaceData.m_fScale_y;		

		// 回転
		xxx = d[i].x * fcos - d[i].y * fsin;
		yyy = d[i].x * fsin + d[i].y * fcos;		
		d[i].x = xxx;
		d[i].y = yyy;

		// 回転、拡縮 中心補正
		d[i].x += (pSurface->m_poOffset.x + pSurface->m_SurfaceData.m_poPos.x);
		d[i].y += (pSurface->m_poOffset.y + pSurface->m_SurfaceData.m_poPos.y);

		v[i].p = d[i];
		v[i].color = pSurface->m_SurfaceData.m_Color;

		DWORD a = pSurface->m_SurfaceData.m_Color;
		a >>= 24;
		a = (DWORD)(a * NmGetAlpha());
		a <<= 24;
		v[i].color &= 0x00FFFFFF;
		v[i].color |= a;

	}
	int index = pSurface->m_SurfaceData.m_nIndex % ( pSurface->m_szDivision.cx * pSurface->m_szDivision.cy );
	v[0].t = D3DXVECTOR2( (float)pSurface->m_szSize.cx*(pSurface->m_SurfaceData.m_nIndex%pSurface->m_szDivision.cx)/pSurface->m_szTextureSize.cx, (float)pSurface->m_szSize.cy*(pSurface->m_SurfaceData.m_nIndex/pSurface->m_szDivision.cx+1)/pSurface->m_szTextureSize.cy  );
	v[1].t = D3DXVECTOR2( (float)pSurface->m_szSize.cx*(pSurface->m_SurfaceData.m_nIndex%pSurface->m_szDivision.cx+1)/pSurface->m_szTextureSize.cx, (float)pSurface->m_szSize.cy*(pSurface->m_SurfaceData.m_nIndex/pSurface->m_szDivision.cx+1)/pSurface->m_szTextureSize.cy );
	v[2].t = D3DXVECTOR2( (float)pSurface->m_szSize.cx*(pSurface->m_SurfaceData.m_nIndex%pSurface->m_szDivision.cx)/pSurface->m_szTextureSize.cx, (float)pSurface->m_szSize.cy*(pSurface->m_SurfaceData.m_nIndex/pSurface->m_szDivision.cx)/pSurface->m_szTextureSize.cy );
	v[3].t = D3DXVECTOR2( (float)pSurface->m_szSize.cx*(pSurface->m_SurfaceData.m_nIndex%pSurface->m_szDivision.cx+1)/pSurface->m_szTextureSize.cx, (float)pSurface->m_szSize.cy*(pSurface->m_SurfaceData.m_nIndex/pSurface->m_szDivision.cx)/pSurface->m_szTextureSize.cy );


	g_pD3DDevice->SetFVF(MY_VERTEX_FVF);
	g_pD3DDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP,2,v,sizeof(MY_VERTEX) );	
}

void  Nm2DRenderFast( Nm2DSurface *pSurface ){
	if( !pSurface->m_SurfaceData.m_bVisible ) return;

	// テクスチャをパイプラインにセット
	setTexture(pSurface->m_pTexture);

	MY_VERTEX *v = pSurface->v;

	v[0].p = D3DXVECTOR3( pSurface->m_SurfaceData.m_poPos.x, (float)pSurface->m_szSize.cy+pSurface->m_SurfaceData.m_poPos.y, 0.0f );
	v[1].p = D3DXVECTOR3( (float)pSurface->m_szSize.cx+pSurface->m_SurfaceData.m_poPos.x, (float)pSurface->m_szSize.cy+pSurface->m_SurfaceData.m_poPos.y, 0.0f );
	v[2].p = D3DXVECTOR3( pSurface->m_SurfaceData.m_poPos.x, pSurface->m_SurfaceData.m_poPos.y, 0.0f );
	v[3].p = D3DXVECTOR3( (float)pSurface->m_szSize.cx+pSurface->m_SurfaceData.m_poPos.x, pSurface->m_SurfaceData.m_poPos.y, 0.0f );

	v[0].color = pSurface->m_SurfaceData.m_Color;
	v[1].color = pSurface->m_SurfaceData.m_Color;
	v[2].color = pSurface->m_SurfaceData.m_Color;
	v[3].color = pSurface->m_SurfaceData.m_Color;

	int index = pSurface->m_SurfaceData.m_nIndex % ( pSurface->m_szDivision.cx * pSurface->m_szDivision.cy );
	v[0].t = D3DXVECTOR2( (float)pSurface->m_szSize.cx*(pSurface->m_SurfaceData.m_nIndex%pSurface->m_szDivision.cx)/pSurface->m_szTextureSize.cx, (float)pSurface->m_szSize.cy*(pSurface->m_SurfaceData.m_nIndex/pSurface->m_szDivision.cx+1)/pSurface->m_szTextureSize.cy  );
	v[1].t = D3DXVECTOR2( (float)pSurface->m_szSize.cx*(pSurface->m_SurfaceData.m_nIndex%pSurface->m_szDivision.cx+1)/pSurface->m_szTextureSize.cx, (float)pSurface->m_szSize.cy*(pSurface->m_SurfaceData.m_nIndex/pSurface->m_szDivision.cx+1)/pSurface->m_szTextureSize.cy );
	v[2].t = D3DXVECTOR2( (float)pSurface->m_szSize.cx*(pSurface->m_SurfaceData.m_nIndex%pSurface->m_szDivision.cx)/pSurface->m_szTextureSize.cx, (float)pSurface->m_szSize.cy*(pSurface->m_SurfaceData.m_nIndex/pSurface->m_szDivision.cx)/pSurface->m_szTextureSize.cy );
	v[3].t = D3DXVECTOR2( (float)pSurface->m_szSize.cx*(pSurface->m_SurfaceData.m_nIndex%pSurface->m_szDivision.cx+1)/pSurface->m_szTextureSize.cx, (float)pSurface->m_szSize.cy*(pSurface->m_SurfaceData.m_nIndex/pSurface->m_szDivision.cx)/pSurface->m_szTextureSize.cy );

	g_pD3DDevice->SetFVF(MY_VERTEX_FVF);
	g_pD3DDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP,2,v,sizeof(MY_VERTEX) );
}

// 当たり判定
BOOL Nm2DIsCollide( Nm2DSurface *pSurface, Nm2DSurface *pTarget )
{
	// ソース側の座標を求める
	D3DXVECTOR3 d[4];
	int ang = (int)pSurface->m_SurfaceData.m_fAngle % 360;
	if( ang < 0 ) ang += 360;
	if( ang < 45 || ang >= 315 )
	{
		d[0] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		d[1] = D3DXVECTOR3( 0.0f, (float)pSurface->m_szSize.cy, 0.0f );
		d[2] = D3DXVECTOR3( (float)pSurface->m_szSize.cx, (float)pSurface->m_szSize.cy, 0.0f );
		d[3] = D3DXVECTOR3( (float)pSurface->m_szSize.cx, 0.0f, 0.0f );
	}
	else if( 45 <= ang && ang < 135 )
	{
		d[3] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		d[0] = D3DXVECTOR3( 0.0f, (float)pSurface->m_szSize.cy, 0.0f );
		d[1] = D3DXVECTOR3( (float)pSurface->m_szSize.cx, (float)pSurface->m_szSize.cy, 0.0f );
		d[2] = D3DXVECTOR3( (float)pSurface->m_szSize.cx, 0.0f, 0.0f );
	}
	else if( 135 <= ang && ang < 225 )
	{
		d[2] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		d[3] = D3DXVECTOR3( 0.0f, (float)pSurface->m_szSize.cy, 0.0f );
		d[0] = D3DXVECTOR3( (float)pSurface->m_szSize.cx, (float)pSurface->m_szSize.cy, 0.0f );
		d[1] = D3DXVECTOR3( (float)pSurface->m_szSize.cx, 0.0f, 0.0f );
	}
	else if( 225 <= ang && ang < 315 )
	{
		d[1] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		d[2] = D3DXVECTOR3( 0.0f, (float)pSurface->m_szSize.cy, 0.0f );
		d[3] = D3DXVECTOR3( (float)pSurface->m_szSize.cx, (float)pSurface->m_szSize.cy, 0.0f );
		d[0] = D3DXVECTOR3( (float)pSurface->m_szSize.cx, 0.0f, 0.0f );
	}

	double fang = RAD(pSurface->m_SurfaceData.m_fAngle);
	float fcos = cosf(fang);
	float fsin = sinf(fang);		

	for (int i=0; i<4; i++) {
		double xxx, yyy;

		// 回転、拡縮 中心補正
		d[i].x -= pSurface->m_poOffset.x;
		d[i].y -= pSurface->m_poOffset.y;
		
		// 拡大
		d[i].x *= pSurface->m_SurfaceData.m_fScale_x;
		d[i].y *= pSurface->m_SurfaceData.m_fScale_y;		

		// 回転
		xxx = d[i].x * fcos - d[i].y * fsin;
		yyy = d[i].x * fsin + d[i].y * fcos;		
		d[i].x = (float)xxx;
		d[i].y = (float)yyy;

		// 回転、拡縮 中心補正
		d[i].x += (pSurface->m_poOffset.x + pSurface->m_SurfaceData.m_poPos.x);
		d[i].y += (pSurface->m_poOffset.y + pSurface->m_SurfaceData.m_poPos.y);
	}

	if( d[0].x<0 && d[1].x<0 && d[2].x<0 && d[3].x<0 ) return FALSE;
	if( d[0].y<0 && d[1].y<0 && d[2].y<0 && d[3].y<0 ) return FALSE;
	if( d[0].x>=SCREEN_WIDTH && d[1].x>=SCREEN_WIDTH && d[2].x>=SCREEN_WIDTH && d[3].x>=SCREEN_WIDTH ) return FALSE;
	if( d[0].y>=SCREEN_HEIGHT && d[1].y>=SCREEN_HEIGHT && d[2].y>=SCREEN_HEIGHT && d[3].y>=SCREEN_HEIGHT ) return FALSE;


	// ターゲット側の座標を求める
	D3DXVECTOR3 t[4];
	ang = (int)pTarget->m_SurfaceData.m_fAngle % 360;
	if( ang < 0 ) ang += 360;
	if( ang < 90  )
	{
		t[0] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		t[1] = D3DXVECTOR3( 0.0f, (float)pTarget->m_szSize.cy, 0.0f );
		t[2] = D3DXVECTOR3( (float)pTarget->m_szSize.cx, (float)pTarget->m_szSize.cy, 0.0f );
		t[3] = D3DXVECTOR3( (float)pTarget->m_szSize.cx, 0.0f, 0.0f );
	}
	else if( 90 <= ang && ang < 180 )
	{
		t[3] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		t[0] = D3DXVECTOR3( 0.0f, (float)pTarget->m_szSize.cy, 0.0f );
		t[1] = D3DXVECTOR3( (float)pTarget->m_szSize.cx, (float)pTarget->m_szSize.cy, 0.0f );
		t[2] = D3DXVECTOR3( (float)pTarget->m_szSize.cx, 0.0f, 0.0f );
	}
	else if( 180 <= ang && ang < 270 )
	{
		t[2] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		t[3] = D3DXVECTOR3( 0.0f, (float)pTarget->m_szSize.cy, 0.0f );
		t[0] = D3DXVECTOR3( (float)pTarget->m_szSize.cx, (float)pTarget->m_szSize.cy, 0.0f );
		t[1] = D3DXVECTOR3( (float)pTarget->m_szSize.cx, 0.0f, 0.0f );
	}
	else if( 270 <= ang && ang < 360 )
	{
		t[1] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		t[2] = D3DXVECTOR3( 0.0f, (float)pTarget->m_szSize.cy, 0.0f );
		t[3] = D3DXVECTOR3( (float)pTarget->m_szSize.cx, (float)pTarget->m_szSize.cy, 0.0f );
		t[0] = D3DXVECTOR3( (float)pTarget->m_szSize.cx, 0.0f, 0.0f );
	}

	double fangd = RAD(pTarget->m_SurfaceData.m_fAngle);
	float fcosd = cosf(fangd);
	float fsind = sinf(fangd);		

	for (int i=0; i<4; i++) {
		double xxx, yyy;

		// 回転、拡縮 中心補正
		t[i].x -= pTarget->m_poOffset.x;
		t[i].y -= pTarget->m_poOffset.y;
		
		// 拡大
		t[i].x *= pTarget->m_SurfaceData.m_fScale_x;
		t[i].y *= pTarget->m_SurfaceData.m_fScale_y;		

		// 回転
		xxx = t[i].x * fcosd - t[i].y * fsind;
		yyy = t[i].x * fsind + t[i].y * fcosd;		
		t[i].x = (float)xxx;
		t[i].y = (float)yyy;

		// 回転、拡縮 中心補正
		t[i].x += (pTarget->m_poOffset.x + pTarget->m_SurfaceData.m_poPos.x);
		t[i].y += (pTarget->m_poOffset.y + pTarget->m_SurfaceData.m_poPos.y);
	}

	if( t[0].x<0 && t[1].x<0 && t[2].x<0 && t[3].x<0 ) return FALSE;
	if( t[0].y<0 && t[1].y<0 && t[2].y<0 && t[3].y<0 ) return FALSE;
	if( t[0].x>=SCREEN_WIDTH && t[1].x>=SCREEN_WIDTH && t[2].x>=SCREEN_WIDTH && t[3].x>=SCREEN_WIDTH ) return FALSE;
	if( t[0].y>=SCREEN_HEIGHT && t[1].y>=SCREEN_HEIGHT && t[2].y>=SCREEN_HEIGHT && t[3].y>=SCREEN_HEIGHT ) return FALSE;


	// 四角形の中に頂点が入っているか？
	if( IsRectIn( t, d[0] ) ) return TRUE;
	if( IsRectIn( t, d[1] ) ) return TRUE;
	if( IsRectIn( t, d[2] ) ) return TRUE;
	if( IsRectIn( t, d[3] ) ) return TRUE;
	if( IsRectIn( d, t[0] ) ) return TRUE;
	if( IsRectIn( d, t[1] ) ) return TRUE;
	if( IsRectIn( d, t[2] ) ) return TRUE;
	if( IsRectIn( d, t[3] ) ) return TRUE;


	// 線分が重なっているか？
	for( int i=0 ; i<4 ; i++ )
	{
		int ii = (i+1)%4;
		for( int j=0 ; j<4 ; j++ )
		{
			int jj = (j+1)%4;
			float sx1, sx2, sy1, sy2, tx1, tx2, ty1, ty2;
			sx1 = d[i].x; sx2 = d[ii].x;
			sy1 = d[i].y; sy2 = d[ii].y;
			tx1 = t[j].x; tx2 = t[jj].x;
			ty1 = t[j].y; ty2 = t[jj].y;

			float a1 = sy2 - sy1;
			float b1 = -(sx2 - sx1);
			float c1 = -sx1*(sy2-sy1)+sy1*(sx2-sx1);
			float a2 = ty2 - ty1;
			float b2 = -(tx2 - tx1);
			float c2 = -tx1*(ty2-ty1)+ty1*(tx2-tx1);

			if( ( a1 * b2 - a2 * b1 ) == 0 ) continue;
			float x0 = ( b1*c2 - b2*c1 ) / ( a1 * b2 - a2 * b1 );
			float y0 = ( a2*c1 - a1*c2 ) / ( a1 * b2 - a2 * b1 );

			float minsx = min( sx1, sx2);
			float maxsx = max( sx1, sx2);
			float minsy = min( sy1, sy2);
			float maxsy = max( sy1, sy2);
			float mintx = min( tx1, tx2);
			float maxtx = max( tx1, tx2);
			float minty = min( ty1, ty2);
			float maxty = max( ty1, ty2);
			if( ( minsy <= y0 ) && ( y0 <= maxsy ) && ( minsx <= x0 ) && ( x0 <= maxsx )
				&& ( minty <= y0 ) && ( y0 <= maxty ) && ( mintx <= x0 ) && ( x0 <= maxtx )) return TRUE;
		}
	}
	return FALSE;
}


BOOL IsRectIn( D3DXVECTOR3 *rect, D3DXVECTOR3 pt )
{

	for( int j=0 ; j<4 ; j++ )
	{
		int jj = (j+1)%4;
		int x1, x2, y1, y2;
		x1 = (int)(rect[j].x+0.5f); x2 = (int)(rect[jj].x+0.5f);
		y1 = (int)(rect[j].y+0.5f); y2 = (int)(rect[jj].y+0.5f);

		if( y1 != y2 )
		{
			float x0 = ( pt.y - y1 ) * ( x2 - x1 ) / (float)( y2 - y1 ) + x1;
			if( j==0 || j==1 ) 
			{
				if( pt.x < x0 )
				{
					return FALSE;
				}
			}
			else  
			{
				if( pt.x > x0 ) 
				{
					return FALSE;
				}
			}
		}
		if( x1 != x2 )
		{
			float y0 = ( pt.x - x1 ) * ( y2 - y1 ) / (float)( x2 - x1 ) + y1;
			if( j==0 || j==3 ) 
			{
				if( pt.y < y0 ) 
				{
					return FALSE;
				}
			}
			else 
			{
				if( pt.y > y0 ) 
				{
					return FALSE;
				}
			}
		}
	}

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////////
// Nm2DSurfaceArray
/////////////////////////////////////////////////////////////////////////////////

// 初期化
void Nm2DArrayInit( Nm2DSurfaceArray *pSurface )
{
	pSurface->m_szDivision = CSize( 1, 1 );
	pSurface->m_pTexture = NULL;

	pSurface->m_bVisible = TRUE;
}


// 画像オープン
BOOL Nm2DArrayOpen(  Nm2DSurfaceArray *pSurface, char  *filename,  int nWidth,  int nHeight,  int nWidthNum,  int nHeightNum , D3DCOLOR rgba )
{

	Nm2DArrayDeleteSurface( pSurface );

//	Nm2DSurfaceDataInit( &(pSurface->m_SurfaceData) );
	pSurface->m_szSize = CSize( nWidth, nHeight );

	MY_VERTEX *v = pSurface->v;

	v[0].p = D3DXVECTOR3( 0.0f, (float)nHeight, 0.0f );
	v[1].p = D3DXVECTOR3( (float)nWidth, (float)nHeight, 0.0f );
	v[2].p = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	v[3].p = D3DXVECTOR3( (float)nWidth, 0.0f, 0.0f );
	v[0].rhw = 1.0f;
	v[1].rhw = 1.0f;
	v[2].rhw = 1.0f;
	v[3].rhw = 1.0f;
	v[0].t = D3DXVECTOR2( 0.0f, 1.0f );
	v[1].t = D3DXVECTOR2( 1.0f, 1.0f );
	v[2].t = D3DXVECTOR2( 0.0f, 0.0f );
	v[3].t = D3DXVECTOR2( 1.0f, 0.0f );
	v[0].color = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
	v[1].color = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
	v[2].color = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
	v[3].color = D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );

	// テクスチャ画像の読み込み
 //   int a = D3DXCreateTextureFromFile(g_pD3DDevice, filename, &m_pTexture);
	D3DXIMAGE_INFO ImageInfo;
	D3DXCreateTextureFromFileExA(g_pD3DDevice,		// D3Dデバイス
								filename,				// ファイル名
								D3DX_DEFAULT,		// テクスチャ幅　（ファイルから取得）
								D3DX_DEFAULT,		// テクスチャ高さ（ファイルから取得）
								D3DX_DEFAULT,		
								0,					
								D3DFMT_A8R8G8B8,	// ピクセル フォーマット
								D3DPOOL_MANAGED,	// テクスチャの配置先のメモリクラス
								D3DX_DEFAULT,		
								D3DX_DEFAULT,		
								rgba,				// カラーキー
								&ImageInfo,				 
								NULL,				
								&pSurface->m_pTexture);			// テクスチャ



	pSurface->m_szTextureSize = CSize( ImageInfo.Width, ImageInfo.Height );
	pSurface->m_szDivision = CSize( nWidthNum, nHeightNum );

	return TRUE;
}

// 空のサーフェイスを作成する
BOOL Nm2DArrayCreateSurface(  Nm2DSurfaceArray *pSurface, int nWidth, int nHeight , D3DCOLOR rgba )
{

	Nm2DArrayDeleteSurface( pSurface );

	pSurface->m_szSize = CSize( nWidth, nHeight );
	pSurface->m_szTextureSize = CSize( nWidth, nHeight );

	MY_VERTEX *v = pSurface->v;

	v[0].p = D3DXVECTOR3( 0.0f, (float)nHeight, 0.0f );
	v[1].p = D3DXVECTOR3( (float)nWidth, (float)nHeight, 0.0f );
	v[2].p = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	v[3].p = D3DXVECTOR3( (float)nWidth, 0.0f, 0.0f );
	v[0].rhw = 1.0f;
	v[1].rhw = 1.0f;
	v[2].rhw = 1.0f;
	v[3].rhw = 1.0f;
	v[0].t = D3DXVECTOR2( 0.0f, 1.0f );
	v[1].t = D3DXVECTOR2( 1.0f, 1.0f );
	v[2].t = D3DXVECTOR2( 0.0f, 0.0f );
	v[3].t = D3DXVECTOR2( 1.0f, 0.0f );
	v[0].color = rgba;
	v[1].color = rgba;
	v[2].color = rgba;
	v[3].color = rgba;

	pSurface->m_szDivision = CSize( 1, 1 );
//	pSurface->m_SurfaceData.m_Color = rgba;
//	pSurface->m_SurfaceData.m_nIndex = 0;

	return 1;

}



// サーフェイスの削除
void  Nm2DArrayDeleteSurface( Nm2DSurfaceArray *pSurface )
{
	Nm2DArrayRemoveAll( pSurface );

	if(pSurface->m_pTexture!=NULL) pSurface->m_pTexture ->Release();
	pSurface->m_pTexture = NULL;
	
}



// 幅、高さを得る
int   Nm2DArrayGetWidth( Nm2DSurfaceArray *pSurface ){ return pSurface->m_szSize.cx; }
int   Nm2DArrayGetHeight( Nm2DSurfaceArray *pSurface ) { return pSurface->m_szSize.cy; }       

// 画像の原点
void  Nm2DArraySetOrigin( Nm2DSurfaceArray *pSurface, int x, int y ) { pSurface->m_poOffset.x = x; pSurface->m_poOffset.y = y; }
void  Nm2DArraySetOriginX( Nm2DSurfaceArray *pSurface, int x ) { pSurface->m_poOffset.x = x; }
void  Nm2DArraySetOriginY( Nm2DSurfaceArray *pSurface, int y ) { pSurface->m_poOffset.y = y;  }       
int   Nm2DArrayGetOriginX( Nm2DSurfaceArray *pSurface ) { return pSurface->m_poOffset.x; }       
int   Nm2DArrayGetOriginY( Nm2DSurfaceArray *pSurface ) { return pSurface->m_poOffset.y; } 

// 表示・非表示
void  Nm2DArraySetVisible( Nm2DSurfaceArray *pSurface, bool bVisible ) { pSurface->m_bVisible = bVisible; }       
bool  Nm2DArrayIsVisible( Nm2DSurfaceArray *pSurface ) { return pSurface->m_bVisible; }       



// 追加する
int Nm2DArrayAddSurface( Nm2DSurfaceArray *pSurface )
{
	int n;
	Nm2DSurfaceData *sur;
	sur = new Nm2DSurfaceData;
	Nm2DSurfaceDataInit( sur );
	n = pSurface->m_SurfaceData.Add( (CObject *)sur );

	return n; 
}

int Nm2DArrayAddSurface( Nm2DSurfaceArray *pSurface, float x, float y )
{
	int n;
	Nm2DSurfaceData *sur;
	sur = new Nm2DSurfaceData;
	Nm2DSurfaceDataInit( sur );
//	sur->m_poPos.x = x - pSurface->m_poOffset.x;
//	sur->m_poPos.y = y - pSurface->m_poOffset.y; 
	sur->m_poPos.x = x;
	sur->m_poPos.y = y; 
	n = pSurface->m_SurfaceData.Add( (CObject *)sur );

	return n; 
}

// すべてのサーフェイスを削除
void Nm2DArrayRemoveAll( Nm2DSurfaceArray *pSurface )
{
	for( int i=0 ; i < pSurface->m_SurfaceData.GetSize() ; i++ )
	{
		delete (Nm2DSurfaceData *)pSurface->m_SurfaceData[i];
	}
	pSurface->m_SurfaceData.RemoveAll();
}


// サーフェイスを削除
void Nm2DArrayRemove( Nm2DSurfaceArray *pSurface, int n )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 
	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		delete (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurface->m_SurfaceData.RemoveAt( n );
	}
}

// 非表示のサーフェイスを削除
void Nm2DArrayRemoveNonVisible( Nm2DSurfaceArray *pSurface )
{
	for( int i=0 ; i < pSurface->m_SurfaceData.GetSize() ;    )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[i];
		if( !pSurfaceData->m_bVisible )
		{
			delete (Nm2DSurfaceData *)pSurface->m_SurfaceData[i];
			pSurface->m_SurfaceData.RemoveAt( i );
		}
		else
		{
			i++;
		}
	}
}


// 要素数を得る
int Nm2DArrayGetSize( Nm2DSurfaceArray *pSurface )
{ 
	return pSurface->m_SurfaceData.GetSize(); 
}


// 画像転送
void Nm2DArrayRender( Nm2DSurfaceArray *pSurfaceArray ){
	if( !pSurfaceArray->m_bVisible ) return;

	// テクスチャをパイプラインにセット
	setTexture(pSurfaceArray->m_pTexture);

	MY_VERTEX *v = pSurfaceArray->v;

	g_pD3DDevice->SetFVF(MY_VERTEX_FVF);

	for( int i=0 ; i < pSurfaceArray->m_SurfaceData.GetSize() ; i++ )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurfaceArray->m_SurfaceData[i]; 
		if( !pSurfaceData->m_bVisible ) continue;

		D3DXMATRIX matTrans;
		D3DXMatrixTranslation( &matTrans, pSurfaceData->m_poPos.x, pSurfaceData->m_poPos.y, 0.0f );
		g_pD3DDevice->SetTransform( D3DTS_WORLD, &matTrans );

		D3DXVECTOR3 d[4];
		d[0] = D3DXVECTOR3( 0.0f, (float)pSurfaceArray->m_szSize.cy, 0.0f );
		d[1] = D3DXVECTOR3( (float)pSurfaceArray->m_szSize.cx, (float)pSurfaceArray->m_szSize.cy, 0.0f );
		d[2] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		d[3] = D3DXVECTOR3( (float)pSurfaceArray->m_szSize.cx, 0.0f, 0.0f );

		double fang = RAD(pSurfaceData->m_fAngle);
		float fcos = cosf(fang);
		float fsin = sinf(fang);		

		// 回転、拡大縮小 処理
		for (int i=0; i<4; i++) {
			float xxx, yyy;

			// 回転、拡縮 中心補正
			d[i].x -= pSurfaceArray->m_poOffset.x;
			d[i].y -= pSurfaceArray->m_poOffset.y;
			
			// 拡大
			d[i].x *= pSurfaceData->m_fScale_x;
			d[i].y *= pSurfaceData->m_fScale_y;		

			// 回転
			xxx = d[i].x * fcos - d[i].y * fsin;
			yyy = d[i].x * fsin + d[i].y * fcos;		
			d[i].x = xxx;
			d[i].y = yyy;

			// 回転、拡縮 中心補正
			d[i].x += (pSurfaceArray->m_poOffset.x + pSurfaceData->m_poPos.x);
			d[i].y += (pSurfaceArray->m_poOffset.y + pSurfaceData->m_poPos.y);

			v[i].p = d[i];
			v[i].color = pSurfaceData->m_Color;

			DWORD a = pSurfaceData->m_Color;
			a >>= 24;
			a = (DWORD)(a * NmGetAlpha());
			a <<= 24;
			v[i].color &= 0x00FFFFFF;
			v[i].color |= a;

		}
		int index = pSurfaceData->m_nIndex % ( pSurfaceArray->m_szDivision.cx * pSurfaceArray->m_szDivision.cy );
		v[0].t = D3DXVECTOR2( (float)pSurfaceArray->m_szSize.cx*(pSurfaceData->m_nIndex%pSurfaceArray->m_szDivision.cx)/pSurfaceArray->m_szTextureSize.cx, (float)pSurfaceArray->m_szSize.cy*(pSurfaceData->m_nIndex/pSurfaceArray->m_szDivision.cx+1)/pSurfaceArray->m_szTextureSize.cy  );
		v[1].t = D3DXVECTOR2( (float)pSurfaceArray->m_szSize.cx*(pSurfaceData->m_nIndex%pSurfaceArray->m_szDivision.cx+1)/pSurfaceArray->m_szTextureSize.cx, (float)pSurfaceArray->m_szSize.cy*(pSurfaceData->m_nIndex/pSurfaceArray->m_szDivision.cx+1)/pSurfaceArray->m_szTextureSize.cy );
		v[2].t = D3DXVECTOR2( (float)pSurfaceArray->m_szSize.cx*(pSurfaceData->m_nIndex%pSurfaceArray->m_szDivision.cx)/pSurfaceArray->m_szTextureSize.cx, (float)pSurfaceArray->m_szSize.cy*(pSurfaceData->m_nIndex/pSurfaceArray->m_szDivision.cx)/pSurfaceArray->m_szTextureSize.cy );
		v[3].t = D3DXVECTOR2( (float)pSurfaceArray->m_szSize.cx*(pSurfaceData->m_nIndex%pSurfaceArray->m_szDivision.cx+1)/pSurfaceArray->m_szTextureSize.cx, (float)pSurfaceArray->m_szSize.cy*(pSurfaceData->m_nIndex/pSurfaceArray->m_szDivision.cx)/pSurfaceArray->m_szTextureSize.cy );

		g_pD3DDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP,2,v,sizeof(MY_VERTEX) );	
	}
}


Nm2DSurfaceData * Nm2DArrayGetSurfaceData( Nm2DSurfaceArray *pSurface, int n )
{
	Nm2DSurfaceData * ret = NULL;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		ret = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
	}
	return ret;
}


// 座標をセットする（すべての要素の）
void Nm2DArraySetAll( Nm2DSurfaceArray *pSurface, float x, float y )
{
	for( int i=0 ; i < pSurface->m_SurfaceData.GetSize() ; i++ )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[i];
		pSurfaceData->m_poPos.x = x;
		pSurfaceData->m_poPos.y = y;
	}
}

// 座標をセットする（要素を指定して）
void Nm2DArraySet( Nm2DSurfaceArray *pSurface, int n, float x, float y )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurfaceData->m_poPos.x = x;
		pSurfaceData->m_poPos.y = y;
	}
}

// Ｘ座標をセットする（要素を指定して）
void Nm2DArraySetX( Nm2DSurfaceArray *pSurface, int n, float x )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurfaceData->m_poPos.x = x;
	}
}

// Ｙ座標をセットする（要素を指定して）
void Nm2DArraySetY( Nm2DSurfaceArray *pSurface, int n, float y )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurfaceData->m_poPos.y = y;
	}
}

// Ｘ座標を得る（要素を指定して）
float  Nm2DArrayGetX( Nm2DSurfaceArray *pSurface, int n )
{
	float x=0;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		x = pSurfaceData->m_poPos.x;
	}
	return x;
}

// Ｙ座標を得る（要素を指定して）
float  Nm2DArrayGetY( Nm2DSurfaceArray *pSurface, int n )
{
	float y=0;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		y = pSurfaceData->m_poPos.y;
	}
	return y;
}

// 左Ｘ座標をセットする（要素を指定して）
void Nm2DArraySetX1( Nm2DSurfaceArray *pSurface, int n, float x )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
//		pSurfaceData->m_poPos.x = x + pSurface->m_poOffset.x;
		pSurfaceData->m_poPos.x = x;
	}
}

// 上Ｙ座標をセットする（要素を指定して）
void Nm2DArraySetY1( Nm2DSurfaceArray *pSurface, int n, float y )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
//		pSurfaceData->m_poPos.y = y + pSurface->m_poOffset.y;
		pSurfaceData->m_poPos.y = y;
	}
}

// 右Ｘ座標をセットする（要素を指定して）
void Nm2DArraySetX2( Nm2DSurfaceArray *pSurface, int n, float x )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
//		pSurfaceData->m_poPos.x = x + pSurface->m_poOffset.x - pSurface->m_szSize.cx;
		pSurfaceData->m_poPos.x = x - pSurface->m_szSize.cx;
	}
}

// 下Ｙ座標をセットする（要素を指定して）
void Nm2DArraySetY2( Nm2DSurfaceArray *pSurface, int n, float y )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
//		pSurfaceData->m_poPos.y = y + pSurface->m_poOffset.y - pSurface->m_szSize.cy;
		pSurfaceData->m_poPos.y = y - pSurface->m_szSize.cy;
	}
}

// 左Ｘ座標を得る（要素を指定して）
float  Nm2DArrayGetX1( Nm2DSurfaceArray *pSurface, int n )
{
	float x=0;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
//		x = pSurfaceData->m_poPos.x - pSurface->m_poOffset.x;
		x = pSurfaceData->m_poPos.x;
	}
	return x;
}

// 上Ｙ座標を得る（要素を指定して）
float  Nm2DArrayGetY1( Nm2DSurfaceArray *pSurface, int n )
{
	float y=0;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
//		y = pSurfaceData->m_poPos.y - pSurface->m_poOffset.y;
		y = pSurfaceData->m_poPos.y;
	}
	return y;
}

// 右Ｘ座標を得る（要素を指定して）
float  Nm2DArrayGetX2( Nm2DSurfaceArray *pSurface, int n )
{
	float x=0;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
//		x = pSurfaceData->m_poPos.x - pSurface->m_poOffset.x + pSurface->m_szSize.cx;
		x = pSurfaceData->m_poPos.x + pSurface->m_szSize.cx;
	}
	return x;
}

// 下Ｙ座標を得る（要素を指定して）
float  Nm2DArrayGetY2( Nm2DSurfaceArray *pSurface, int n )
{
	float y=0;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
//		y = pSurfaceData->m_poPos.y - pSurface->m_poOffset.y + pSurface->m_szSize.cy;
		y = pSurfaceData->m_poPos.y + pSurface->m_szSize.cy;
	}
	return y;
}


// 座標を移動する（すべての要素の）
void Nm2DArrayMoveAll( Nm2DSurfaceArray *pSurface, float x, float y )
{
	for( int i=0 ; i < pSurface->m_SurfaceData.GetSize() ; i++ )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[i];
		pSurfaceData->m_poPos.x += x;
		pSurfaceData->m_poPos.y += y;
	}
}

// Ｘ座標を移動する（すべての要素の）
void Nm2DArrayMoveXAll( Nm2DSurfaceArray *pSurface, float x )
{
	for( int i=0 ; i < pSurface->m_SurfaceData.GetSize() ; i++ )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[i];
		pSurfaceData->m_poPos.x += x;
	}
}

// Ｙ座標を移動する（すべての要素の）
void Nm2DArrayMoveYAll( Nm2DSurfaceArray *pSurface, float y )
{
	for( int i=0 ; i < pSurface->m_SurfaceData.GetSize() ; i++ )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[i];
		pSurfaceData->m_poPos.y += y;
	}
}


// 座標を移動する（要素を指定して）
FPoint Nm2DArrayMove( Nm2DSurfaceArray *pSurface, int n, float x, float y )
{
	FPoint ret;
	ret.x = ret.y = 0;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		ret.x = pSurfaceData->m_poPos.x += x;
		ret.y = pSurfaceData->m_poPos.y += y;
	}
	return ret;
}

// Ｘ座標を移動する（要素を指定して）
float   Nm2DArrayMoveX( Nm2DSurfaceArray *pSurface, int n, float x )
{
	float ret=0;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		ret = pSurfaceData->m_poPos.x += x;
	}
	return ret;
}

// Ｙ座標を移動する（要素を指定して）
float   Nm2DArrayMoveY( Nm2DSurfaceArray *pSurface, int n, float y )
{
	float ret=0;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		ret = pSurfaceData->m_poPos.y += y;
	}
	return ret;
}



// 表示状態をセットする（要素を指定して）
void Nm2DArraySetVisible( Nm2DSurfaceArray *pSurface, int n, bool bVisible )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurfaceData->m_bVisible = bVisible;
	}
}

// 表示状態を得る（要素を指定して）
bool Nm2DArrayIsVisible( Nm2DSurfaceArray *pSurface, int n )
{
	bool ret=FALSE;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		ret = pSurfaceData->m_bVisible;
	}
	return ret;
}



// 回転角度をセットする（要素を指定して）
void Nm2DArraySetAngle( Nm2DSurfaceArray *pSurface, int n, float angle )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurfaceData->m_fAngle = angle;
	}
}

// 回転角度を加算する（要素を指定して）
void Nm2DArrayAddAngle( Nm2DSurfaceArray *pSurface, int n, float angle )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurfaceData->m_fAngle += angle;
	}
}

// 回転角度を得る（要素を指定して）
float Nm2DArrayGetAngle( Nm2DSurfaceArray *pSurface, int n )
{
	float ret=0.0f;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		ret = pSurfaceData->m_fAngle;
	}
	return ret;
}


// 拡大縮小量をセットする（要素を指定して）
void Nm2DArraySetScale( Nm2DSurfaceArray *pSurface, int n, float sc )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurfaceData->m_fScale_x = sc;
		pSurfaceData->m_fScale_y = sc;
	}
}

// Ｘ軸方向拡大縮小量をセットする（要素を指定して）
void Nm2DArraySetScaleX( Nm2DSurfaceArray *pSurface, int n, float sc )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurfaceData->m_fScale_x = sc;
	}
}

// Ｙ軸方向拡大縮小量をセットする（要素を指定して）
void Nm2DArraySetScaleY( Nm2DSurfaceArray *pSurface, int n, float sc )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurfaceData->m_fScale_y = sc;
	}
}

// Ｘ軸方向拡大縮小量を得る（要素を指定して）
float Nm2DArrayGetScaleX( Nm2DSurfaceArray *pSurface, int n )
{
	float ret=0.0f;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		ret = pSurfaceData->m_fScale_x;
	}
	return ret;
}

// Ｙ軸方向拡大縮小量を得る（要素を指定して）
float Nm2DArrayGetScaleY( Nm2DSurfaceArray *pSurface, int n )
{
	float ret=0.0f;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		ret = pSurfaceData->m_fScale_y;
	}
	return ret;
}


// 頂点カラーをセットする（要素を指定して）
void Nm2DArraySetColor( Nm2DSurfaceArray *pSurface, int n, D3DCOLOR color )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurfaceData->m_Color = color;
	}
}

// 頂点カラーを得る（要素を指定して）
D3DCOLOR Nm2DArrayGetColor( Nm2DSurfaceArray *pSurface, int n )
{
	D3DCOLOR ret=0;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		ret = pSurfaceData->m_Color;
	}
	return ret;
}

void Nm2DSetIndex( Nm2DSurface *pSurface, int index){
	pSurface->m_SurfaceData.m_nIndex = index;
}

int Nm2DGetIndex( Nm2DSurface *pSurface){
	return pSurface->m_SurfaceData.m_nIndex;	
}


// インデックス番号をセットする（要素を指定して）
void Nm2DArraySetIndex( Nm2DSurfaceArray *pSurface, int n, int index )
{
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		pSurfaceData->m_nIndex = index;
	}
}

// インデックス番号を得る（要素を指定して）
int Nm2DArrayGetIndex( Nm2DSurfaceArray *pSurface, int n )
{
	int ret=0;
	ASSERT( 0 <= n && n < pSurface->m_SurfaceData.GetSize() ); 

	if( 0 <= n && n < pSurface->m_SurfaceData.GetSize() )
	{
		Nm2DSurfaceData *pSurfaceData = (Nm2DSurfaceData *)pSurface->m_SurfaceData[n];
		ret = pSurfaceData->m_nIndex;
	}
	return ret;
}




// 当り判定チェック（配列と単体）
BOOL  Nm2DArrayCollideCheck( Nm2DSurfaceArray *surSource, Nm2DSurface *surTarget, int nCollideID )
{
	int i,j;
	BOOL ret=FALSE;
	if( !Nm2DIsVisible(surTarget) ) return ret;

	// ターゲット側の座標を求める
	D3DXVECTOR3 t[4];
	int ang;
	ang = (int)surTarget->m_SurfaceData.m_fAngle % 360;
	if( ang < 0 ) ang += 360;
	if( ang < 90  )
	{
		t[0] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		t[1] = D3DXVECTOR3( 0.0f, (float)surTarget->m_szSize.cy, 0.0f );
		t[2] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, (float)surTarget->m_szSize.cy, 0.0f );
		t[3] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, 0.0f, 0.0f );
	}
	else if( 90 <= ang && ang < 180 )
	{
		t[3] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		t[0] = D3DXVECTOR3( 0.0f, (float)surTarget->m_szSize.cy, 0.0f );
		t[1] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, (float)surTarget->m_szSize.cy, 0.0f );
		t[2] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, 0.0f, 0.0f );
	}
	else if( 180 <= ang && ang < 270 )
	{
		t[2] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		t[3] = D3DXVECTOR3( 0.0f, (float)surTarget->m_szSize.cy, 0.0f );
		t[0] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, (float)surTarget->m_szSize.cy, 0.0f );
		t[1] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, 0.0f, 0.0f );
	}
	else if( 270 <= ang && ang < 360 )
	{
		t[1] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
		t[2] = D3DXVECTOR3( 0.0f, (float)surTarget->m_szSize.cy, 0.0f );
		t[3] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, (float)surTarget->m_szSize.cy, 0.0f );
		t[0] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, 0.0f, 0.0f );
	}

	double fang = RAD(surTarget->m_SurfaceData.m_fAngle);
	float fcos = cosf(fang);
	float fsin = sinf(fang);		

	for ( i=0; i<4; i++) {
		double xxx, yyy;

		// 回転、拡縮 中心補正
		t[i].x -= surTarget->m_poOffset.x;
		t[i].y -= surTarget->m_poOffset.y;
		
		// 拡大
		t[i].x *= surTarget->m_SurfaceData.m_fScale_x;
		t[i].y *= surTarget->m_SurfaceData.m_fScale_y;		

		// 回転
		xxx = t[i].x * fcos - t[i].y * fsin;
		yyy = t[i].x * fsin + t[i].y * fcos;		
		t[i].x = (float)xxx;
		t[i].y = (float)yyy;

		// 回転、拡縮 中心補正
		t[i].x += (surTarget->m_poOffset.x + surTarget->m_SurfaceData.m_poPos.x);
		t[i].y += (surTarget->m_poOffset.y + surTarget->m_SurfaceData.m_poPos.y);
	}

	if( t[0].x<0 && t[1].x<0 && t[2].x<0 && t[3].x<0 ) return FALSE;
	if( t[0].y<0 && t[1].y<0 && t[2].y<0 && t[3].y<0 ) return FALSE;
	if( t[0].x>=SCREEN_WIDTH && t[1].x>=SCREEN_WIDTH && t[2].x>=SCREEN_WIDTH && t[3].x>=SCREEN_WIDTH ) return FALSE;
	if( t[0].y>=SCREEN_HEIGHT && t[1].y>=SCREEN_HEIGHT && t[2].y>=SCREEN_HEIGHT && t[3].y>=SCREEN_HEIGHT ) return FALSE;
	
	
	
	for( int s=0 ; s < surSource->m_SurfaceData.GetSize() ; s++ )
	{
		if( !Nm2DArrayIsVisible( surSource, s ) ) continue;

		// ソース側の座標を求める
		Nm2DSurfaceData *pSurfaceSData = (Nm2DSurfaceData *)surSource->m_SurfaceData[s];
		D3DXVECTOR3 d[4];
		int ang = (int)pSurfaceSData->m_fAngle % 360;
		if( ang < 0 ) ang += 360;
		if( ang < 45 || ang >= 315 )
		{
			d[0] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			d[1] = D3DXVECTOR3( 0.0f, (float)surSource->m_szSize.cy, 0.0f );
			d[2] = D3DXVECTOR3( (float)surSource->m_szSize.cx, (float)surSource->m_szSize.cy, 0.0f );
			d[3] = D3DXVECTOR3( (float)surSource->m_szSize.cx, 0.0f, 0.0f );
		}
		else if( 45 <= ang && ang < 135 )
		{
			d[3] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			d[0] = D3DXVECTOR3( 0.0f, (float)surSource->m_szSize.cy, 0.0f );
			d[1] = D3DXVECTOR3( (float)surSource->m_szSize.cx, (float)surSource->m_szSize.cy, 0.0f );
			d[2] = D3DXVECTOR3( (float)surSource->m_szSize.cx, 0.0f, 0.0f );
		}
		else if( 135 <= ang && ang < 225 )
		{
			d[2] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			d[3] = D3DXVECTOR3( 0.0f, (float)surSource->m_szSize.cy, 0.0f );
			d[0] = D3DXVECTOR3( (float)surSource->m_szSize.cx, (float)surSource->m_szSize.cy, 0.0f );
			d[1] = D3DXVECTOR3( (float)surSource->m_szSize.cx, 0.0f, 0.0f );
		}
		else if( 225 <= ang && ang < 315 )
		{
			d[1] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			d[2] = D3DXVECTOR3( 0.0f, (float)surSource->m_szSize.cy, 0.0f );
			d[3] = D3DXVECTOR3( (float)surSource->m_szSize.cx, (float)surSource->m_szSize.cy, 0.0f );
			d[0] = D3DXVECTOR3( (float)surSource->m_szSize.cx, 0.0f, 0.0f );
		}

		double fang = RAD(pSurfaceSData->m_fAngle);
		float fcos = cosf(fang);
		float fsin = sinf(fang);		

		for ( i=0; i<4; i++) {
			double xxx, yyy;

			// 回転、拡縮 中心補正
			d[i].x -= surSource->m_poOffset.x;
			d[i].y -= surSource->m_poOffset.y;
			
			// 拡大
			d[i].x *= pSurfaceSData->m_fScale_x;
			d[i].y *= pSurfaceSData->m_fScale_y;		

			// 回転
			xxx = d[i].x * fcos - d[i].y * fsin;
			yyy = d[i].x * fsin + d[i].y * fcos;		
			d[i].x = (float)xxx;
			d[i].y = (float)yyy;

			// 回転、拡縮 中心補正
			d[i].x += (surSource->m_poOffset.x + pSurfaceSData->m_poPos.x);
			d[i].y += (surSource->m_poOffset.y + pSurfaceSData->m_poPos.y);
		}

		if( d[0].x<0 && d[1].x<0 && d[2].x<0 && d[3].x<0 ) continue;
		if( d[0].y<0 && d[1].y<0 && d[2].y<0 && d[3].y<0 ) continue;
		if( d[0].x>=SCREEN_WIDTH && d[1].x>=SCREEN_WIDTH && d[2].x>=SCREEN_WIDTH && d[3].x>=SCREEN_WIDTH ) continue;
		if( d[0].y>=SCREEN_HEIGHT && d[1].y>=SCREEN_HEIGHT && d[2].y>=SCREEN_HEIGHT && d[3].y>=SCREEN_HEIGHT ) continue;


		
		// 四角形の中に頂点が入っているか？
		if( IsRectIn( t, d[0] ) 
			|| IsRectIn( t, d[1] ) 
			|| IsRectIn( t, d[2] ) 
			|| IsRectIn( t, d[3] ) 
			|| IsRectIn( d, t[0] ) 
			|| IsRectIn( d, t[1] ) 
			|| IsRectIn( d, t[2] ) 
			|| IsRectIn( d, t[3] ) )
		{
			CollideCallback( s, 0, nCollideID );
			ret = TRUE;
			continue;
		}


		// 線分が重なっているか？
		for( i=0 ; i<4 ; i++ )
		{
			int ii = (i+1)%4;
			for( j=0 ; j<4 ; j++ )
			{
				int jj = (j+1)%4;
				float sx1, sx2, sy1, sy2, tx1, tx2, ty1, ty2;
				sx1 = d[i].x; sx2 = d[ii].x;
				sy1 = d[i].y; sy2 = d[ii].y;
				tx1 = t[j].x; tx2 = t[jj].x;
				ty1 = t[j].y; ty2 = t[jj].y;

				float a1 = sy2 - sy1;
				float b1 = -(sx2 - sx1);
				float c1 = -sx1*(sy2-sy1)+sy1*(sx2-sx1);
				float a2 = ty2 - ty1;
				float b2 = -(tx2 - tx1);
				float c2 = -tx1*(ty2-ty1)+ty1*(tx2-tx1);

				if( ( a1 * b2 - a2 * b1 ) == 0 ) continue;
				float x0 = ( b1*c2 - b2*c1 ) / ( a1 * b2 - a2 * b1 );
				float y0 = ( a2*c1 - a1*c2 ) / ( a1 * b2 - a2 * b1 );

				float minsx = min( sx1, sx2);
				float maxsx = max( sx1, sx2);
				float minsy = min( sy1, sy2);
				float maxsy = max( sy1, sy2);
				float mintx = min( tx1, tx2);
				float maxtx = max( tx1, tx2);
				float minty = min( ty1, ty2);
				float maxty = max( ty1, ty2);
				if( ( minsy <= y0 ) && ( y0 <= maxsy ) && ( minsx <= x0 ) && ( x0 <= maxsx )
					&& ( minty <= y0 ) && ( y0 <= maxty ) && ( mintx <= x0 ) && ( x0 <= maxtx )) 
				{
					CollideCallback( s, 0, nCollideID );
					ret = TRUE;
					break;
				}
			}
			if( j<4 ) break;
		}
		if( i<4 ) continue;

	}
	return ret;
}


// 当り判定チェック（配列と配列）
BOOL  Nm2DArrayCollideCheck( Nm2DSurfaceArray *surSource, Nm2DSurfaceArray *surTarget, int nCollideID )
{
	int i;
	int j;
	BOOL ret=FALSE;
	for( int ta = 0 ; ta < surTarget->m_SurfaceData.GetSize() ; ta++ )
	{
		if( !Nm2DArrayIsVisible( surTarget, ta ) ) continue;

		Nm2DSurfaceData *pSurfaceTData = (Nm2DSurfaceData *)surTarget->m_SurfaceData[ta];

		// ターゲット側の座標を求める
		D3DXVECTOR3 t[4];
		int ang;
		ang = (int)pSurfaceTData->m_fAngle % 360;
		if( ang < 0 ) ang += 360;
		if( ang < 90  )
		{
			t[0] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			t[1] = D3DXVECTOR3( 0.0f, (float)surTarget->m_szSize.cy, 0.0f );
			t[2] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, (float)surTarget->m_szSize.cy, 0.0f );
			t[3] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, 0.0f, 0.0f );
		}
		else if( 90 <= ang && ang < 180 )
		{
			t[3] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			t[0] = D3DXVECTOR3( 0.0f, (float)surTarget->m_szSize.cy, 0.0f );
			t[1] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, (float)surTarget->m_szSize.cy, 0.0f );
			t[2] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, 0.0f, 0.0f );
		}
		else if( 180 <= ang && ang < 270 )
		{
			t[2] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			t[3] = D3DXVECTOR3( 0.0f, (float)surTarget->m_szSize.cy, 0.0f );
			t[0] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, (float)surTarget->m_szSize.cy, 0.0f );
			t[1] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, 0.0f, 0.0f );
		}
		else if( 270 <= ang && ang < 360 )
		{
			t[1] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			t[2] = D3DXVECTOR3( 0.0f, (float)surTarget->m_szSize.cy, 0.0f );
			t[3] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, (float)surTarget->m_szSize.cy, 0.0f );
			t[0] = D3DXVECTOR3( (float)surTarget->m_szSize.cx, 0.0f, 0.0f );
		}

		double fang = RAD(pSurfaceTData->m_fAngle);
		float fcos = cosf(fang);
		float fsin = sinf(fang);		

		for ( i=0; i<4; i++) {
			double xxx, yyy;

			// 回転、拡縮 中心補正
			t[i].x -= surTarget->m_poOffset.x;
			t[i].y -= surTarget->m_poOffset.y;
			
			// 拡大
			t[i].x *= pSurfaceTData->m_fScale_x;
			t[i].y *= pSurfaceTData->m_fScale_y;		

			// 回転
			xxx = t[i].x * fcos - t[i].y * fsin;
			yyy = t[i].x * fsin + t[i].y * fcos;		
			t[i].x = (float)xxx;
			t[i].y = (float)yyy;

			// 回転、拡縮 中心補正
			t[i].x += (surTarget->m_poOffset.x + pSurfaceTData->m_poPos.x);
			t[i].y += (surTarget->m_poOffset.y + pSurfaceTData->m_poPos.y);
		}

		if( t[0].x<0 && t[1].x<0 && t[2].x<0 && t[3].x<0 ) continue;
		if( t[0].y<0 && t[1].y<0 && t[2].y<0 && t[3].y<0 ) continue;
		if( t[0].x>=SCREEN_WIDTH && t[1].x>=SCREEN_WIDTH && t[2].x>=SCREEN_WIDTH && t[3].x>=SCREEN_WIDTH ) continue;
		if( t[0].y>=SCREEN_HEIGHT && t[1].y>=SCREEN_HEIGHT && t[2].y>=SCREEN_HEIGHT && t[3].y>=SCREEN_HEIGHT ) continue;

		
		for( int s=0 ; s < surSource->m_SurfaceData.GetSize() ; s++ )
		{
			if( !Nm2DArrayIsVisible( surSource, s ) ) continue;

			Nm2DSurfaceData *pSurfaceSData = (Nm2DSurfaceData *)surSource->m_SurfaceData[s];

			// ソース側の座標を求める
			D3DXVECTOR3 d[4];
			int ang = (int)pSurfaceSData->m_fAngle % 360;
			if( ang < 0 ) ang += 360;
			if( ang < 45 || ang >= 315 )
			{
				d[0] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
				d[1] = D3DXVECTOR3( 0.0f, (float)surSource->m_szSize.cy, 0.0f );
				d[2] = D3DXVECTOR3( (float)surSource->m_szSize.cx, (float)surSource->m_szSize.cy, 0.0f );
				d[3] = D3DXVECTOR3( (float)surSource->m_szSize.cx, 0.0f, 0.0f );
			}
			else if( 45 <= ang && ang < 135 )
			{
				d[3] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
				d[0] = D3DXVECTOR3( 0.0f, (float)surSource->m_szSize.cy, 0.0f );
				d[1] = D3DXVECTOR3( (float)surSource->m_szSize.cx, (float)surSource->m_szSize.cy, 0.0f );
				d[2] = D3DXVECTOR3( (float)surSource->m_szSize.cx, 0.0f, 0.0f );
			}
			else if( 135 <= ang && ang < 225 )
			{
				d[2] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
				d[3] = D3DXVECTOR3( 0.0f, (float)surSource->m_szSize.cy, 0.0f );
				d[0] = D3DXVECTOR3( (float)surSource->m_szSize.cx, (float)surSource->m_szSize.cy, 0.0f );
				d[1] = D3DXVECTOR3( (float)surSource->m_szSize.cx, 0.0f, 0.0f );
			}
			else if( 225 <= ang && ang < 315 )
			{
				d[1] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
				d[2] = D3DXVECTOR3( 0.0f, (float)surSource->m_szSize.cy, 0.0f );
				d[3] = D3DXVECTOR3( (float)surSource->m_szSize.cx, (float)surSource->m_szSize.cy, 0.0f );
				d[0] = D3DXVECTOR3( (float)surSource->m_szSize.cx, 0.0f, 0.0f );
			}

			double fang = RAD(pSurfaceSData->m_fAngle);
			float fcos = cosf(fang);
			float fsin = sinf(fang);		

			for ( i=0; i<4; i++) {
				double xxx, yyy;

				// 回転、拡縮 中心補正
				d[i].x -= surSource->m_poOffset.x;
				d[i].y -= surSource->m_poOffset.y;
				
				// 拡大
				d[i].x *= pSurfaceSData->m_fScale_x;
				d[i].y *= pSurfaceSData->m_fScale_y;		

				// 回転
				xxx = d[i].x * fcos - d[i].y * fsin;
				yyy = d[i].x * fsin + d[i].y * fcos;		
				d[i].x = (float)xxx;
				d[i].y = (float)yyy;

				// 回転、拡縮 中心補正
				d[i].x += (surSource->m_poOffset.x + pSurfaceSData->m_poPos.x);
				d[i].y += (surSource->m_poOffset.y + pSurfaceSData->m_poPos.y);
			}

			if( d[0].x<0 && d[1].x<0 && d[2].x<0 && d[3].x<0 ) continue;
			if( d[0].y<0 && d[1].y<0 && d[2].y<0 && d[3].y<0 ) continue;
			if( d[0].x>=SCREEN_WIDTH && d[1].x>=SCREEN_WIDTH && d[2].x>=SCREEN_WIDTH && d[3].x>=SCREEN_WIDTH ) continue;
			if( d[0].y>=SCREEN_HEIGHT && d[1].y>=SCREEN_HEIGHT && d[2].y>=SCREEN_HEIGHT && d[3].y>=SCREEN_HEIGHT ) continue;



			// 四角形の中に頂点が入っているか？
			if( IsRectIn( t, d[0] ) 
				|| IsRectIn( t, d[1] ) 
				|| IsRectIn( t, d[2] ) 
				|| IsRectIn( t, d[3] ) 
				|| IsRectIn( d, t[0] ) 
				|| IsRectIn( d, t[1] ) 
				|| IsRectIn( d, t[2] ) 
				|| IsRectIn( d, t[3] ) )
			{
				CollideCallback( s, ta, nCollideID );
				ret = TRUE;
				continue;
			}


			// 線分が重なっているか？
			for( i=0 ; i<4 ; i++ )
			{
				int ii = (i+1)%4;
				for(  j=0 ; j<4 ; j++ )
				{
					int jj = (j+1)%4;
					float sx1, sx2, sy1, sy2, tx1, tx2, ty1, ty2;
					sx1 = d[i].x; sx2 = d[ii].x;
					sy1 = d[i].y; sy2 = d[ii].y;
					tx1 = t[j].x; tx2 = t[jj].x;
					ty1 = t[j].y; ty2 = t[jj].y;

					float a1 = sy2 - sy1;
					float b1 = -(sx2 - sx1);
					float c1 = -sx1*(sy2-sy1)+sy1*(sx2-sx1);
					float a2 = ty2 - ty1;
					float b2 = -(tx2 - tx1);
					float c2 = -tx1*(ty2-ty1)+ty1*(tx2-tx1);

					if( ( a1 * b2 - a2 * b1 ) == 0 ) continue;
					float x0 = ( b1*c2 - b2*c1 ) / ( a1 * b2 - a2 * b1 );
					float y0 = ( a2*c1 - a1*c2 ) / ( a1 * b2 - a2 * b1 );

					float minsx = min( sx1, sx2);
					float maxsx = max( sx1, sx2);
					float minsy = min( sy1, sy2);
					float maxsy = max( sy1, sy2);
					float mintx = min( tx1, tx2);
					float maxtx = max( tx1, tx2);
					float minty = min( ty1, ty2);
					float maxty = max( ty1, ty2);
					if( ( minsy <= y0 ) && ( y0 <= maxsy ) && ( minsx <= x0 ) && ( x0 <= maxsx )
						&& ( minty <= y0 ) && ( y0 <= maxty ) && ( mintx <= x0 ) && ( x0 <= maxtx )) 
					{
						CollideCallback( s, ta, nCollideID );
						ret = TRUE;
						break;
					}
				}
				if( j<4 ) break;
			}
			if( i<4 ) continue;

			
//			if( rc.IntersectRect( &rcSrc, &rcTarget ) )
//			{
//				m_pGameMain->CollideCallback( i, j, nCollideID );
//				ret = TRUE;
//			}
//
		}
	}
	return ret;

}


