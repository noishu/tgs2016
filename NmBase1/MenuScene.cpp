#include "NmBase1.h"
#include "GameMain.h"
#include "MenuScene.h"

//ゲーム独自の変数(他のファイルで宣言した変数・配列にはexternをつける)
extern NmInput input;		

//アニメーション変数
int animation(Nm2DSurface* surf, int end, int reverse);
int animation(Nm2DSurfaceArray* surf, int id, int end, int recerse);
#define BIAS 10
int gCnt, aCnt;
//メニュー項目のシーン番号の配列
#define MENU_MAX 5
extern SCENE_NO menu[MENU_MAX] = { SCENE_GAME1};
//選択されたゲームを表すメニュー番号の初期化（menuの添え字）
Nm2DSurface taitolSurf;
Nm2DSurface startSurf;
Nm2DSurface back1Surf;
Nm2DSurface corsol1Surf;

//文字の設定
NmFont MenuFont;
Nm2DSurface STARTSurf;
Nm2DSurface taitolofSurf;
Nm2DSurface carsol1Surf;
//曲の設定
NmMusic TemaSong;

//シーン開始前の初期化を行う
BOOL initMenuScene(void){

	NmFontInit(&MenuFont);
	NmFontCreate(&MenuFont,36,"");
	Nm2DInit(&back1Surf);
	Nm2DOpen(&back1Surf, "res/haikei2.png", 5120, 720);
	Nm2DInit(&STARTSurf);
	Nm2DOpen(&STARTSurf, "res/STARTKye.png", 493, 110, 4, 1, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&taitolofSurf);
	Nm2DOpen(&taitolofSurf, "res/taitol.png", 384, 105, D3DCOLOR_RGBA(0, 0, 0, 0));
	Nm2DInit(&carsol1Surf);
	Nm2DOpen(&carsol1Surf, "res/crasol.png", 50, 50, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&taitolofSurf);
	Nm2DOpen(&taitolofSurf, "res/taitol.png",
		384, 105, D3DCOLOR_RGBA(0, 255, 0, 0));
	NmMusicInit(&TemaSong);
	NmMusicOpen(&TemaSong, "res/MOUSIC/tema/game_maoudamashii_3_theme08.mid", 0);
	NmMusicPlay(&TemaSong);

	return TRUE;
}

//フレーム処理
void moveMenuScene(DWORD dwTickDiff){
	if (gCnt++ > 50){ 
		gCnt = 0; //再度カウントしてもらえるようにする
		aCnt++;
	}
	//アニメーション
	Nm2DSetIndex(&STARTSurf, BIAS + aCnt);

	//背景移動
	Nm2DMoveX(&back1Surf, -0.5f);
	if (Nm2DGetX(&back1Surf)<-5120){
		//右端まで移動
		Nm2DSetX(&back1Surf, 1280 + 5120);
	}
	//クリックされたら次のシーンへ移動
	if (GetMouseButtonLWait()){
		if (GetMouseX() > 400 && GetMouseX() < 400 + 493 &&
			GetMouseY() > 500 && GetMouseY() < 500 + 110){
			changeScene(menu[SCENE_GAME1]);
		}
	}
	
}

//レンダリング処理
void renderMenuScene(void){

	//NmFontDrawText(&MenuFont,"メニュー画面",30,50,D3DCOLOR_RGBA(255,255,255,255));
	//NmFontDrawText(&MenuFont,"左クリックで移動",30,400,D3DCOLOR_RGBA(255,255,255,255));
	Nm2DRender(&back1Surf);
	//タイトル文字
	Nm2DSet(&taitolofSurf, 450, 200);
	Nm2DRender(&taitolofSurf);
	Nm2DSet(&STARTSurf, 400, 500);
	Nm2DRender(&STARTSurf);
	Nm2DSet(&carsol1Surf, GetMouseX(), GetMouseY());
	Nm2DRender(&carsol1Surf);
	//６(2) メニュー項目の表示
	//int x = 500, y = 300, gapY = 80;	//（x,y)：表示開始座標　gapY：行の高さ
	//for( int i = 0; i < MENU_MAX; i++, y += gapY ){
	//	
		//６(2) �@選択された項目の表示
	//	if( i == selectedGame ){
	//		NmFontDrawText(&MenuFont,menuList[i],x,y,D3DCOLOR_RGBA(255,0,0,255));
	//	//６(2) �A選択されていない項目の表示
	///	}else{
		//	NmFontDrawText(&MenuFont,menuList[i],x,y,D3DCOLOR_RGBA(255,255,255,255));
		//}
	//	
//	}
}

//シーン終了時の後処理
void releaseMenuScene(void){
	NmFontDelete(&MenuFont);
	Nm2DDeleteSurface(&back1Surf);
	Nm2DDeleteSurface(&taitolofSurf);
	Nm2DDeleteSurface(&STARTSurf);
	Nm2DDeleteSurface(&carsol1Surf);
	NmMusicDelete(&TemaSong);
}

//当り判定コールバック 　　　ここでは要素を削除しないこと！！
void  MenuSceneCollideCallback( int nSrc, int nTarget, int nCollideID ){
	/*if (GetMouseButtonLWait()){
		if (GetMouseX() > 500 && GetMouseX() < 500+320 &&
			GetMouseY() > 500 && GetMouseY() < 500+100){
			changeScene(menu[SCENE_GAME1]);
		}
	}*/
}
//アニメーション関数の実装
int animation(Nm2DSurface* surf, int end, int reverse){
	//画像の示す番号の加算
	Nm2DSetIndex(surf, Nm2DGetIndex(surf) + 1);
	//エンドに指定したIndex番号に来たらループ先に行く
	if (Nm2DGetIndex(surf) > end){
		Nm2DSetIndex(surf, reverse);
	}
		return Nm2DGetIndex(surf);
}