#include "Enemy.h"

player enemy;
extern int enemyMaxNom;
extern unit UnitE[MonsterMax];
extern int DropF;

int Ecool1 = 100;	//スライムスポーン用coolTime(基本)
int Ecool2 = 150;	//ゴブリンスポーン用coolTime(基本)
int Ecool3 = 300;	//ゴーレムスポーン用coolTime(基本)
int Ecool4 = 500;	//ドラゴンスポーン用coolTime(基本)
int Ecool5 = 0;		//
int Ecool6 = 0;		//
int Ecool7 = 0;		//
int Ecool8 = 0;		//
int enemyHpMax;

void enemyInit(){
	if (DropF == 1){
		enemy.PlayerHp = HpStart;
	}
	if (DropF == 2){
		enemy.PlayerHp = HpStart;
		enemy.PlayerHp += (HpStart / 2);
	}
	if (DropF == 3){
		enemy.PlayerHp = HpStart;
		enemy.PlayerHp += HpStart;
	}
	enemyMaxNom = 0;
	enemy.PlayerCnt = 0;
	enemyHpMax = enemy.PlayerHp;
	Nm2DInit(&enemy.PlayerBaceSurf);
	Nm2DOpen(&enemy.PlayerBaceSurf, "res/BattlWall.png", 100, 100, 2, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DSetScale(&enemy.PlayerBaceSurf, 2);
	Nm2DSetIndex(&enemy.PlayerBaceSurf, 1);
}
void enemyUpdata(int time){
	if (DropF == 1){
		if (time % Ecool1 == 0){
			if (enemy.PlayerCnt < MonsterMax){
				enemy.PlayerCnt = UnitAliveChack(EnemyM);
				if (UnitE[enemy.PlayerCnt].UnitExit == false){
					Unitspawn(&enemy.PlayerCnt, Slime, EnemyM);
				}
			}
		}
	}
	if (DropF == 2){
		if (time % Ecool2 == 0){
			if (enemy.PlayerCnt < MonsterMax){
				enemy.PlayerCnt = UnitAliveChack(EnemyM);
				if (UnitE[enemy.PlayerCnt].UnitExit == false){
					Unitspawn(&enemy.PlayerCnt, Slime, EnemyM);
				}
			}
		}
		if (time % Ecool3 == 0){
			if (enemy.PlayerCnt < MonsterMax){
				enemy.PlayerCnt = UnitAliveChack(EnemyM);
				if (UnitE[enemy.PlayerCnt].UnitExit == false){
					Unitspawn(&enemy.PlayerCnt, goblin, EnemyM);
				}
			}
		}
	}
	if (DropF == 3){
		if (time % Ecool4 == 0){
			if (enemy.PlayerCnt < MonsterMax){
				enemy.PlayerCnt = UnitAliveChack(EnemyM);
				if (UnitE[enemy.PlayerCnt].UnitExit == false){
					Unitspawn(&enemy.PlayerCnt, Slime, EnemyM);
				}
			}
		}
		if (time % Ecool5 == 0){
			if (enemy.PlayerCnt < MonsterMax){
				enemy.PlayerCnt = UnitAliveChack(EnemyM);
				if (UnitE[enemy.PlayerCnt].UnitExit == false){
					Unitspawn(&enemy.PlayerCnt, goblin, EnemyM);
				}
			}
		}
		if (time % Ecool6 == 0){
			if (enemy.PlayerCnt < MonsterMax){
				enemy.PlayerCnt = UnitAliveChack(EnemyM);
				if (UnitE[enemy.PlayerCnt].UnitExit == false){
					Unitspawn(&enemy.PlayerCnt, golem, EnemyM);
				}
			}
		}
	}
}
void enemyRender(){
	Nm2DSet(&enemy.PlayerBaceSurf, 50, 200);
	Nm2DRender(&enemy.PlayerBaceSurf);
	
	
}
void enemyDelete(){
	Nm2DSetVisible(&enemy.PlayerBaceSurf, false);
	Nm2DDeleteSurface(&enemy.PlayerBaceSurf);
}
void enemySumon(int time){
}
