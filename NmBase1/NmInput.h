/********************************************************/
/* DirectX  ゲームプログラミング						*/
/*						ゲームライブラリー				*/
/*														*/
/* NmInput.h - 入力										*/
/*														*/
/*		オリジナル		　			  Oikawa			*/
/*		改編		沼津情報専門学校　Atsushi Ohba		*/
/*						Copyright (c) 2003-2004			*/
/*						All Rights Reserved.			*/
/********************************************************/

/////////////////////////////////////////////////////////////////////////////
// NmInput
#pragma once

struct SKeyData{int Up,Down,Right,Left,Botton1,Botton2,Botton3,Botton4;};

typedef struct _NmInput
{
	JOYCAPS m_joyCaps;
	static SKeyData staticKeyNull;
	SKeyData m_KeyData[3];

	int m_nPush[10];
	int m_nPush2[10];

	BOOL m_bU;
	BOOL m_bR;
	BOOL m_bD;
	BOOL m_bL;
	int m_nButtons;
	UINT m_nJoyPort;

}NmInput;


void NmInputCreate( NmInput *pInput, int nJoyPort, int nKey);
void NmInputGet( NmInput *pInput );
BOOL NmInputGetKeyStat(NmInput *pInput,int nIndex);
BOOL NmInputGetKeyStat2(NmInput *pInput,int nIndex);
void NmInputSetKeyData(NmInput *pInput, int nIndex,SKeyData* pKeyData);

BOOL NmInputIsUp(NmInput *pInput);
BOOL NmInputIsDown(NmInput *pInput);
BOOL NmInputIsLeft(NmInput *pInput);
BOOL NmInputIsRight(NmInput *pInput);
BOOL NmInputIsButton1(NmInput *pInput);
BOOL NmInputIsButton2(NmInput *pInput);
BOOL NmInputIsButton3(NmInput *pInput);
BOOL NmInputIsButton4(NmInput *pInput);

BOOL NmInputIsUpWait(NmInput *pInput);
BOOL NmInputIsDownWait(NmInput *pInput);
BOOL NmInputIsLeftWait(NmInput *pInput);
BOOL NmInputIsRightWait(NmInput *pInput);
BOOL NmInputIsButton1Wait(NmInput *pInput);
BOOL NmInputIsButton2Wait(NmInput *pInput);
BOOL NmInputIsButton3Wait(NmInput *pInput);
BOOL NmInputIsButton4Wait(NmInput *pInput);

