// GameScene.cppファイル内の関数のうち、他のファイルから呼び出される関数のプロトタイプ宣言を記述する

BOOL initKurezit(void);
void moveKurezit(DWORD dwTickDiff);
void renderKurezit(void);
void releaseKurezit(void);
void KurezitCollideCallback(int nSrc, int nTarget, int nCollideID);