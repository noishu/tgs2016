/********************************************************/
/* DirectX  ゲームプログラミング						*/
/*						ゲームライブラリー				*/
/*														*/
/* NmBase.h - ウィンドウ処理							*/
/*														*/
/*					沼津情報専門学校　Atsushi Ohba		*/
/*						Copyright (c) 2005-2005			*/
/*						All Rights Reserved.			*/
/********************************************************/
#ifndef __NMBASE_H_
#define __NMBASE_H_

// 下で指定された定義の前に対象プラットフォームを指定しなければならない場合、以下の定義を変更してください。
// 異なるプラットフォームに対応する値に関する最新情報については、MSDN を参照してください。
#ifndef WINVER				// Windows XP 以降のバージョンに固有の機能の使用を許可します。
#define WINVER 0x0501		// これを Windows の他のバージョン向けに適切な値に変更してください。
#endif

#ifndef _WIN32_WINNT		// Windows XP 以降のバージョンに固有の機能の使用を許可します。                   
#define _WIN32_WINNT 0x0501	// これを Windows の他のバージョン向けに適切な値に変更してください。
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 以降のバージョンに固有の機能の使用を許可します。
#define _WIN32_WINDOWS 0x0410 // これを Windows Me またはそれ以降のバージョン向けに適切な値に変更してください。
#endif

#ifndef _WIN32_IE			// IE 6.0 以降のバージョンに固有の機能の使用を許可します。
#define _WIN32_IE 0x0600	// これを IE の他のバージョン向けに適切な値に変更してください。
#endif


//	ヘッダファイルのインクルード
#include <afxwin.h>				// Windowsの標準ヘッダーファイル
#include <mmsystem.h>
#include <stdio.h>				// 標準ＩＯヘッダファイル
#include <string.h>				// 文字列処理用ヘッダファイル
#include <math.h>				// 数学関数ヘッダファイル

#include "NmGraphic.h"
#include "Nm2DSur.h"
#include "NmInput.h"
#include "NmSound.h"

//	マクロ定義
#define	WINDOW_NAME		"魔界統一記"				// ウインドウ名
#define APP_NAME 		"魔界統一記"				// アプリケーション名
#define	SCREEN_WIDTH	1280						// スクリーンの横幅
#define	SCREEN_HEIGHT 	720						// スクリーンの縦幅
#define WINDOW_MODE		1						// ウインドウモード、スクリーンモード切替

// 汎用マクロ
#define	RAD(p)			( (p) * (float)(D3DX_PI / 180) )
#define FSIN(p)			( (float)sin(p) )
#define FCOS(p)			( (float)cos(p) )
#define	F(p)			( (float)(p) )

// オブジェクトリリース用マクロ
#define RELEASE(p) 		if(p){(p)->Release();(p)=NULL;}


// Zバッファー深度フォーマット
#define DEPTHFORMAT		D3DFMT_D16
//#define DEPTHFORMAT		D3DFMT_D24X8

//マウス座標のX座標取得
int 	GetMouseX(void);
//マウス座標のY座標取得
int 	GetMouseY(void);

//マウス右クリック判定(押しっぱなし判定)
BOOL	GetMouseButtonR();
//マウス左クリック判定(押しっぱなし判定）
BOOL	GetMouseButtonL();
//マウス左クリック判定(単一)
BOOL	GetMouseButtonRWait();
//マウス左クリック判定(単一)
BOOL	GetMouseButtonLWait();
void	updateMouseState();

#endif

