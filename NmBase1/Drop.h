#ifndef _DROP_H_
#define _DROP_H_

#include "NmBase1.h"
#include "Unit.h"

void DropInit();
void DropUpdata(int stageNo);
void DropRender(int nextF);
void DropDelete();

#endif