#include "NmBase1.h"
#include "GameMain.h"
#include "UnitScene.h"

//独自の変数
extern NmInput input;
//メニュー
#define MENU_MAX 7
extern	SCENE_NO menu4[7] = { SCENE_GAME1, SCENE_GAME2, SCENE_YUNIT, SCENE_ITIRAN, SCENE_HENSEI, SCENE_MENU };

//メニュー背景
static Nm2DSurface menu4BackSurf;
//メニュー項目
static Nm2DSurface menu4Surf;
static Nm2DSurface return4Surf;
//カーソル
static Nm2DSurface corasol4Surf;
//曲の設定
static NmMusic MainMusic;
//初期化
BOOL initUnitScene(void){
	Nm2DInit(&menu4BackSurf);
	Nm2DOpen(&menu4BackSurf, "res/UnitBack.png", 1280, 720);
	Nm2DInit(&return4Surf);
	Nm2DOpen(&return4Surf, "res/modoru2.png", 150, 120);
	Nm2DInit(&menu4Surf);
	Nm2DOpen(&menu4Surf, "res/unit on.png", 800, 100);
	Nm2DInit(&corasol4Surf);
	Nm2DOpen(&corasol4Surf, "res/crasol.png", 50, 50);
	NmMusicInit(&MainMusic);
	NmMusicOpen(&MainMusic,"res/MOUSIC/MEIN/game_maoudamashii_7_event34.mid",0);
	NmMusicPlay(&MainMusic);

		return TRUE;
}
//フレーム処理
void moveUnitScene(DWORD dwTickDiff){
	//シーン移動
	if (GetMouseButtonLWait()){
	//	if (GetMouseX() > 200 && GetMouseX() < 200 + 400 &&
	//		GetMouseY()>350 && GetMouseY() < 350 + 100){
	//		changeScene(menu4[SCENE_ITIRAN]);
	//	}
		if (GetMouseX() > 610 && GetMouseX()<610 + 400 &&
			GetMouseY()>350 && GetMouseY() < 350 + 100){
			changeScene(menu4[SCENE_HENSEI]);
		}
		if (GetMouseX() > 1050 && GetMouseX()<1050 + 150 &&
			GetMouseY()>10 && GetMouseY() < 10 + 120){
			changeScene(menu4[SCENE_GAME1]);
		}
	}
}
//レンダリング処理
void renderUnitScene(void){
	//メニュー背景
	Nm2DRender(&menu4BackSurf);
	//メニューの表示
	Nm2DSet(&menu4Surf, 200, 350);
	Nm2DRender(&menu4Surf);
	//戻るボタン
	Nm2DSet(&return4Surf, 1050, 10);
	Nm2DRender(&return4Surf);
	//カーソル
	Nm2DSet(&corasol4Surf, GetMouseX(), GetMouseY());
	Nm2DRender(&corasol4Surf);
}
//シーン終了時の処理
void releaseUnitScene(void){
	Nm2DDeleteSurface(&menu4BackSurf);
	Nm2DDeleteSurface(&menu4Surf);
	Nm2DDeleteSurface(&return4Surf);
	Nm2DDeleteSurface(&corasol4Surf);
	NmMusicDelete(&MainMusic);
}
//あたり判定コールバック
void UnitSceneCollideCallback(int nSrc, int nTarget, int nCollideID){
	if (GetMouseButtonLWait()){
		//	if (GetMouseX() > 450 && GetMouseX() < 450 + 140 &&
		//		GetMouseY()>350 && GetMouseY() < 350 + 60){
		//		changeScene(menu4[SCENE_ITIRAN]);
		//	}
			if (GetMouseX() > 600 && GetMouseX()<600 + 140 &&
				GetMouseY()>350 && GetMouseY() < 350 + 60){
				changeScene(menu4[SCENE_HENSEI]);
			}
		if (GetMouseX() > 1050 && GetMouseX()<1050 + 150 &&
			GetMouseY()>10 && GetMouseY() < 10 + 120){
			changeScene(menu4[SCENE_GAME1]);
		}
	}
}