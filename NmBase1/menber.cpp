#include "NmBase1.h"
#include "menber.h"
#include "Unit.h"
//初期宣言
Nm2DSurface MonsterSurf;
NmFont MapFont;
//変数
int MenberData[SCREEN_H][SCREEN_W];
int left = 0;
int top = 0;
int leftGap = -30;
int topGap = 60;
//マップの初期化
void MenberInit(){
	//配列の初期化
	for (int i = 0; i < SCREEN_H; i++){
		for (int j = 0; j < SCREEN_W; j++){
			MenberData[i+1][j] = 0;
		}
	}
	//配列の初期化
	ZeroMemory(MenberData, (int)SCREEN_W*SCREEN_W);

	//モンスター画像の読み込み
	Nm2DInit(&MonsterSurf);
	Nm2DOpen(&MonsterSurf, "res/MIcon.png", 160, 160, 8, 4, D3DCOLOR_RGBA(0, 255, 0, 0));
	NmFontInit(&MapFont);
	NmFontCreate(&MapFont, 15,"");
}
//データを放り込む
void  MenberTipDraw(int x, int y, int index){
	Nm2DSet(&MonsterSurf, x, y);
	Nm2DSetIndex(&MonsterSurf, index);
	Nm2DRender(&MonsterSurf);
}
//マップデータにデータを設定
//データを取り出す
void MenbreSet(int mx, int my, int index){
	int cx = (mx+leftGap) / TIP_SIZE;
	int cy = (my+topGap) / TIP_SIZE;
	//メンバーデータにデータを入れる
	MenberData[cy + top][cx + left] = index;
}
//メンバーデータの取得
int GetMenberData(int mx, int my){
	return MenberData[my][mx];
}
//メンバーの表示
void MenberRender(){
//マップの表示
	for (int i = 0; i < SCREEN_H; i++){
		for (int j = 0; j < SCREEN_W; j++){
			if (MenberData[i][j] == 0)continue;
				Nm2DSet(&MonsterSurf, j*TIP_SIZE - leftGap, i*TIP_SIZE - topGap);
				Nm2DSetIndex(&MonsterSurf, MenberData[i + top][j + left]);
				if (MenberData[i + top][j + left] == skeletonGeneral2){
					Nm2DSetIndex(&MonsterSurf,skeletonGeneral);
				}
				Nm2DRender(&MonsterSurf);
		}
	}
}
//エディットメンバーの表示
void EditMenberRender(){
	//マップの表示
	for (int i = 0; i < SCREEN_H; i++){
		for (int j = 0; j < SCREEN_W; j++){
			Nm2DSet(&MonsterSurf, j*TIP_SIZE - leftGap, i*TIP_SIZE - topGap);
			Nm2DSetIndex(&MonsterSurf, MenberData[i+top][j+left]);
			Nm2DRender(&MonsterSurf);
		}
	}
	//デバック用のマップ位置
	//横
	for (int i = 0; i < SCREEN_W; i++){
		char buf[256];
		sprintf(buf, "%03d", i+left);
		NmFontDrawText(&MapFont, buf, i*TIP_SIZE, 0, D3DCOLOR_RGBA(255, 255, 255, 255));
	}
	//縦
	for (int i = 0; i < SCREEN_H; i++){
		char buf[256];
		sprintf(buf, "%03d", i + top);
		NmFontDrawText(&MapFont, buf, 0, i*TIP_SIZE, D3DCOLOR_RGBA(255, 255, 255, 255));
	}
}
//マップデータのセーブとロード
void menberSave(char* _FileName){
	//ファイルを開く
	FILE *fp = fopen(_FileName, "w");
	//ファイルの書き込み
	fwrite(MenberData, sizeof(int), SCREEN_W*SCREEN_H, fp);
	//ファイルを閉じる
	fclose(fp);
}
void menberLoad(char* _FileName){
	//ファイルを開く
	FILE *fp = fopen(_FileName, "r");
	//ファイルの読み込み
	fread(MenberData, sizeof(int), SCREEN_W*SCREEN_H, fp);
	//ファイルを閉じる
	fclose(fp);
}