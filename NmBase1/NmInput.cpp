/********************************************************/
/* DirectX  ゲームプログラミング						*/
/*						ゲームライブラリー				*/
/*														*/
/* NmInput.h - 入力										*/
/*														*/
/*		オリジナル		　			  Oikawa			*/
/*		改編		沼津情報専門学校　Atsushi Ohba		*/
/*						Copyright (c) 2003-2004			*/
/*						All Rights Reserved.			*/
/********************************************************/

#define STRICT
#include "NmBase1.h"
#include "NmInput.h"


/////////////////////////////////////////////////////////////////////////////
// CNmInput
// モード０　単独プレイ
static SKeyData staticP0Key1 = {VK_UP,VK_DOWN,VK_RIGHT,VK_LEFT,VK_SPACE,VK_RETURN,-1,-1};
static SKeyData staticP0Key2 = {VK_NUMPAD8,VK_NUMPAD2,VK_NUMPAD6,VK_NUMPAD4,'Z','X','C','V'};
static SKeyData staticP0Key3 = {-1,-1,-1,-1,VK_SHIFT,VK_CONTROL,VK_ESCAPE,-1};

// モード１　二人プレイ　プレイヤー１
static SKeyData staticP1Key1 = {VK_UP,VK_DOWN,VK_RIGHT,VK_LEFT,VK_SPACE,VK_RETURN,-1,-1};
static SKeyData staticP1Key2 = {VK_NUMPAD8,VK_NUMPAD2,VK_NUMPAD6,VK_NUMPAD4,VK_NUMPAD0,VK_NUMPAD5,-1,-1};
static SKeyData staticP1Key3 = {-1,-1,-1,-1,-1,-1,-1,-1};

// モード２　二人プレイ　プレイヤー２
static SKeyData staticP2Key1 = {'T','B','H','F','Z','X','C','V'};
static SKeyData staticP2Key2 = {-1,-1,-1,-1,-1,-1,-1,-1};
static SKeyData staticP2Key3 = {-1,-1,-1,-1,-1,-1,-1,-1};

// モード３　
static SKeyData staticWindowKey1 = {VK_UP,VK_DOWN,VK_RIGHT,VK_LEFT,VK_SPACE,VK_ESCAPE,-1,-1};
static SKeyData staticWindowKey2 = {VK_NUMPAD8,VK_NUMPAD2,VK_NUMPAD6,VK_NUMPAD4,VK_RETURN,-1,-1,-1};
static SKeyData staticWindowKey3 = {-1,-1,-1,-1,'Z','X',-1,-1};

static SKeyData staticKeyNull = {-1,-1,-1,-1,-1,-1,-1,-1};

// インプットの準備
void NmInputCreate( NmInput *pInput, int nJoyPort, int nKey)
{
	pInput->m_KeyData[0] = staticKeyNull;
	pInput->m_KeyData[1] = staticKeyNull;
	pInput->m_KeyData[2] = staticKeyNull;
	pInput->m_nJoyPort = 0;
	pInput->m_bU = FALSE;
	pInput->m_bR = FALSE;
	pInput->m_bD = FALSE;
	pInput->m_bL = FALSE;
	pInput->m_nButtons = 0;

	pInput->m_KeyData[0] = staticKeyNull;
	pInput->m_KeyData[1] = staticKeyNull;
	pInput->m_KeyData[2] = staticKeyNull;
	switch(nKey)
	{
	case 0:
		pInput->m_KeyData[0] = staticP0Key1;
		pInput->m_KeyData[1] = staticP0Key2;
		pInput->m_KeyData[2] = staticP0Key3;
		break;
	case 1:
		pInput->m_KeyData[0] = staticP1Key1;
		pInput->m_KeyData[1] = staticP1Key2;
		pInput->m_KeyData[2] = staticP1Key3;
		break;
	case 2:
		pInput->m_KeyData[0] = staticP2Key1;
		pInput->m_KeyData[1] = staticP2Key2;
		pInput->m_KeyData[2] = staticP2Key3;
		break;
	case 3:
		pInput->m_KeyData[0] = staticWindowKey1;
		pInput->m_KeyData[1] = staticWindowKey2;
		pInput->m_KeyData[2] = staticWindowKey3;
		break;
	}
	if(nJoyPort==0)
		pInput->m_nJoyPort = JOYSTICKID1;
	else
		pInput->m_nJoyPort = JOYSTICKID2;

	joyGetDevCaps(pInput->m_nJoyPort,&pInput->m_joyCaps,sizeof(pInput->m_joyCaps));
}

// 入力状態を得る
void NmInputGet( NmInput *pInput )
{
	pInput->m_nButtons = 0;
	JOYINFO joyInfo;
	if(joyGetPos(pInput->m_nJoyPort,&joyInfo) == JOYERR_NOERROR)
	{
		int lx = 0;
		int ly = 0; 
		int x = 0;
		int y = 0;
		lx = pInput->m_joyCaps.wXmax / 3; 
		ly = pInput->m_joyCaps.wYmax / 3; 
		x = joyInfo.wXpos - pInput->m_joyCaps.wXmax / 2;
		y = joyInfo.wYpos - pInput->m_joyCaps.wYmax / 2;
		pInput->m_nButtons = joyInfo.wButtons;
		pInput->m_bU = (-y > ly);
		pInput->m_bR = (x > lx);
		pInput->m_bD = (y > ly);
		pInput->m_bL = (-x > lx);
	}

	int i;
	int nPush = 0;
	for(i=0;i<8;i++)
	{
		if(NmInputGetKeyStat(pInput, i))
			nPush |= (1<<i);
		else
			nPush &= ~(1<<i);
	}
	pInput->m_nPush2[pInput->m_nJoyPort] = pInput->m_nPush[pInput->m_nJoyPort];
	pInput->m_nPush[pInput->m_nJoyPort] = nPush;
}


BOOL NmInputGetKeyStat2(NmInput *pInput,int nIndex)
{
	if(pInput->m_nPush2[pInput->m_nJoyPort] & (1<<nIndex))
		return FALSE;
	return NmInputGetKeyStat(pInput, nIndex);
}
BOOL NmInputGetKeyStat(NmInput *pInput,int nIndex)
{
	int i;
	BOOL bFlag = FALSE;
	for(i=0;i<3;i++)
	{
		int Key;
		switch(nIndex)
		{
		case 0:	Key = pInput->m_KeyData[i].Up;break;
		case 1:	Key = pInput->m_KeyData[i].Right;break;
		case 2:	Key = pInput->m_KeyData[i].Down;break;
		case 3:	Key = pInput->m_KeyData[i].Left;break;
		case 4:	Key = pInput->m_KeyData[i].Botton1;break;
		case 5:	Key = pInput->m_KeyData[i].Botton2;break;
		case 6:	Key = pInput->m_KeyData[i].Botton3;break;
		case 7:	Key = pInput->m_KeyData[i].Botton4;break;
		}
		if(Key!=-1)
		{
			bFlag = GetAsyncKeyState(Key)<0;
			if(bFlag)
				break;
		}
		switch(nIndex)
		{
		case 0:	bFlag |= pInput->m_bU;break;
		case 1:	bFlag |= pInput->m_bR;break;
		case 2:	bFlag |= pInput->m_bD;break;
		case 3:	bFlag |= pInput->m_bL;break;
		case 4:	bFlag |= (pInput->m_nButtons&1);break;
		case 5:	bFlag |= (pInput->m_nButtons&2);break;
		case 6:	bFlag |= (pInput->m_nButtons&4);break;
		case 7:	bFlag |= (pInput->m_nButtons&8);break;
		}
	}

	return bFlag;
}



void NmInputSetKeyData(NmInput *pInput, int nIndex,SKeyData* pKeyData){pInput->m_KeyData[nIndex]=*pKeyData;}

// 各キーの情報をを得る
BOOL NmInputIsUp(NmInput *pInput){return NmInputGetKeyStat(pInput,0);}
BOOL NmInputIsDown(NmInput *pInput){return NmInputGetKeyStat(pInput,2);}
BOOL NmInputIsLeft(NmInput *pInput){return NmInputGetKeyStat(pInput,3);}
BOOL NmInputIsRight(NmInput *pInput){return NmInputGetKeyStat(pInput,1);}
BOOL NmInputIsButton1(NmInput *pInput){return NmInputGetKeyStat(pInput,4);}
BOOL NmInputIsButton2(NmInput *pInput){return NmInputGetKeyStat(pInput,5);}
BOOL NmInputIsButton3(NmInput *pInput){return NmInputGetKeyStat(pInput,6);}
BOOL NmInputIsButton4(NmInput *pInput){return NmInputGetKeyStat(pInput,7);}

BOOL NmInputIsUpWait(NmInput *pInput){return NmInputGetKeyStat2(pInput,0);}
BOOL NmInputIsDownWait(NmInput *pInput){return NmInputGetKeyStat2(pInput,2);}
BOOL NmInputIsLeftWait(NmInput *pInput){return NmInputGetKeyStat2(pInput,3);}
BOOL NmInputIsRightWait(NmInput *pInput){return NmInputGetKeyStat2(pInput,1);}
BOOL NmInputIsButton1Wait(NmInput *pInput){return NmInputGetKeyStat2(pInput,4);}
BOOL NmInputIsButton2Wait(NmInput *pInput){return NmInputGetKeyStat2(pInput,5);}
BOOL NmInputIsButton3Wait(NmInput *pInput){return NmInputGetKeyStat2(pInput,6);}
BOOL NmInputIsButton4Wait(NmInput *pInput){return NmInputGetKeyStat2(pInput,7);}
