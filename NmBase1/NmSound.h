/********************************************************/
/* DirectX  ゲームプログラミング						*/
/*						ゲームライブラリー				*/
/*														*/
/* NmSound.h - サウンド									*/
/*														*/
/*		オリジナル		　			  Oikawa			*/
/*		改編		沼津情報専門学校　Atsushi Ohba		*/
/*						Copyright (c) 2003-2004			*/
/*						All Rights Reserved.			*/
/********************************************************/
#include <dsound.h>

/////////////////////////////////////////////////////////////////////////////
// CStdFile
class CStdFile
{
public:
	enum OpenFlags{modeRead=0x0000,modeWrite=0x0001,modeReadWrite=0x0002};
	CStdFile();
	~CStdFile();
	BOOL Open(LPCTSTR pFileName,UINT nMode);
	UINT Read(LPVOID pBuff,UINT nCount)const;
	UINT Write(LPVOID pBuff,UINT nCount)const;
	LPSTR Gets(LPSTR pStr,int nLength){return fgets(pStr,nLength,m_pFile);}
	long GetSeek(){return ftell(m_pFile);}
	DWORD GetLength()const;
	BOOL SetSeek(long lSeek,int nOrigin){return fseek(m_pFile,lSeek,nOrigin);}
	LPCSTR GetFileName(){return m_pFileName;}
protected:
	FILE* m_pFile;
	LPCTSTR m_pFileName;
};

/////////////////////////////////////////////////////////////////////////////
// CResourceFile
class CResourceFile
{
public:
	CResourceFile();
	~CResourceFile();
	BOOL Open(LPCSTR pName,LPCSTR pType);
	void Close();
	BOOL Read(LPVOID pVoid,DWORD dwSeek);
	DWORD GetLength()const{return SizeofResource(NULL,m_hRsrc);}
	LPCVOID GetData()const{return m_pVoid;}
	long GetSeek(){return (long)m_dwSeek;}
	BOOL SetSeek(long lSeek,int nOrigin)
	{
		if(nOrigin == SEEK_CUR)
			m_dwSeek += lSeek;
		else if(nOrigin == SEEK_END)
			m_dwSeek = GetLength() + lSeek;
		else if(nOrigin == SEEK_SET)
			m_dwSeek = lSeek;
		return TRUE;
	}
	LPSTR Gets(LPSTR pStr,int nLength)
	{
		int i;
		LPSTR pData = (LPSTR)m_pVoid;
		for(i=0;m_dwSeek+i<GetLength() && i<nLength &&
			pData[m_dwSeek+i]!='\n' && pData[m_dwSeek+i]!='\0';i++)
			pStr[i] = pData[m_dwSeek+i];
			pStr[i] = 0;
		m_dwSeek += i;
		return pStr;
	}
	LPCSTR GetFileName(){return m_pFileName;}
protected:
	LPCSTR m_pFileName;
	HRSRC m_hRsrc;
	DWORD m_dwSeek;
	HGLOBAL m_hGlobal;
	LPVOID m_pVoid;
};

/////////////////////////////////////////////////////////////////////////////
// CPressFile
class CPressFile
{
public:	
	struct SDxmpHeader
		{char cCheak[5];int nVersion;int nSize;};
	struct SDxmpTHeader
		{int nType;int nNameSize;int nFlagSize;int nDataSize;int nFileSize;};
	CPressFile();
	~CPressFile();
	BOOL Pack(LPCTSTR pFileName,LPCTSTR pPackName=NULL);
	BOOL Open(LPCSTR pFileName);
	BOOL Open(LPBYTE pHeader);
	BOOL Unpack(LPCSTR pFileName);
	BOOL Unpack();
	BOOL Read(LPVOID pVoid,DWORD dwSeek);
	long GetSeek(){return m_nSeek;}
	const LPBYTE GetData()const{return m_pData;} 
	int GetSize()const{return m_Dth.nFileSize;} 
	int GetLength()const{return m_Dth.nFileSize;} 
	LPCTSTR GetFileName()const{return m_pFileName;} 
	LPTSTR GetPackName(LPCSTR pFileName);
	LPSTR Gets(LPSTR pStr,int nLength);
	BOOL SetSeek(long lSeek,int nOrigin);
private:
	SDxmpTHeader m_Dth;
	LPBYTE m_pMemData;
	int m_nSrcPt;
	int m_nSeek;
	LPTSTR m_pPackName;
	LPTSTR m_pFileName;
	LPBYTE m_pData;
};

/////////////////////////////////////////////////////////////////////////////
// CDxmFile
class CDxmFile
{
public:
	CDxmFile();
	~CDxmFile();
	int GetLength()const;
	void SetType(LPCSTR pType);
	BOOL Open(LPCTSTR pFileName);
	BOOL Read(LPVOID pVoid,DWORD dwSeek);
	int GetChar();
	BOOL SetSeek(long lSeek,int nOrigin);
	long GetSeek();
	LPSTR Gets(LPSTR pStr,int nLength);
	BOOL IsEof(){return GetSeek()>=GetLength();}
	LPCSTR GetFileName();
protected:
	int m_nType;
	LPSTR m_pType;
	CStdFile* m_pSFile;
	CResourceFile* m_pRFile;
	CPressFile* m_pPFile;
};


/////////////////////////////////////////////////////////////////////////////
// CNmSound
struct _SWaveHeder
	{DWORD dwCheck;DWORD dwSize;DWORD dwWaveCheck;DWORD dwFmtCheck;DWORD dwFmtSize;WAVEFORMATEX formatWave;};
typedef HRESULT (WINAPI DSoundCreate)(GUID FAR * lpGUID,LPDIRECTSOUND * ppDS,IUnknown FAR *pUnkOuter);
typedef struct _NmSound
{
	DSoundCreate* m_pDirectSoundCreate;
	HINSTANCE m_hDirectSound;
	
	DWORD m_dwDataSize;
	LPBYTE m_pbyData;

	LPDIRECTSOUND m_pDirectSound;

	int m_nCount;
	LPDIRECTSOUNDBUFFER* m_ppDSBuffer;
	HWND m_hWnd;
	struct _SWaveHeder* m_pWave;
}NmSound;

//サウンド再生の初期化(サウンド変数)
void NmSoundInit( NmSound *pSound );
//サウンドデータの削除(サウンド変数)：データの削除のみ
void NmSoundClose( NmSound *pSound );
//サウンドの削除(サウンド変数)：デバイス設定も含む為、ゲームの終了時はこちらで
void NmSoundDelete( NmSound *pSound );
//なにこれ
BOOL NmSoundSetCoopWnd(NmSound *pSound, HWND hWnd=0);
//サウンドデータの読込(サウンド変数、ファイルパス、リソースタイプ名(サウンドの種類設定など)指定しなければ0に指定する)
BOOL NmSoundOpen(NmSound *pSound,LPCSTR pName,LPCSTR pType=NULL);
//サウンドデータの読込(サウンド変数、ファイルパス、wave指定済み版)
BOOL NmSoundOpenResource(NmSound *pSound, LPCSTR pName,LPCSTR pType="wave");
//サウンドデータの読込(サウンド変数、ファイルパス)：リソースタイプを設定しない版
BOOL NmSoundOpenFile(NmSound *pSound, LPCSTR pFileName);
//サウンドデータのリスト設定?
void NmSoundCreateList(NmSound *pSound, int nCount);
//サウンドデータの再生（サウンド変数、パン(音の振り分け)の指定
BOOL NmSoundPlay(NmSound *pSound, int nPan=0);
//内部呼び出し(サウンドデータの削除）
void NmSoundRelease( NmSound *pSound );
LPDIRECTSOUNDBUFFER NmSoundCreatBuffer( NmSound *pSound );


//------------------------------------------------------------
// CDxMTimer
// マルチメディアタイマー用
//------------------------------------------------------------
class CDxMTimer
{
public:
	CDxMTimer();
	~CDxMTimer();
	BOOL IsTimer()const{return m_hTimer;}
	WORD GetMinTime(){return (WORD)m_timeCaps.wPeriodMin;}
	BOOL Start(UINT uDelay,LPTIMECALLBACK lpTimeProc = NULL,DWORD dwUser = NULL);
	BOOL Stop();
	virtual void procEnter(){}
	static void CALLBACK procTime(UINT uID,UINT uMsg,DWORD dwUser,DWORD dw1,DWORD dw2);

protected:
	UINT m_hTimer;
	TIMECAPS m_timeCaps;

};


//------------------------------------------------------------
// CDxMidiDevice
// MIDIデバイス制御用
//------------------------------------------------------------
class CDxMidiDevice
{
public:
	CDxMidiDevice();
	~CDxMidiDevice();
	BOOL Open(UINT uDeviceID = MIDI_MAPPER);
	BOOL Close();
	BOOL Out(LPMIDIHDR pMidiHdr)const;
	BOOL Out(DWORD dwData)const;
	BOOL Out(BYTE byData1,BYTE byData2,BYTE byData3)const;

	BOOL IsDevice()const{return (BOOL)m_hMidi;}
	int GetDeviceCount()const{return midiOutGetNumDevs();}
	void GetDeviceName(int nIndex,LPSTR pString);

protected:
	HMIDIOUT m_hMidi;
	UINT m_uDevice;

};

/////////////////////////////////////////////////////////////////////////////
//     CMdExclusive
//---------------------------------------------------------
class CMdExclusive
{
public:
	CMdExclusive(){ZeroMemory(&m_midiHDR,sizeof(MIDIHDR));}
	~CMdExclusive();
	void Set(int nCount,LPBYTE pData);
	LPMIDIHDR Get(){return &m_midiHDR;}
protected:
	MIDIHDR m_midiHDR;
};

//---------------------------------------------------------
//     ダブルワード変換
//---------------------------------------------------------
inline DWORD protConvDW(DWORD dwData)
{
	DWORD dwWork;
	((char*)&dwWork)[0] = ((char*)&dwData)[3];
	((char*)&dwWork)[1] = ((char*)&dwData)[2];
	((char*)&dwWork)[2] = ((char*)&dwData)[1];
	((char*)&dwWork)[3] = ((char*)&dwData)[0];
	return dwWork;
}
inline DWORD protGetWaitCount(LPBYTE* ppData)
{
	BYTE bbData;
	DWORD dwData = 0;
	do
	{
		bbData = **ppData;
		++*ppData;
		dwData = (dwData<<7) + (bbData&0x7f);
	}while(bbData&0x80);
	return dwData;
}

//---------------------------------------------------------
//     ワード変換
//---------------------------------------------------------
inline WORD protConvW(WORD wData)
{
	WORD wWork;
	((char*)&wWork)[0] = ((char*)&wData)[1];
	((char*)&wWork)[1] = ((char*)&wData)[0];
	return wWork;
}


//---------------------------------------------------------
//     SPlayInfo
//---------------------------------------------------------
struct SPlayInfo
{
	DWORD dwAllStep;
	DWORD dwNowStep;
	WORD wRCount;
	WORD wTempo;
	LPCSTR pTitle;
	struct Channel
	{
		BYTE bbNote[16];
		BYTE bbPC;
		BYTE bbCC;
		BYTE bbPan;
		BYTE bbRev;
		BYTE bbCos;
		BYTE bbVolume;
		BYTE bbVolume2;
	}Chn[16];//チャンネルは１６（ｂｙ浅田）
};

//---------------------------------------------------------
//     SMidiCode
union UMidiData
{
	DWORD dwMsg;
	BYTE bbData[4];
};
//---------------------------------------------------------
struct SMidiCode
{
	DWORD dwWait;
	UMidiData Mdf;
};

/////////////////////////////////////////////////////////////////////////////
//     CPlayData
//---------------------------------------------------------
class CPlayData
{
	struct STrackHeader{DWORD dwCheck;DWORD dwBytes;};
	struct SMidiHeader{DWORD dwCheck,dwBytes;WORD wFormat,wTracks;WORD wCount;};
public:
	/////////////////////////////////////////////////////////////////////////////
	//     CTrackData
	//---------------------------------------------------------
	class CTrackData
	{
	public:
		CTrackData(){m_dwWCount = 0;m_nStep = 0;}
		BYTE GetStat();
		BYTE GetData(){BYTE bbByte = *m_pByte;m_pByte++;return bbByte;}
		DWORD SetHeader(STrackHeader* pTHeader);
		DWORD GetNextCount(){return m_dwNextCount;}
		void Next(){m_dwNextCount += GetWait();}
		void SetStep(int nStep){m_dwNextCount-=nStep;}
		BOOL IsPlay()const{return m_pByte<m_pLastByte-1;}
		DWORD GetWait(){return protGetWaitCount(&m_pByte);}
		void SetSkip(){m_pByte += GetWait();}
		void SetSkip(DWORD dwSkip){m_pByte += dwSkip;}
		void Init()
			{m_dwNextCount = 0;m_pByte = (LPBYTE)m_pTHeader+8;
			m_pLastByte=m_pByte+protConvDW(m_pTHeader->dwBytes);}
		DWORD GetStep()const{return m_nStep;}
		DWORD GetEStep()const{return m_nEStep;}
		LPCSTR GetString(){return (LPCSTR)m_pByte;}
		LPBYTE GetAdr(){return m_pByte;}
	protected:	
		STrackHeader* m_pTHeader;
		DWORD m_dwWCount;
		BYTE m_bbStatBack;
		DWORD m_dwNextCount;
		LPBYTE m_pByte,m_pLastByte; 
		int m_nStep,m_nEStep;
	};
public:
	CPlayData();
	~CPlayData(){ReleaseData();}
	void ReleaseData();
	void Init(){m_dwlCount=0;m_dwPoint = 0;}
	BOOL Read(const LPBYTE pData);
	LPCSTR GetTitle()const{return m_pTitle;}
	WORD GetRCount()const{return m_wRCount;}
	DWORD GetNextCount()const{return m_pMCode[m_dwPoint].dwWait;}
	BYTE GetCode()const{return m_pMCode[m_dwPoint].Mdf.bbData[0];}
	DWORD GetDWord()const{return m_pMCode[m_dwPoint].Mdf.dwMsg;}
	BYTE GetByte0()const{return m_pMCode[m_dwPoint].Mdf.bbData[0];}
	BYTE GetByte1()const{return m_pMCode[m_dwPoint].Mdf.bbData[1];}
	BYTE GetByte2()const{return m_pMCode[m_dwPoint].Mdf.bbData[2];}
	BYTE GetByte3()const{return m_pMCode[m_dwPoint].Mdf.bbData[3];}
	LPMIDIHDR GetEData(){return m_pECode[GetDataWord()].Get();}
	WORD GetDataWord()const{return *(LPWORD)(&m_pMCode[m_dwPoint].Mdf.bbData[1]);}
	BOOL IsPlay()const{return (m_dwPoint < m_dwStep);}
	void Next(){m_dwlCount+=GetNextCount();m_dwPoint++;}
	DWORDLONG GetNowCount()const{return m_dwlCount;}
	DWORDLONG GetAllCount()const{return m_dwlAllCount;}
	DWORD GetAllTime()const{return m_dwAllTime;}
	BOOL IsData()const{return (BOOL)m_pMCode;}

protected:
	void Track(STrackHeader** ppTHeader,int nTCount);
	void SetTracks(CTrackData* pTData,int nTracks);
	WORD m_wRCount;

	LPSTR m_pTitle;
	DWORD m_dwStep;
	SMidiCode* m_pMCode;
	CMdExclusive* m_pECode;
	DWORD m_dwPoint;
	DWORDLONG m_dwlCount;
	DWORDLONG m_dwlAllCount;
	DWORDLONG m_dwlWorkCount;
	DWORD m_dwAllTime;
	DWORD m_dwTempoCount;
};

/////////////////////////////////////////////////////////////////////////////
// CElapse

class CElapse
{
public:
	CElapse(){ Init();}
	void Init(){m_nSkipTime=0;m_nDefaultCount=30;m_bValid = TRUE;m_dwCount = timeGetTime();}
	BOOL IsValid() const {return m_bValid;}
	void SetValid(BOOL bValid){m_nSkipTime=0;m_bValid = bValid;}
	void AddCount(int nCount);
	DWORD GetCount();
	DWORD GetCount2()const;
	void Sleep(DWORD dwWaitCount);
	void Sleep(){Sleep(m_nDefaultCount);}
	void SetSleepCount(DWORD dwWaitCount){m_nDefaultCount=dwWaitCount;}
	BOOL IsSkip()const{return m_nSkipTime;}
protected:
	int m_nSkipTime;
	DWORD m_dwCount;
	BOOL m_bValid;
	DWORD m_nDefaultCount;
};


/////////////////////////////////////////////////////////////////////////////
// CNmMusic
typedef struct _NmMusic
{
	volatile BOOL m_bPlayOut;
	volatile BOOL m_bStopFlag;
	int m_nTimeCycle;
	int m_nWaitCycle;
	DWORD m_timePlay;
	DWORD m_dwTempCount;
	DWORD m_dwTempChengeCount;

	WORD m_wTemp;
	DWORD m_dwTempParam;
	DWORD m_dwNowCount;
	DWORD m_dwNowTime;
	int m_nFadeVolume;
	float m_fRelativeVolume;
	int m_nRelativeVolumeWork;
	int m_nRelativeVolume;
	int m_nRelativeTemp;
	int m_nFadeCount;
	int m_nFadeCountWork;
	char m_cKey;
	BOOL m_bFadeIn,m_bFadeOut;
	BOOL m_bThrough;
	BOOL m_bLoop;
	DWORDLONG m_dwlLoopSearch;
	BOOL m_bThread;
	UINT m_uDevice;
	CElapse m_Elapse;
	SPlayInfo m_playInfo;
	CPlayData m_MidiData;
	CDxMTimer m_MTimer;
	CDxMidiDevice m_MDevice;
}NmMusic;

void NmMusicInit( NmMusic *pMusic );
BOOL NmMusicOpen(NmMusic *pMusic, LPCSTR pName, LPCSTR pType=NULL);
BOOL NmMusicClose(NmMusic *pMusic);
BOOL NmMusicDelete(NmMusic *pMusic);
BOOL NmMusicPlay(NmMusic *pMusic);
BOOL NmMusicCont( NmMusic *pMusic );
BOOL NmMusicStop( NmMusic *pMusic );
BOOL NmMusicSearch(NmMusic *pMusic, DWORDLONG dwlCount);
BOOL NmMusicFadeIn(NmMusic *pMusic,int nTime,int nVolume=-70);
BOOL NmMusicFadeOut(NmMusic *pMusic, int nTime);
BOOL NmMusicSetDevice(NmMusic *pMusic, int nIndex=MIDI_MAPPER);
void NmMusicSetTemp(NmMusic *pMusic, int nRelativeTempo);
BOOL NmMusicInsideOpenDevice(NmMusic *pMusic);
void NmMusicInsideComeback(NmMusic *pMusic);
void NmMusicInsideSetVolume(NmMusic *pMusic, int nSVolume=0);
void NmMusicInsideSetNoteOff(NmMusic *pMusic);
BOOL NmMusicInsideSetTimer(NmMusic *pMusic);
DWORD WINAPI NmMusicthreadCallBack( LPVOID pParam);
void CALLBACK NmMusictimerCallBackMCB(UINT uID,UINT uMsg,DWORD dwUser, DWORD dw1, DWORD dw2);
void NmMusicInsidePlay(NmMusic *pMusic);
void NmMusicInsideMidiOut(NmMusic *pMusic);

void NmMusicSetThreadMode(NmMusic *pMusic, BOOL bFlag);
BOOL NmMusicIsData(NmMusic *pMusic);
void NmMusicSetLoop(NmMusic *pMusic, BOOL bLoop);
void NmMusicSetLoopTop(NmMusic *pMusic, DWORDLONG dwlLoopSearch);
void NmMusicSetKey(NmMusic *pMusic, char cKey);
BOOL NmMusicIsPlay(NmMusic *pMusic);
WORD NmMusicGetTemp(NmMusic *pMusic);
WORD NmMusicGetRCount(NmMusic *pMusic);
DWORDLONG NmMusicGetAllCount(NmMusic *pMusic);
DWORDLONG NmMusicGetPlayCount(NmMusic *pMusic);
DWORDLONG NmMusicGetAllTime(NmMusic *pMusic);
DWORDLONG NmMusicGetPlayTime(NmMusic *pMusic);
LPCSTR NmMusicGetTitle(NmMusic *pMusic);
SPlayInfo* NmMusicGetPlayInfo(NmMusic *pMusic);

BOOL NmMusicOutMidiDevice(NmMusic *pMusic, DWORD dwData);
int NmMusicGetDeviceCount(NmMusic *pMusic);
void NmMusicGetDeviceName(NmMusic *pMusic, int nIndex,LPSTR pString);

UINT NmMusicGetTimerID(NmMusic *pMusic);
void NmMusicIncPlayCount(NmMusic *pMusic);
BOOL NmMusicIsPlayOut(NmMusic *pMusic);
BOOL NmMusicStdMidi(NmMusic *pMusic, LPBYTE pData);
