#include "NmBase1.h"
#include "GameMain.h"
#include "Game1Scene.h"

//独自の変数
extern NmInput input;
//メニュー
#define MENU_MAX 8
extern	SCENE_NO menu2[8] = { SCENE_MENU ,SCENE_GAME2, SCENE_GAME3,SCENE_HENSEI, SCENE_MENU,SCENE_GAME1,SCENE_GOUSEI,SCENE_SONTA};

//メニュー背景
static Nm2DSurface menu2BackSurf;
//メニュー項目
static Nm2DSurface menu2Surf;
static Nm2DSurface returnSurf;
static Nm2DSurface BWSurf;
static Nm2DSurface HWSurf;
static Nm2DSurface GWSurf;
static Nm2DSurface KWSurf;
//カーソル
Nm2DSurface corasolSurf;
	//マウス入力の保存
	int moveIndex = NULL;

//曲の設定
static NmMusic MainMusic;
// シーン開始前の初期化を行う
BOOL initGame1Scene(void){
	Nm2DInit(&menu2BackSurf);
	Nm2DOpen(&menu2BackSurf, "res/haikei.png", 1280, 720);
	//メニュー項目
	Nm2DInit(&menu2Surf);
	Nm2DOpen(&menu2Surf, "res/menu2.png", 
		1200, 200, 1, 1, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&returnSurf);
	Nm2DOpen(&returnSurf, "res/modoru1.png", 150, 120, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&BWSurf);
	Nm2DOpen(&BWSurf, "res/BSW.png", 1000, 400);
	Nm2DInit(&HWSurf);
	Nm2DOpen(&HWSurf, "res/HSW.png", 1000, 400);
	Nm2DInit(&GWSurf);
	Nm2DOpen(&GWSurf, "res/GSW.png", 1000, 400);
	Nm2DInit(&KWSurf);
	Nm2DOpen(&KWSurf, "res/CSW.png", 1000, 400);
	//カーソル
	Nm2DInit(&corasolSurf);
	Nm2DOpen(&corasolSurf, "res/crasol.png", 50, 50, D3DCOLOR_RGBA(0, 255, 0, 0));
	//曲関連の初期化
	NmMusicInit(&MainMusic);
	NmMusicOpen(&MainMusic, "res/MOUSIC/MEIN/game_maoudamashii_7_event34.mid", 0);
	NmMusicPlay(&MainMusic);
	return TRUE;
}
//	フレーム処理
void moveGame1Scene(DWORD dwTickDiff){
	//シーン移動
	if (GetMouseButtonLWait()){
	if (GetMouseX() > 0 && GetMouseX()<0 + 300 &&
		GetMouseY()>520 && GetMouseY() < 520 + 200){
		//NmMusicStop(&MainMusic);
		changeScene(menu2[SCENE_GAME2]);
	}		
	if (GetMouseX() > 320 && GetMouseX()<320 + 300 &&
			GetMouseY()>520 && GetMouseY() < 520 + 200){
			changeScene(menu2[SCENE_YUNIT]);
		}
	if (GetMouseX()>620 && GetMouseX()<620 + 300 &&
		GetMouseY()>520 && GetMouseY() < 520 + 200){
		changeScene(menu2[SCENE_GOUSEI]);
	}
	if (GetMouseX()>920 && GetMouseX()<920 + 300 &&
			GetMouseY()>520 && GetMouseY() < 520 + 200){
			changeScene(menu2[SCENE_SONTA]);
		}
		if (GetMouseX()>1050 && GetMouseX()<1050 + 150 &&
			GetMouseY()>10 && GetMouseY() < 10 + 120){
			changeScene(menu2[SCENE_MENU]);
		}
	
	}

}
//	レンダリング処理
void renderGame1Scene(void){
	//メニュー背景の表示
	Nm2DRender(&menu2BackSurf);
	//メニューの表示

	Nm2DSet(&menu2Surf, 20, 520);
	Nm2DRender(&menu2Surf);
	if (GetMouseX() > 0 && GetMouseX()<0 + 300 &&
		GetMouseY()>520 && GetMouseY() < 520 + 200){
		Nm2DSet(&BWSurf, 140, 130);
		Nm2DRender(&BWSurf);
	}
	if (GetMouseX() > 320 && GetMouseX()<320 + 300 &&
		GetMouseY()>520 && GetMouseY() < 520 + 200){
		Nm2DSet(&HWSurf,140,130);
		Nm2DRender(&HWSurf);
	}
	if (GetMouseX()>620 && GetMouseX()<620 + 300 &&
		GetMouseY()>520 && GetMouseY() < 520 + 200){
		Nm2DSet(&GWSurf, 140, 130);
		Nm2DRender(&GWSurf);
	}
	if (GetMouseX()>920 && GetMouseX()<920 + 300 &&
		GetMouseY()>520 && GetMouseY() < 520 + 200){
		Nm2DSet(&KWSurf,140,130);
		Nm2DRender(&KWSurf);
	}
	//戻るボタンの表示
	Nm2DSet(&returnSurf, 1050, 10);
	Nm2DRender(&returnSurf);
	Nm2DSet(&corasolSurf, GetMouseX(), GetMouseY());
	Nm2DRender(&corasolSurf);
}

//	シーン終了時の後処理
void releaseGame1Scene(void){
	Nm2DDeleteSurface(&menu2BackSurf);
	Nm2DDeleteSurface(&menu2Surf);
	Nm2DDeleteSurface(&returnSurf);
	Nm2DDeleteSurface(&corasolSurf);
	NmMusicDelete(&MainMusic);
}

// 当り判定コールバック 　　　ここでは要素を削除しないこと！！
void  Game1SceneCollideCallback(int nSrc, int nTarget, int nCollideID){
	if (GetMouseButtonLWait()){
		if (GetMouseX() > 20 && GetMouseX()< 20 + 300 &&
			GetMouseY()>520 && GetMouseY() < 520 + 200){
			changeScene(menu2[SCENE_GAME2]);
		}
		if (GetMouseX() > 320 && GetMouseX()<320 + 300 &&
			GetMouseY()>5200 && GetMouseY() < 520 + 200){
			changeScene(menu2[SCENE_YUNIT]);
		}
		//if (GetMouseX()>620 && GetMouseX()<620 + 300 &&
		//	GetMouseY()>520 && GetMouseY() < 520 + 200){
		//	changeScene(menu2[SCENE_GOUSEI]);
		//}
		//if (GetMouseX()>920 && GetMouseX()<920 + 300 &&
		//	GetMouseY()>520 && GetMouseY() < 520 + 200){
		//	changeScene(menu2[SCENE_SONTA]);
		//}
		if (GetMouseX()>1050 && GetMouseX()<1050 + 150 &&
			GetMouseY()>10 && GetMouseY() < 10 + 120){
			changeScene(menu2[SCENE_MENU]);
		}

	}
}