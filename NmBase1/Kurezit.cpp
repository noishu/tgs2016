#include "NmBase1.h"
#include "GameMain.h"
#include "Kurezit.h"

//�摜�̔z��
static Nm2DSurface KbackSurf;//�w�i
static Nm2DSurface KUSurf;	//�g�p�f�ރ^�u
static Nm2DSurface KMSurf;	//�����X�^�[
static Nm2DSurface KRSurf;	//�߂�{�^��
static Nm2DSurface KBsurf;	//�w�i�Љ�
static Nm2DSurface KMKSurf;	//���y
static Nm2DSurface KWSurf;	//�e�L�X�g
static Nm2DSurface KKSurf;	//���ӕ�
static Nm2DSurface KCorasolSurf;//�J�[�\��
static NmMusic KMusicSurf;//��
#define MENU_MAX 5;			//���h���g
SCENE_NO menuK[4] = { SCENE_GAME1, SCENE_GAME1, SCENE_GAME3, };
//�e�평����
BOOL initKurezit(void){
	//�C���X�g�n�̏�����
	//�w�i
	Nm2DInit(&KbackSurf);
	Nm2DOpen(&KbackSurf, "res/haikei.png", 1280, 720);
	//�����X�^�[�Љ�
	Nm2DInit(&KMSurf);
	Nm2DOpen(&KMSurf, "res/kurezit/KMK.png", 792, 100);
	//�f�ރ^�u
	Nm2DInit(&KUSurf);
	Nm2DOpen(&KUSurf, "res/kurezit/KU.png", 310, 101);
	//�߂�{�^��
	Nm2DInit(&KRSurf);
	Nm2DOpen(&KRSurf, "res/modoru2.png", 150, 120, D3DCOLOR_RGBA(0, 255, 0, 0));
	//�w�i�Љ�
	Nm2DInit(&KBsurf);
	Nm2DOpen(&KBsurf, "res/kurezit/KB.png", 687, 101);
	//�ȏЉ�
	Nm2DInit(&KMKSurf);
	Nm2DOpen(&KMKSurf, "res/kurezit/KM.png", 687, 100);
	//���S�Љ�
	Nm2DInit(&KWSurf);
	Nm2DOpen(&KWSurf, "res/kurezit/KW.png", 1092, 100);
	//���ӕ�
	Nm2DInit(&KKSurf);
	Nm2DOpen(&KKSurf, "res/kurezit/KK.png", 897, 101);
	//BGM
	NmMusicInit(&KMusicSurf);
	NmMusicOpen(&KMusicSurf, "res/MOUSIC/game_maoudamashii_7_event34.mid", 0);
	NmMusicCont(&KMusicSurf);
	//�J�[�\��
	Nm2DInit(&KCorasolSurf);
	Nm2DOpen(&KCorasolSurf, "res/crasol.png", 50, 50, D3DCOLOR_RGBA(0, 255, 0, 0));
	return TRUE;
}
//�t���[������
void moveKurezit(DWORD dwTickDiff){
	//�V�[���ړ�
	if (GetMouseButtonLWait()){
		if (GetMouseX() > 1050 && GetMouseX()<1050 + 150 &&
			GetMouseY()>10 && GetMouseY() < 10 + 120){
			changeScene(menuK[SCENE_GAME1]);
		}
	}
}
//�����_�����O����
void renderKurezit(void){
	//�w�i�̕\��
	Nm2DRender(&KbackSurf);
	//�߂�{�^���̕\��
	Nm2DSet(&KRSurf, 1050, 10);
	Nm2DRender(&KRSurf);
	//���̕\��
	//�f��
	Nm2DSet(&KUSurf, 450, 100);
	Nm2DRender(&KUSurf);
	//�w�i
	Nm2DSet(&KBsurf, 350, 200);
	Nm2DRender(&KBsurf);
	//�����X�^�[
	Nm2DSet(&KMSurf, 150, 300);
	Nm2DRender(&KMSurf);
	//��
	Nm2DSet(&KMKSurf, 350, 400);
	Nm2DRender(&KMKSurf);
	//����
	Nm2DSet(&KWSurf, 60, 500);
	Nm2DRender(&KWSurf);
	//����
	Nm2DSet(&KKSurf, 250, 600);
	Nm2DRender(&KKSurf);
	//�J�[�\��
	Nm2DSet(&KCorasolSurf, GetMouseX(), GetMouseY());
	Nm2DRender(&KCorasolSurf);
}
//�V�[���I����̏���
void releaseKurezit(void){
	Nm2DDeleteSurface(&KbackSurf);
	Nm2DDeleteSurface(&KBsurf);
	Nm2DDeleteSurface(&KCorasolSurf);
	Nm2DDeleteSurface(&KMSurf);
	Nm2DDeleteSurface(&KMKSurf);
	Nm2DDeleteSurface(&KWSurf);
	Nm2DDeleteSurface(&KKSurf);
	Nm2DDeleteSurface(&KRSurf);
	NmMusicDelete(&KMusicSurf);
}
void KurezitCollideCallback(int nSrc, int nTarget, int nCollideID){}