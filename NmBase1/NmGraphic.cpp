/********************************************************/
/* DirectX  ゲームプログラミング						*/
/*						ゲームライブラリー				*/
/*														*/
/* NmGraphic.cpp - DirectXGraphics関連処理				*/
/*														*/
/*					沼津情報専門学校　Atsushi Ohba		*/
/*						Copyright (c) 2005-2005			*/
/*						All Rights Reserved.			*/
/********************************************************/
#include "NmBase1.h"

// グローバル変数の宣言
// ダイレクトＸグラフィック系
LPDIRECT3DDEVICE9			g_pD3DDevice  = NULL;	// ダイレクト３Ｄデバイス

static LPDIRECT3D9			g_pD3DObject  = NULL;	// ダイレクト３Ｄオブジェクト
static D3DVIEWPORT9			g_ViewPort;				// ビューポート
static float				g_fAlpha;				// アルファブレンド


//  プロトタイプ宣言
LPDIRECT3DDEVICE9  GetD3DDevice(void) { return g_pD3DDevice; }
BOOL GrpInit(HWND hwnd);
void GrpRelease(void);


//	ダイレクトグラフィックス初期化関数
BOOL GrpInit(HWND hwnd)
{
	
	// Direct3Dオブジェクトの取得
	g_pD3DObject = Direct3DCreate9(D3D_SDK_VERSION);
	if (g_pD3DObject == NULL) return FALSE;
	
	D3DPRESENT_PARAMETERS	d3dppApp;

#if WINDOW_MODE
	D3DDISPLAYMODE dmode;
	// 現在のディスプレイモードを得る（DEFAULT指定でプライマリアダプタを選択）
	if(FAILED(g_pD3DObject->GetAdapterDisplayMode(D3DADAPTER_DEFAULT,&dmode)))	
		return FALSE;
	// バックサーフェースのフォーマットをコピーして使用する
	ZeroMemory(&d3dppApp,sizeof(d3dppApp));
	d3dppApp.BackBufferWidth			= SCREEN_WIDTH;						// 解像度セット
	d3dppApp.BackBufferHeight			= SCREEN_HEIGHT;
	d3dppApp.Windowed					= TRUE;						// ウィンドウモード
	d3dppApp.SwapEffect					= D3DSWAPEFFECT_DISCARD;
	d3dppApp.BackBufferFormat			= dmode.Format;
	d3dppApp.BackBufferCount			= 1;
	d3dppApp.EnableAutoDepthStencil     = TRUE;
	d3dppApp.AutoDepthStencilFormat		= DEPTHFORMAT;				// Ｚバッファビット深度１６Ｂｉｔ

	// デバイスの作成
	if(FAILED(g_pD3DObject->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,hwnd,D3DCREATE_HARDWARE_VERTEXPROCESSING,&d3dppApp,&g_pD3DDevice))){
		if(FAILED(g_pD3DObject->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,hwnd,D3DCREATE_SOFTWARE_VERTEXPROCESSING,&d3dppApp,&g_pD3DDevice))){
			return FALSE;
		}
	}
#else
	// デバイス生成情報をセット
	ZeroMemory(&d3dppApp,sizeof(d3dppApp));
	d3dppApp.BackBufferWidth			= SCREEN_WIDTH;						// 解像度セット
	d3dppApp.BackBufferHeight			= SCREEN_HEIGHT;
	d3dppApp.BackBufferFormat			= D3DFMT_X8R8G8B8;			// ピクセルフォーマットを指定
	d3dppApp.Windowed					= FALSE;					// フルスクリーン
	d3dppApp.BackBufferCount			= 1;
	d3dppApp.SwapEffect					= D3DSWAPEFFECT_DISCARD;
    d3dppApp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;	
	d3dppApp.EnableAutoDepthStencil     = TRUE;
	d3dppApp.AutoDepthStencilFormat		= DEPTHFORMAT;				// Ｚバッファビット深度１６Ｂｉｔ
	
	// デバイスの作成
	if (FAILED(g_pD3DObject->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,hwnd,D3DCREATE_SOFTWARE_VERTEXPROCESSING,&d3dppApp,&g_pD3DDevice))) {
		return FALSE;
	}
#endif

	// レンダリング色 = テクセル色と頂点色の平均
	g_pD3DDevice->SetTextureStageState(0,D3DTSS_COLOROP,D3DTOP_MODULATE);
	g_pD3DDevice->SetTextureStageState(0,D3DTSS_COLORARG1,D3DTA_TEXTURE);
	g_pD3DDevice->SetTextureStageState(0,D3DTSS_COLORARG2,D3DTA_DIFFUSE);

	// アルファ値 = テクセルアルファと頂点アルファの平均
	g_pD3DDevice->SetTextureStageState(0,D3DTSS_ALPHAOP,D3DTOP_MODULATE);
	g_pD3DDevice->SetTextureStageState(0,D3DTSS_ALPHAARG1,D3DTA_TEXTURE);
	g_pD3DDevice->SetTextureStageState(0,D3DTSS_ALPHAARG2,D3DTA_DIFFUSE);

	// Ｚバッファを有効にする
	g_pD3DDevice->SetRenderState(D3DRS_ZENABLE,D3DZB_TRUE);
	g_pD3DDevice->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);
	
	// ビューポートの設定
	g_ViewPort.X		= 0;
	g_ViewPort.Y		= 0;
	g_ViewPort.Width	= SCREEN_WIDTH;
	g_ViewPort.Height	= SCREEN_HEIGHT;
	g_ViewPort.MinZ		= 0.0f;
	g_ViewPort.MaxZ		= 1.0f;
	g_pD3DDevice->SetViewport( &g_ViewPort );
	

	g_pD3DDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
	g_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE,TRUE);

	g_pD3DDevice->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
	g_pD3DDevice->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);	

	g_fAlpha = 1.0f;
	
	return TRUE;
}



//	ダイレクトグラフィックス開放関数
void GrpRelease(void)
{
	
	RELEASE(g_pD3DDevice);							// ダイレクト３Ｄデバイス開放
	RELEASE(g_pD3DObject);							// ダイレクト３Ｄオブジェクト開放
}


////////////////////////////////////////////////////////////////////////////
//	関数名	：	NmFpsDisp
//	処理概要：	ＦＰＳ表示
////////////////////////////////////////////////////////////////////////////
void NmFpsDisp(NmFont *pFont, int x, int y)
{
	static unsigned int start = 0;
	static unsigned int end   = 0;

	char buf[100];

	end = timeGetTime();
	float fps = (float)1000 / (end - start);
	sprintf_s(buf,sizeof(buf),"FPS : %5.1f",fps);
	if( fps < 25.0f ) strcat_s( buf,sizeof(buf), "!!" );
	
	NmFontDrawText(pFont,buf, x, y, D3DCOLOR_RGBA(255, 255, 255, 255));

	start = end;
}



// アルファブレンド
void NmSetAlpha( float alpha )
{
	if( alpha > 1.0f ) alpha = 1.0f;
	if( alpha < 0.0f ) alpha = 0.0f;
	g_fAlpha = alpha;
}

void NmAddAlpha( float alpha )
{
	g_fAlpha += alpha;
	if( g_fAlpha > 1.0f ) g_fAlpha = 1.0f;
	if( g_fAlpha < 0.0f ) g_fAlpha = 0.0f;
}

float NmGetAlpha(  )
{
	return g_fAlpha;
}

void NmSetAlpha( float alpha, float time )
{
}

void NmUpdateAlpha(  )
{
}

