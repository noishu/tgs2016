// GameScene.cppファイル内の関数のうち、他のファイルから呼び出される関数のプロトタイプ宣言を記述する

BOOL initGame2Scene(void);
void moveGame2Scene(DWORD dwTickDiff);
void renderGame2Scene(void);
void releaseGame2Scene(void);
void Game2SceneCollideCallback( int nSrc, int nTarget, int nCollideID );

//関数の追加
//void MapSave( char* _FileName );
//void MapLoad( char* _FileName );
