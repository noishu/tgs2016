/********************************************************/
/* DirectX  ゲームプログラミング						*/
/*						ゲームライブラリー				*/
/*														*/
/* NmSound.cpp - サウンド								*/
/*														*/
/*		オリジナル		　			  Oikawa			*/
/*		改編		沼津情報専門学校　Atsushi Ohba		*/
/*						Copyright (c) 2003-2004			*/
/*						All Rights Reserved.			*/
/********************************************************/


#define STRICT
#include "NmBase1.h"

#define DSBCAPS_CTRLDEFAULT         0x000000E0

/////////////////////////////////////////////////////////////////////////////
// CStdFile
// 標準ファイルラッパー

//-----------------------------------------------------
//	コンストラクタ
//-----------------------------------------------------
CStdFile::CStdFile()
{
	m_pFileName = NULL;
	m_pFile = NULL;
}

//-----------------------------------------------------
//	デストラクタ
//-----------------------------------------------------
CStdFile::~CStdFile()
{
	if(m_pFile)
		fclose(m_pFile);
}

//-----------------------------------------------------
//	開く
//-----------------------------------------------------
BOOL CStdFile::Open(LPCTSTR pFileName,UINT nMode)
{
	static LPCTSTR Mode[3] = {"rb","wb","rb+"};
	if(m_pFile)
		fclose(m_pFile);
	m_pFileName = pFileName;
	fopen_s(&m_pFile, pFileName,Mode[nMode]);
	return (BOOL)m_pFile;
}

//-----------------------------------------------------
//	読む
//-----------------------------------------------------
UINT CStdFile::Read(LPVOID pBuff,UINT nCount)const
{
	if(!m_pFile)
		return FALSE;
	return fread(pBuff,1,nCount,m_pFile);
}

//-----------------------------------------------------
//	書く
//-----------------------------------------------------
UINT CStdFile::Write(LPVOID pBuff,UINT nCount)const
{
	if(!m_pFile)
		return FALSE;
	return fwrite(pBuff,1,nCount,m_pFile);
}

//-----------------------------------------------------
//	数える
//-----------------------------------------------------
DWORD CStdFile::GetLength()const
{
	if(!m_pFile)
		return FALSE;
	long lSeek = ftell(m_pFile);
	fseek(m_pFile,0,SEEK_END);
	long lSeekRet = ftell(m_pFile);
	fseek(m_pFile,lSeek,SEEK_SET);
	return lSeekRet;
}

/////////////////////////////////////////////////////////
//CResourceFile
//リソースファイル

//-----------------------------------------------------
//	コンストラクタ
//-----------------------------------------------------
CResourceFile::CResourceFile()
{
	m_hRsrc = 0;
	m_hGlobal = 0;
	m_pVoid = NULL;
}

//-----------------------------------------------------
//	デストラクタ
//-----------------------------------------------------
CResourceFile::~CResourceFile()
{
	Close();
}

//-----------------------------------------------------
//	開く
//-----------------------------------------------------
BOOL CResourceFile::Open(LPCSTR pName,LPCSTR pType)
{
	Close();
	m_pFileName = pName;
	m_hRsrc = FindResource(NULL,pName,pType);
	if(!m_hRsrc)
		return FALSE;
	m_hGlobal = LoadResource(NULL,m_hRsrc);
	m_pVoid = LockResource(m_hGlobal);
	m_dwSeek = 0;
	return TRUE;
}

//-----------------------------------------------------
//	閉じる
//-----------------------------------------------------
void CResourceFile::Close()
{
	if(m_hGlobal)
		m_hGlobal = 0;
}

//-----------------------------------------------------
//	読む
//-----------------------------------------------------
BOOL CResourceFile::Read(LPVOID pVoid,DWORD dwSeek)
{
	CopyMemory(pVoid,(LPBYTE)m_pVoid+m_dwSeek,dwSeek);
	m_dwSeek += dwSeek;
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CPressFile
// 圧縮ファイル

//-----------------------------------------------------
//	コンストラクタ
//-----------------------------------------------------
CPressFile::CPressFile()
{
	ZeroMemory(&m_Dth,sizeof(m_Dth));
	m_nSeek = 0;
	m_nSrcPt = 0;
	m_pPackName = NULL;
	m_pFileName = NULL;
	m_pData = NULL;
}
//-----------------------------------------------------
//	デストラクタ
//-----------------------------------------------------
CPressFile::~CPressFile()
{
	if(m_pPackName)
		delete m_pPackName;
	if(m_pFileName)
		delete m_pFileName;
	if(m_pData)
		delete m_pData;
}
//-----------------------------------------------------
//	圧縮ファイルの作成
//-----------------------------------------------------
BOOL CPressFile::Pack(LPCTSTR pFileName,LPCTSTR pPackName)
{
	int i,j,k;
	FILE* pFile;
	long lFileSize;
	int nFlagPt = 0;
	int nDestPt = 0;

	fopen_s(&pFile, pFileName,"rb");
	fseek(pFile,0L,SEEK_END);
	lFileSize = ftell(pFile);
	fseek(pFile,0L,SEEK_SET);
	char* pSrcData = new char[lFileSize];
	fread(pSrcData,lFileSize,1,pFile);
	fclose(pFile);

	char* pFlag = new char[(lFileSize+7)/8];
	for(i=0;i<(lFileSize+7)/8;i++)
		pFlag[i] = 0;
	char* pData = new char[lFileSize];
	
	for(i=0;i<lFileSize;i++)
	{
		int nKeep;
		int nMax=2;
		int nSet = i - 4096-2;
		if(nSet < 0)
			nSet = 0;
		for(j=nSet;j<i-1;j++)
		{
			for(k=0;j+k<i && i+k<lFileSize && pSrcData[j+k] == pSrcData[i+k] && k<16+2;k++);
			if(k > nMax)
			{
				nMax = k;
				nKeep = i-j-3;
				if(nMax == 18)
					break;
			}
		}
		if(nMax < 3)
		{
			pFlag[nFlagPt/8] |= BYTE(1<<(nFlagPt%8));
			pData[nDestPt] = pSrcData[i];
			nDestPt++;
		}
		else
		{
			*(unsigned short*)&pData[nDestPt] = (unsigned short)((nKeep<<4)+nMax-3);
			i+=nMax-1;
			nDestPt+=2;
		}
		nFlagPt++;
	}
	delete pSrcData;

	errno_t err;
	if(!pPackName)
	{
		LPTSTR pPack = new TCHAR[strlen(pFileName)+5];
		strcpy_s(pPack,strlen(pFileName)+5,pFileName);
		LPTSTR pWork = strrchr(pPack,'.');
		if(pWork)
			*pWork = 0;
		strcat_s(pPack,strlen(pFileName)+5,".dpa");
		err = fopen_s(&pFile, pPack,"wb");
		delete pPack;
	}
	else
		err = fopen_s(&pFile, pPackName,"wb");
	if(err)
	{
		delete pData;
		delete pFlag;
		return FALSE;
	}
	SDxmpHeader Dph;
	Dph.nVersion = 100;
	Dph.nSize = sizeof(SDxmpTHeader);
	strcpy_s(Dph.cCheak,5,"DXMP");

	SDxmpTHeader Dpth;
	Dpth.nType = 1;
	Dpth.nNameSize = strlen(pFileName)+1;
	Dpth.nFlagSize = (nDestPt+7)/8;
	Dpth.nDataSize = nDestPt;
	Dpth.nFileSize = lFileSize;
	fwrite(&Dph,sizeof(Dph),1,pFile);
	fwrite(&Dpth,sizeof(Dpth),1,pFile);
	fwrite(pFileName,Dpth.nNameSize,1,pFile);
	fwrite(pFlag,Dpth.nFlagSize,1,pFile);
	fwrite(pData,Dpth.nDataSize,1,pFile);

	fclose(pFile);
	delete pData;
	delete pFlag;
	return TRUE;
}

//-----------------------------------------------------
//　圧縮ファイルを開く
//-----------------------------------------------------
BOOL CPressFile::Open(LPCSTR pFileName)
{
	FILE* pFile;

	if(m_pPackName)
	{
		delete m_pPackName;
		m_pPackName = NULL;
	}
	errno_t err;
	err = fopen_s(&pFile, pFileName,"rb");
	if(err)
	{
		LPTSTR pName = GetPackName(pFileName);
		err = fopen_s(&pFile, pName,"rb");
		if(err)
		{
			delete pName;
			return FALSE;
		}
		m_pPackName = pName;
	}
	else
	{
		m_pPackName = new CHAR[strlen(pFileName)+1];
		strcpy_s(m_pPackName, strlen(pFileName)+1,pFileName);
	}
	SDxmpHeader Dph;
	fread(&Dph,sizeof(Dph),1,pFile);
	if(strcmp(Dph.cCheak,"DXMP"))
		return FALSE;
	SDxmpTHeader* pDpth = (SDxmpTHeader*) new BYTE[Dph.nSize];
	fread(pDpth,Dph.nSize,1,pFile);
	if(m_pFileName)
		delete m_pFileName;
	m_pFileName = new char[pDpth->nNameSize];
	fread(m_pFileName,pDpth->nNameSize,1,pFile);
	m_nSrcPt = ftell(pFile);
	fclose(pFile);
	m_Dth = *pDpth;
	delete pDpth;
	m_pMemData = 0;
	return TRUE;
}

//-----------------------------------------------------
//	メモリ上の圧縮データの展開
//-----------------------------------------------------
BOOL CPressFile::Open(LPBYTE pHeader)
{
	SDxmpHeader Dph;
	CopyMemory(&Dph,&pHeader[0],sizeof(Dph));
	if(strcmp(Dph.cCheak,"DXMP"))
		return FALSE;
	m_nSrcPt += sizeof(Dph);
	SDxmpTHeader* pDpth = (SDxmpTHeader*) new BYTE[Dph.nSize];
	CopyMemory(pDpth,&pHeader[m_nSrcPt],Dph.nSize);
	m_nSrcPt += Dph.nSize;
	if(m_pFileName)
		delete m_pFileName;
	m_pFileName = new char[pDpth->nNameSize];
	CopyMemory(m_pFileName,&pHeader[m_nSrcPt],pDpth->nNameSize);
	m_nSrcPt += pDpth->nNameSize;
	m_Dth = *pDpth;
	delete pDpth;
	m_pMemData = pHeader;
	return TRUE;
}

//-----------------------------------------------------
//	圧縮データの読み出し
//-----------------------------------------------------
BOOL CPressFile::Read(LPVOID pVoid,DWORD dwSeek)
{
	CopyMemory(pVoid,&GetData()[m_nSeek],dwSeek);
	m_nSeek += dwSeek;
	return TRUE;
}

//-----------------------------------------------------
//	圧縮ファイルを展開
//-----------------------------------------------------
BOOL CPressFile::Unpack()
{
	int i,j,k;
	int nSrcPt = 0;
	char* pFlag;
	char* pData;

	pFlag = new char[m_Dth.nFlagSize];
	pData = new char[m_Dth.nDataSize];
	if(m_pData)
		delete m_pData;
	m_pData = new BYTE[m_Dth.nFileSize];

	if(!m_pMemData)
	{
		errno_t err;
		FILE* pFile;
		err = fopen_s(&pFile, m_pPackName,"rb");
		if(err)
		{
			delete pFlag;
			delete pData;
			return FALSE;
		}
		fseek(pFile,m_nSrcPt,SEEK_SET);
		fread(pFlag,m_Dth.nFlagSize,1,pFile);
		fread(pData,m_Dth.nDataSize,1,pFile);
		fclose(pFile);
	}
	else
	{
		CopyMemory(pFlag,&m_pMemData[m_nSrcPt],m_Dth.nFlagSize);
		CopyMemory(pData,&m_pMemData[m_nSrcPt+m_Dth.nFlagSize],m_Dth.nDataSize);
	}

	for(i=0,j=0;i<m_Dth.nDataSize;i++,j++)
	{
		if(pFlag[j/8] & (1<<(j%8)))
		{
			m_pData[nSrcPt] = pData[i];
			nSrcPt++;
		}
		else
		{
			unsigned short sData = *(unsigned short*)&pData[i];
			int nMax = (sData & 15) + 3;
			int nPt = (sData >> 4) + 3;
			for(k=0;k<nMax;k++)
			{
				m_pData[nSrcPt] = m_pData[nSrcPt-nPt];
				nSrcPt++;
			}
			i++;
		}
	}
	delete pData;
	delete pFlag;
	return TRUE;
}

//-----------------------------------------------------
//	１行を得る
//-----------------------------------------------------
LPSTR CPressFile::Gets(LPSTR pStr,int nLength)
{
	int i;
	for(i=0;m_nSeek+i<GetLength() && i<nLength &&
		m_pData[m_nSeek+i]!='\n' && m_pData[m_nSeek+i]!='\0';i++)
		pStr[i] = m_pData[m_nSeek+i];
		pStr[i] = 0;
	m_nSeek += i;
	return pStr;
}

//-----------------------------------------------------
//	位置指定
//-----------------------------------------------------
BOOL CPressFile::SetSeek(long lSeek,int nOrigin)
{
	if(nOrigin == SEEK_CUR)
		m_nSeek += lSeek;
	else if(nOrigin == SEEK_END)
		m_nSeek = GetSize() + lSeek;
	else if(nOrigin == SEEK_SET)
		m_nSeek = lSeek;
	return TRUE;
}

//-----------------------------------------------------
//	圧縮ファイルの展開
//-----------------------------------------------------
BOOL CPressFile::Unpack(LPCSTR pFileName)
{
	if(!Open(pFileName))
		return FALSE;
	if(!Unpack())
		return FALSE;
	errno_t err;
	FILE* pFile;
	err = fopen_s(&pFile, m_pFileName,"wb");
	if(err)
		return FALSE;
	fwrite(m_pData,m_Dth.nFileSize,1,pFile);
	fclose(pFile);
	return TRUE;
}

//-----------------------------------------------------
//	圧縮前のファイル名を得る
//-----------------------------------------------------
LPTSTR CPressFile::GetPackName(LPCSTR pFileName)
{
	LPTSTR pPack = new TCHAR[strlen(pFileName)+5];
	strcpy_s(pPack, strlen(pFileName)+5,pFileName);
	LPTSTR pWork = strrchr(pPack,'.');
	if(pWork)
		*pWork = 0;
	strcat_s(pPack, strlen(pFileName)+5, ".dpa");
	return pPack;
}




/////////////////////////////////////////////////////////////////////////////
// CDxmFile
// ファイル／リソース／圧縮データ、自動判定クラス

//-----------------------------------------------------
//	コンストラクタ
//-----------------------------------------------------
CDxmFile::CDxmFile()
{
	m_pRFile = NULL;
	m_pSFile = NULL;
	m_pPFile = NULL;
	m_pType = NULL;
}

//-----------------------------------------------------
//	デストラクタ
//-----------------------------------------------------
CDxmFile::~CDxmFile()
{
	if(m_pType)
		delete m_pType;
	if(m_pRFile)
		delete m_pRFile;
	if(m_pSFile)
		delete m_pSFile;
	if(m_pPFile)
		delete m_pPFile;
}

//-----------------------------------------------------
//	圧縮前のサイズ
//-----------------------------------------------------
int CDxmFile::GetLength()const
{
	int nByte = 0;
	switch(m_nType)
	{
	case 1:
		nByte = m_pSFile->GetLength();
		break;
	case 2:
		nByte = m_pPFile->GetSize();
		break;
	case 3:
		nByte = m_pRFile->GetLength();
		break;
	}
	return nByte;
}
BOOL CDxmFile::SetSeek(long lSeek,int nOrigin)
{
	BOOL bFlag = FALSE;
	switch(m_nType)
	{
	case 1:
		bFlag = m_pSFile->SetSeek(lSeek,nOrigin);
		break;
	case 2:
		bFlag = m_pPFile->SetSeek(lSeek,nOrigin);
		break;
	case 3:
		bFlag = m_pRFile->SetSeek(lSeek,nOrigin);
		break;
	}
	return bFlag;
}
LPCSTR CDxmFile::GetFileName()
{
	switch(m_nType)
	{
	case 1:
		return m_pSFile->GetFileName();
	case 2:
		return m_pPFile->GetFileName();
	case 3:
		return m_pRFile->GetFileName();
	}
	return NULL;
}
long CDxmFile::GetSeek()
{
	long lSeek = 0;
	switch(m_nType)
	{
	case 1:
		lSeek = m_pSFile->GetSeek();
		break;
	case 2:
		lSeek = m_pPFile->GetSeek();
		break;
	case 3:
		lSeek = m_pRFile->GetSeek();
		break;
	}
	return lSeek;
}
LPSTR CDxmFile::Gets(LPSTR pStr,int nLength)
{
	LPSTR pRStr = NULL;
	switch(m_nType)
	{
	case 1:
		pRStr = m_pSFile->Gets(pStr,nLength);
		break;
	case 2:
		pRStr = m_pPFile->Gets(pStr,nLength);
		break;
	case 3:
		pRStr = m_pRFile->Gets(pStr,nLength);
		break;
	}
	return pRStr;
}
//-----------------------------------------------------
//	リソースタイプ名の指定
//-----------------------------------------------------
void CDxmFile::SetType(LPCSTR pType)
{
	if(m_pType)
		delete m_pType;
	if(pType)
	{
		m_pType = new CHAR[strlen(pType)+1];
		strcpy_s(m_pType, strlen(pType)+1, pType);
	}
	else
		m_pType = NULL;
}

//-----------------------------------------------------
//	ファイルを自動判定で開く
//-----------------------------------------------------
BOOL CDxmFile::Open(LPCTSTR pFileName)
{
	if(!pFileName)		//ファイル名のチェック
		return FALSE;
	if(m_pRFile)		//不必要なポインタの解放
	{	delete m_pRFile; m_pRFile = 0;	}
	if(m_pSFile)
	{	delete m_pSFile; m_pSFile = 0;	}
	if(m_pPFile)
	{	delete m_pPFile; m_pPFile = 0;	}
	if(m_pSFile)
	{	delete m_pSFile; m_pSFile = 0;	}
	
	//圧縮ファイル
	m_pPFile = new CPressFile;
	if(m_pPFile->Open(pFileName))
	{
		m_pPFile->Unpack();
		m_nType = 2;
		return TRUE;
	}
	//通常ファイル
	m_pSFile = new CStdFile;
	if(m_pSFile->Open(pFileName,0))
	{
		m_nType = 1;
		return TRUE;
	}
	delete m_pSFile;
	m_pSFile = 0;
	//リソースファイル
	m_pRFile = new CResourceFile;
	if(m_pRFile->Open(pFileName,m_pType))
	{
		//リソース圧縮ファイル
		if(!m_pPFile->Open((LPBYTE)m_pRFile->GetData()))
		{
			delete m_pPFile;
			m_pPFile = 0;
			m_nType = 3;
		}
		else
		{
			m_pPFile->Unpack();
			m_nType = 2;
		}
		return TRUE;
	}
	delete m_pRFile;
	m_pRFile = 0;
	return FALSE;
}

//-----------------------------------------------------
//	データの読み出し
//-----------------------------------------------------
BOOL CDxmFile::Read(LPVOID pVoid,DWORD dwSeek)
{
	BOOL bFlag = FALSE;
	switch(m_nType)
	{
	case 1:
		bFlag = m_pSFile->Read(pVoid,dwSeek);
		break;
	case 2:
		bFlag = m_pPFile->Read(pVoid,dwSeek);
		break;
	case 3:
		bFlag = m_pRFile->Read(pVoid,dwSeek);
		break;
	}
	return bFlag;
}
//-----------------------------------------------------
//	データの読み出し
//-----------------------------------------------------
int CDxmFile::GetChar()
{
	CHAR cData;
	switch(m_nType)
	{
	case 1:
		m_pSFile->Read(&cData,1);
		break;
	case 2:
		m_pPFile->Read(&cData,1);
		break;
	case 3:
		m_pRFile->Read(&cData,1);
		break;
	}
	return cData;
}



//////////////////////////////////////////////////////////
//NmSound

//サウンド再生の初期化
void NmSoundInit( NmSound *pSound )
{
	pSound->m_nCount = 0;
	pSound->m_ppDSBuffer = NULL;
	pSound->m_pWave = NULL;
	pSound->m_hWnd = 0;
	pSound->m_pDirectSound = NULL;
	pSound->m_pDirectSoundCreate = NULL;
	pSound->m_hDirectSound = ::LoadLibrary("dsound.dll");
	if(pSound->m_hDirectSound)
	{
		pSound->m_pDirectSoundCreate = (DSoundCreate*)::GetProcAddress(pSound->m_hDirectSound,"DirectSoundCreate");
		pSound->m_pDirectSoundCreate(NULL,&pSound->m_pDirectSound,NULL);
		if(pSound->m_pDirectSound)
			NmSoundSetCoopWnd( pSound );
	}
}

//サウンドの削除(デバイス設定も含む為、ゲームの終了時はこちらで)
void NmSoundDelete( NmSound *pSound )
{
	NmSoundRelease( pSound );
	NmSoundClose( pSound );

	if(pSound->m_pDirectSound)
		pSound->m_pDirectSound->Release();
	pSound->m_pDirectSound = NULL;


}

//サウンドデータの削除のみ
void NmSoundClose( NmSound *pSound )
{
	NmSoundRelease( pSound );

	if(pSound->m_pWave)
		delete pSound->m_pWave;
	pSound->m_pWave = NULL;

}

//なにこれ？
BOOL NmSoundSetCoopWnd(NmSound *pSound, HWND hWnd)
{
	if(!pSound->m_pDirectSound || (hWnd && (pSound->m_hWnd == hWnd)))
		return FALSE;
	
	if(!hWnd)
	{
		hWnd = ::GetActiveWindow();
		if(hWnd)
			pSound->m_hWnd = hWnd;
	}
	else
		pSound->m_hWnd = hWnd;

	return (pSound->m_pDirectSound->SetCooperativeLevel(pSound->m_hWnd,DSSCL_NORMAL)==DS_OK);
}

//サウンドデータの読込(サウンド変数、ファイルパス、リソースタイプ名(サウンドの種類設定など)指定しなければ0に指定する)
BOOL NmSoundOpen(NmSound *pSound,LPCSTR pName,LPCSTR pType)
{
	NmSoundClose( pSound );
	//ＤＩＢヘッダ情報の読み出し
	CDxmFile File;
	DWORD dwDataLength;
	File.SetType(pType);
	if(!File.Open(pName))
		return FALSE;
	dwDataLength = File.GetLength();
	pSound->m_pWave = (_SWaveHeder*)new BYTE[dwDataLength];
	File.Read(pSound->m_pWave,dwDataLength);
	if(pSound->m_pWave->dwCheck != 0x46464952 && pSound->m_pWave->dwWaveCheck!=0x45564157)
		return FALSE;
	pSound->m_dwDataSize = *((LPDWORD)((LPBYTE)pSound->m_pWave+24+pSound->m_pWave->dwFmtSize));
	pSound->m_pbyData = (LPBYTE)pSound->m_pWave + pSound->m_pWave->dwFmtSize+28;
	NmSoundCreateList(pSound, 3);
	return TRUE;
}

BOOL NmSoundOpenResource(NmSound *pSound, LPCSTR pName,LPCSTR pType)
{
	NmSoundClose( pSound );

	CResourceFile File;
	DWORD dwDataLength;
	File.Open(pName,pType);
	dwDataLength = File.GetLength();
	pSound->m_pWave = (_SWaveHeder*)new BYTE[dwDataLength];
	File.Read(pSound->m_pWave,dwDataLength);
	if(pSound->m_pWave->dwCheck != 0x46464952 || pSound->m_pWave->dwWaveCheck!=0x45564157)
		return FALSE;
	NmSoundCreateList(pSound, 3);
	return TRUE;
}

BOOL NmSoundOpenFile(NmSound *pSound, LPCSTR pFileName)
{
	NmSoundClose( pSound );

	CStdFile File;
	DWORD dwDataLength;
	File.Open(pFileName,CStdFile::modeRead);
	dwDataLength = File.GetLength();
	pSound->m_pWave = (_SWaveHeder*)new BYTE[dwDataLength];
	File.Read(pSound->m_pWave,dwDataLength);
	if(pSound->m_pWave->dwCheck != 0x46464952 || pSound->m_pWave->dwWaveCheck!=0x45564157)
		return FALSE;
	NmSoundCreateList(pSound, 3);
	return TRUE;
}

void NmSoundCreateList(NmSound *pSound, int nCount)
{
	int i;
	if(!pSound->m_pWave)
		return;
	NmSoundRelease( pSound );
	pSound->m_ppDSBuffer = new LPDIRECTSOUNDBUFFER[nCount];
	pSound->m_nCount = nCount;
	for(i=0;i<pSound->m_nCount;i++)
		pSound->m_ppDSBuffer[i] = NmSoundCreatBuffer(pSound);
}

BOOL NmSoundPlay(NmSound *pSound, int nPan)
{
	int i;
	if(pSound->m_ppDSBuffer && pSound->m_ppDSBuffer[0] && NmSoundSetCoopWnd(pSound))
	{
		DWORD dwWord;
		for(i=0;i<pSound->m_nCount;i++)
		{
			if(pSound->m_ppDSBuffer[i])
			{
				pSound->m_ppDSBuffer[i]->GetStatus(&dwWord);
				if(dwWord != DSBSTATUS_PLAYING)
				{
					pSound->m_ppDSBuffer[i]->SetPan(nPan);
					return (pSound->m_ppDSBuffer[i]->Play(0,0,0)==DS_OK);
				}
			}
		}
		if(pSound->m_ppDSBuffer[pSound->m_nCount-1])
		{
			pSound->m_ppDSBuffer[pSound->m_nCount-1]->Stop();
			pSound->m_ppDSBuffer[pSound->m_nCount-1]->SetPan(nPan);
			pSound->m_ppDSBuffer[pSound->m_nCount-1]->SetCurrentPosition(0);
			return (pSound->m_ppDSBuffer[pSound->m_nCount-1]->Play(0,0,0)==DS_OK);
		}
		return FALSE;
	}
	else
		return PlaySound((char*)pSound->m_pWave,NULL,SND_MEMORY|SND_ASYNC);
}


void NmSoundRelease( NmSound *pSound )
{
	int i;
	if(pSound->m_ppDSBuffer)
	{
		for(i=0;i<pSound->m_nCount;i++)
		{
			if(pSound->m_ppDSBuffer[i])
				pSound->m_ppDSBuffer[i]->Release();
		}
		delete pSound->m_ppDSBuffer;
		pSound->m_nCount = 0;
		pSound->m_ppDSBuffer=NULL;
	}
}


LPDIRECTSOUNDBUFFER NmSoundCreatBuffer( NmSound *pSound )
{
	LPDIRECTSOUNDBUFFER pDSBuffer = NULL;
	if(pSound->m_pDirectSound)
	{
		DSBUFFERDESC Desc;
		ZeroMemory(&Desc,sizeof(DSBUFFERDESC));
		WAVEFORMATEX formatWaveEx;
		Desc.dwSize = sizeof(DSBUFFERDESC);
		Desc.dwFlags = DSBCAPS_CTRLDEFAULT |DSBCAPS_STATIC;
		Desc.dwBufferBytes = pSound->m_dwDataSize;
		formatWaveEx = pSound->m_pWave->formatWave;
		Desc.lpwfxFormat = &formatWaveEx;
		pSound->m_pDirectSound->CreateSoundBuffer(&Desc,&pDSBuffer,NULL);
		if(pDSBuffer)
		{
			LPVOID pData1;
			DWORD dwBytes1;
			pDSBuffer->Lock(0,pSound->m_dwDataSize,&pData1,&dwBytes1,NULL,NULL,0);
			CopyMemory(pData1,pSound->m_pbyData,dwBytes1);
			pDSBuffer->Unlock(pData1,dwBytes1,NULL,0);
		}
	}
	return pDSBuffer;
}







/////////////////////////////////////////////////////////////////////////////
//     CMdExclusive
//---------------------------------------------------------
CMdExclusive::~CMdExclusive()
{
	if(m_midiHDR.lpData)
		delete m_midiHDR.lpData;
}
void CMdExclusive::Set(int nCount,LPBYTE pData)
{
	m_midiHDR.lpData = new char[nCount+1];
	m_midiHDR.lpData[0] = (char)0xf0;
	CopyMemory(&m_midiHDR.lpData[1],pData,nCount);
	m_midiHDR.dwFlags = 0;
	m_midiHDR.dwBufferLength = nCount+1;
}

/////////////////////////////////////////////////////////////////////////////
// CDxMidiDevice
// MIDIデバイス制御用
//------------------------------------------------------------
//-------------------------------
//MIDI Device コンストラクタ
CDxMidiDevice::CDxMidiDevice()
{
	m_hMidi = 0;
	m_uDevice = 0;
}
//-------------------------------
//MIDI Device デストラクタ
CDxMidiDevice::~CDxMidiDevice()
{
	Close();
}
//-------------------------------
//MIDI Device 開く
BOOL CDxMidiDevice::Open(UINT uDeviceID)
{
	if(m_hMidi)
	{
		if(m_uDevice != uDeviceID)
			Close();
		else
			return TRUE;
	}
	m_uDevice = uDeviceID;
	return midiOutOpen(&m_hMidi,uDeviceID,NULL,NULL,CALLBACK_NULL) == MMSYSERR_NOERROR;
}
//-------------------------------
//MIDI Device 閉じる
BOOL CDxMidiDevice::Close()
{
	if(!m_hMidi)
		return FALSE;
	midiOutReset(m_hMidi);
	if(m_hMidi)midiOutClose(m_hMidi);
	m_hMidi = 0;
	return TRUE;
}
//-------------------------------
//MIDI Device ロングメッセージの送信
BOOL CDxMidiDevice::Out(LPMIDIHDR pMidiHdr) const
{
	if(!m_hMidi)
		return FALSE;
	midiOutPrepareHeader(m_hMidi,pMidiHdr,sizeof(MIDIHDR));
	return midiOutLongMsg(m_hMidi,pMidiHdr,sizeof(MIDIHDR)) == MMSYSERR_NOERROR;
}
//-------------------------------
//MIDI Device ショートメッセージの送信
BOOL CDxMidiDevice::Out(DWORD dwData) const
{
	if(!m_hMidi)
		return FALSE;
	return midiOutShortMsg(m_hMidi,dwData) == MMSYSERR_NOERROR;
}
//-------------------------------
//MIDI Device ショートメッセージの送信
BOOL CDxMidiDevice::Out(BYTE byData1,BYTE byData2,BYTE byData3) const
{
	if(!m_hMidi)
		return FALSE;
	UMidiData OutWork;
    OutWork.bbData[0] = byData1;
    OutWork.bbData[1] = byData2;
    OutWork.bbData[2] = byData3;
	return midiOutShortMsg(m_hMidi,OutWork.dwMsg) == MMSYSERR_NOERROR;
}
//-------------------------------
//MIDI Device デバイス名を得る
void CDxMidiDevice::GetDeviceName(int nIndex,LPSTR pString)
{
	MIDIOUTCAPS midiCaps;
	midiOutGetDevCaps(nIndex,&midiCaps,sizeof(midiCaps));
	strcpy_s(pString,256,midiCaps.szPname);
}


//------------------------------------------------------------
// CDxMTimer
// マルチメディアタイマー用
//------------------------------------------------------------

//-------------------------------
//Timer コンストラクタ
CDxMTimer::CDxMTimer()
{
	timeGetDevCaps(&m_timeCaps,sizeof(TIMECAPS));
	m_hTimer = 0;
}
//-------------------------------
//Timer デストラクタ
CDxMTimer::~CDxMTimer()
{
	Stop();
	timeEndPeriod(m_timeCaps.wPeriodMin);
}
//-------------------------------
//Timer タイマー開始制御
BOOL CDxMTimer::Start(UINT uDelay,LPTIMECALLBACK lpTimeProc,DWORD dwUser)
{
	Stop();
	timeBeginPeriod(m_timeCaps.wPeriodMin);
	
	if(lpTimeProc)
		m_hTimer = timeSetEvent(uDelay,m_timeCaps.wPeriodMin,lpTimeProc,dwUser,TIME_PERIODIC);
	else
		m_hTimer = timeSetEvent(uDelay,m_timeCaps.wPeriodMin,procTime,(DWORD)this,TIME_PERIODIC);
	return m_hTimer;
}
//-------------------------------
//Timer タイマー停止
BOOL CDxMTimer::Stop()
{
	if(m_hTimer)
		timeKillEvent(m_hTimer);
	timeEndPeriod(m_timeCaps.wPeriodMin);
	m_hTimer = 0;
	return TRUE;
}
//-------------------------------
//Timer コールバック用
void CALLBACK CDxMTimer::procTime(UINT uID,UINT uMsg,DWORD dwUser,DWORD dw1,DWORD dw2)
{
	CDxMTimer* pMTimer = (CDxMTimer*)dwUser;
	pMTimer->procEnter();
}

/////////////////////////////////////////////////////////////////////////////
// CPlayData::CTrackData
// .midファイル読み出し用

//-----------------------------------------------------
//	ステータス読みだし
//-----------------------------------------------------
BYTE CPlayData::CTrackData::GetStat()
{
	BYTE bbStat = *m_pByte;
	if(bbStat < (BYTE)0x80)
		bbStat = m_bbStatBack;
	else
	{
		m_bbStatBack = bbStat;
		++m_pByte;
	}
	return bbStat;
}

//-----------------------------------------------------
//	ヘッダー読みだし
//-----------------------------------------------------
DWORD CPlayData::CTrackData::SetHeader(STrackHeader* pTHeader)
{	
	BYTE bbStat;
	m_nStep = 0;
	m_nEStep = 0;
	m_dwWCount = 0;
	m_pTHeader = pTHeader;
	Init();
	
	while(IsPlay())
	{
		m_dwWCount += GetWait();
		bbStat = GetStat();
		if((bbStat&0xf0) == 0xc0 || (bbStat&0xf0) == 0xd0)
			m_pByte++;
		else if(bbStat == 0xfe)
			continue;
		else if(bbStat == 0xff)
		{
			if(*m_pByte != 0x51)
				m_nStep--;
			else
				m_nStep++;

			m_pByte++;
			m_pByte += GetWait();
		}
		else if(bbStat == 0xf0)
		{
			m_nEStep++;
			m_pByte += GetWait();
		}
		else
			m_pByte += 2;
		m_nStep++;
	}
	return m_dwWCount;
}


/////////////////////////////////////////////////////////////////////////////
// CPlayData
// 演奏データ用

//-----------------------------------------------------
//	コンストラクタ
//-----------------------------------------------------
CPlayData::CPlayData()
{
	m_pMCode = NULL;
	m_pECode = NULL;
	m_pTitle = NULL;
	m_dwPoint = 0;
}

//-----------------------------------------------------
//	データの解放
//-----------------------------------------------------
void CPlayData::ReleaseData()
{
	m_dwPoint = 0;
	m_wRCount = 0;
	if(m_pTitle)
		delete m_pTitle;
	m_pTitle = NULL;
	if(m_pMCode)
		delete[] m_pMCode;
	m_pMCode = NULL;
	if(m_pECode)
		delete[] m_pECode;
	m_pECode = NULL;
}

//-----------------------------------------------------
//	データファイルの読み出し
//-----------------------------------------------------
BOOL CPlayData::Read(const LPBYTE pData)
{
	int i;
	const SMidiHeader* pFormat;
	int nHSize;
	int wTracks;

	ReleaseData();
	pFormat = (const SMidiHeader*)pData;
	nHSize =  protConvDW(pFormat->dwBytes);
	wTracks = protConvW(pFormat->wTracks);
	m_wRCount = protConvW(pFormat->wCount);
	m_dwlAllCount = 0;
	m_dwlWorkCount = 0;
	STrackHeader** ppTHeader = new STrackHeader*[wTracks];
	ppTHeader[0] = (STrackHeader*)(pData + 8 + nHSize);
	for(i=1;i<wTracks;i++)
	{
		DWORD dwData = protConvDW(ppTHeader[i-1]->dwBytes)+8;
		ppTHeader[i] = (STrackHeader*)((LPBYTE)ppTHeader[i-1] + dwData);
	}
	Track(ppTHeader,wTracks);
	delete[] ppTHeader;
	return TRUE;
}

//-----------------------------------------------------
//	トラックごとの読み出し
//-----------------------------------------------------
void CPlayData::Track(STrackHeader** ppTHeader,int nTCount)
{
	int i;
	CTrackData* pTData = new CTrackData[nTCount];
	for(i=0;i<nTCount;i++)
		pTData[i].SetHeader(ppTHeader[i]);
	SetTracks(pTData,nTCount);
	delete[] pTData;
}

//-----------------------------------------------------
//	トラックデータの併合
//-----------------------------------------------------
void CPlayData::SetTracks(CTrackData* pTData,int nTracks)
{
	int i;
	BOOL bFlag = TRUE;;
	DWORD dwStep = 0;
	DWORD dwExStep = 0;
	DWORD dwNowStep = 0;
	DWORD dwNowEStep = 0;
	for(i=0;i<nTracks;i++)
	{
		pTData[i].Init();
		pTData[i].Next();
		dwStep += pTData[i].GetStep();
		dwExStep += pTData[i].GetEStep();
	}
	m_dwAllTime = 0;
	m_dwTempoCount = 0;
	m_dwStep = dwStep;
	m_pECode = new CMdExclusive[dwExStep];
	m_pMCode = new SMidiCode[dwStep+16];
	ZeroMemory(m_pMCode,sizeof(SMidiCode)*dwStep);
	while(bFlag)
	{
		bFlag = FALSE;
		DWORD dwNextCount = 0xffffffff;
		//Trackから次に取り出すデータまでのカウンタを取得
		for(i=0;i<nTracks;i++)
		{
			if(pTData[i].IsPlay())
			{
				bFlag = TRUE;
				if(pTData[i].GetNextCount() < dwNextCount)
					dwNextCount = pTData[i].GetNextCount();
			}
		}
		m_pMCode[dwNowStep].dwWait = dwNextCount;	//カウンタを入れる
		if(dwNextCount != 0xffffffff)
			m_dwlAllCount += dwNextCount;
		//Trackデータを併合
		for(i=0;i<nTracks;i++)
		{
			if(pTData[i].IsPlay())
			{
				if(dwNextCount == pTData[i].GetNextCount())
				{
					BYTE bbStat = pTData[i].GetStat();
					m_pMCode[dwNowStep].Mdf.bbData[0] = bbStat; 
					if((bbStat&0xf0) == 0xc0 || (bbStat&0xf0) == 0xd0)
					{
						m_pMCode[dwNowStep].Mdf.bbData[1] = pTData[i].GetData();
					}
					else if(bbStat == 0xfe)
						continue;
					else if(bbStat == 0xf0)
					{
						DWORD dwCount = pTData[i].GetWait();
						m_pECode[dwNowEStep].Set(dwCount,pTData[i].GetAdr());
						*((LPWORD)&m_pMCode[dwNowStep].Mdf.bbData[1]) = (WORD)dwNowEStep;
						pTData[i].SetSkip(dwCount);
						dwNowEStep++;
					}
					else if(bbStat == 0xff)
					{
						BYTE bbData = pTData[i].GetData();
						DWORD dwCount = pTData[i].GetWait();
						if(bbData==0x51)
						{
							DWORD dwTCount=0;
							for(int j=0;j<(int)dwCount;j++)
								dwTCount += (dwTCount<<8)+pTData[i].GetData();
							if(dwTCount)
							{
								if(m_dwTempoCount)
								{
									DWORDLONG dwlCount = m_dwlAllCount - m_dwlWorkCount;
									if(dwlCount > 0)
										m_dwAllTime += DWORD(dwlCount*m_dwTempoCount/(1000*GetRCount()));
								}
								m_dwTempoCount = dwTCount;
								*((LPWORD)&m_pMCode[dwNowStep].Mdf.bbData[1]) = ((LPWORD)&dwTCount)[0];
								*((LPWORD)&m_pMCode[dwNowStep+1].Mdf.bbData[1]) = ((LPWORD)&dwTCount)[1];
								dwNowStep++;
							}
						}
						else
						{
							if(bbData == 3 && i == 0)
							{
								if(m_pTitle)
									delete m_pTitle;
								m_pTitle = new char[dwCount+1];
								strncpy_s(m_pTitle,dwCount+1,(LPCSTR)pTData[i].GetString(),dwCount);
								m_pTitle[dwCount] = 0;
								pTData[i].SetSkip(dwCount);
							}
							else
							{
								pTData[i].SetSkip(dwCount);
							}
							--dwNowStep;
						}
					}
					else
					{
						m_pMCode[dwNowStep].Mdf.bbData[1] = pTData[i].GetData();
						m_pMCode[dwNowStep].Mdf.bbData[2] = pTData[i].GetData();
					}
					dwNowStep++;
					if(pTData[i].IsPlay())
						pTData[i].Next();
				}
				pTData[i].SetStep(dwNextCount);
			}
		}

	}
	DWORDLONG dwlCount = m_dwlAllCount - m_dwlWorkCount;
	if(dwlCount > 0)
		m_dwAllTime += DWORD(dwlCount*m_dwTempoCount/(1000*GetRCount()));
}

/////////////////////////////////////////////////////////////////////////////
// CElapse
// スレッドWAIT用関数

//-----------------------------------------------------
//	カウントを取得後リセット
//-----------------------------------------------------
DWORD CElapse::GetCount()
{
	DWORD dwTickCount = timeGetTime();
	DWORD dwCount = dwTickCount - m_dwCount;
	m_dwCount = dwTickCount;
	return dwCount;
}
//-----------------------------------------------------
//	カウントを取得
//-----------------------------------------------------
DWORD CElapse::GetCount2()const
{
	return timeGetTime() - m_dwCount;
}
void CElapse::AddCount(int nCount)
{
	m_dwCount += nCount;
}
//-----------------------------------------------------
//	指定時間スリープ＆経過時間考慮
//-----------------------------------------------------
void CElapse::Sleep(DWORD dwWaitCount)
{
	if(m_bValid)
	{
		if(m_nSkipTime > 500)
			m_nSkipTime = 0;
		int nWait = dwWaitCount - GetCount2() - m_nSkipTime;
		if(nWait < 0)
		{
			m_nSkipTime = -nWait;
			SleepEx(0,TRUE);
		}
		else
		{
			m_nSkipTime = 0;
			SleepEx(nWait,TRUE);
		}
		m_dwCount = timeGetTime();
	}
}


/////////////////////////////////////////////////////////////////////////////
// NmMusic

void NmMusicInit( NmMusic *pMusic )
{
	pMusic->m_uDevice = MIDI_MAPPER;
	pMusic->m_bFadeIn = pMusic->m_bFadeOut = FALSE;
	pMusic->m_bThread = FALSE;
	pMusic->m_bPlayOut = FALSE;
	pMusic->m_bLoop = TRUE;
	pMusic->m_bThrough = FALSE;
	pMusic->m_fRelativeVolume = 0;
	pMusic->m_nRelativeVolume = 0;
	pMusic->m_nRelativeTemp = 0;
	pMusic->m_dwlLoopSearch = 0;
	pMusic->m_cKey = 0;

}

BOOL NmMusicOpen(NmMusic *pMusic, LPCSTR pName, LPCSTR pType)
{
	NmMusicStop( pMusic );
	CDxmFile File;
	File.SetType(pType);
	if(!File.Open(pName))
		return FALSE;
	int nLength = File.GetLength();
	if(!nLength)
		return FALSE;
	LPBYTE pData = new BYTE[nLength];
	File.Read(pData,nLength);
	NmMusicStdMidi(pMusic, pData);
	delete[] pData;

	ZeroMemory(&pMusic->m_playInfo,sizeof(SPlayInfo));
	pMusic->m_playInfo.pTitle = NmMusicGetTitle( pMusic );
	pMusic->m_dwNowCount=0;
	pMusic->m_dwNowTime = 0;
	pMusic->m_wTemp = 250;
	pMusic->m_dwTempParam = 60000000 / 150;
	pMusic->m_bFadeIn = pMusic->m_bFadeOut = FALSE;
	
	return TRUE;
}

//---------------------------------------------------------
//     データの解放
//---------------------------------------------------------
BOOL NmMusicClose(NmMusic *pMusic)
{
	if(!NmMusicIsData(pMusic))
		return FALSE;
	NmMusicStop(pMusic);
	pMusic->m_MidiData.ReleaseData();
	return TRUE;
}
BOOL NmMusicDelete(NmMusic *pMusic)
{
	return NmMusicClose( pMusic );
}

//---------------------------------------------------------
//     演奏開始
//---------------------------------------------------------
BOOL NmMusicPlay(NmMusic *pMusic)
{
	if(!NmMusicIsData(pMusic))
		return FALSE;
	NmMusicStop(pMusic);
	pMusic->m_bStopFlag = FALSE;

	ZeroMemory(&pMusic->m_playInfo,sizeof(SPlayInfo));
	pMusic->m_playInfo.pTitle = NmMusicGetTitle(pMusic);
	pMusic->m_dwNowCount=0;
	pMusic->m_dwNowTime = 0;
	pMusic->m_wTemp = 250;
	pMusic->m_dwTempParam = 60000000 / 150;
	pMusic->m_bFadeIn = pMusic->m_bFadeOut = FALSE;

	pMusic->m_MidiData.Init();
	pMusic->m_MDevice.Open();
	NmMusicInsideSetTimer(pMusic);
	return TRUE;
}

//---------------------------------------------------------
//     演奏再開
//---------------------------------------------------------
BOOL NmMusicCont( NmMusic *pMusic )
{
	if(!NmMusicIsData(pMusic))
		return FALSE;
	pMusic->m_bStopFlag = FALSE;
//	pMusic->m_bFadeOut = FALSE;
//	pMusic->m_bFadeIn = FALSE;
	pMusic->m_fRelativeVolume = 0;
	if(!pMusic->m_MDevice.IsDevice())
		NmMusicInsideOpenDevice(pMusic);
	NmMusicInsideComeback(pMusic);
	return NmMusicInsideSetTimer(pMusic);
}


//---------------------------------------------------------
//     演奏停止
//---------------------------------------------------------
BOOL NmMusicStop( NmMusic *pMusic )
{
	pMusic->m_bStopFlag = TRUE;
	pMusic->m_MTimer.Stop();
	while(NmMusicIsPlayOut(pMusic));
	NmMusicInsideSetVolume(pMusic,-128);
	pMusic->m_MDevice.Close();
	pMusic->m_bFadeIn = FALSE;
	pMusic->m_bFadeOut = FALSE;

	int i;
	for(i=0;i<16;i++)
		ZeroMemory(pMusic->m_playInfo.Chn[i].bbNote,sizeof(BYTE[16]));
	return TRUE;
}


//---------------------------------------------------------
//     頭出し
//---------------------------------------------------------
BOOL NmMusicSearch(NmMusic *pMusic, DWORDLONG dwlCount)
{
	if(!NmMusicIsData(pMusic))
		return FALSE;
	pMusic->m_MidiData.Init();
	NmMusicInsideOpenDevice(pMusic);
	pMusic->m_bThrough = TRUE;
	while(pMusic->m_MidiData.IsPlay() && pMusic->m_MidiData.GetNowCount() < dwlCount)
	{
		pMusic->m_dwNowCount = 0;
		NmMusicInsideMidiOut(pMusic);
	}
	pMusic->m_bThrough = FALSE;
	NmMusicCont(pMusic);
	return TRUE;
}


//---------------------------------------------------------
//     フェードイン
//---------------------------------------------------------
BOOL NmMusicFadeIn(NmMusic *pMusic,int nTime,int nVolume)
{
	if(!NmMusicIsData(pMusic))
		return FALSE;
	pMusic->m_nFadeVolume = nVolume;
	pMusic->m_fRelativeVolume = (float)nVolume;
	pMusic->m_nFadeCount = nTime;
	pMusic->m_nFadeCountWork = 0;
	pMusic->m_bFadeOut = FALSE;
	pMusic->m_bFadeIn = TRUE;
	NmMusicCont(pMusic);
	return TRUE;
}

//---------------------------------------------------------
//     フェードアウト
//---------------------------------------------------------
BOOL NmMusicFadeOut(NmMusic *pMusic, int nTime)
{
	if(!NmMusicIsData(pMusic))
		return FALSE;
	pMusic->m_fRelativeVolume = (float)0;
	pMusic->m_nFadeCount = nTime/pMusic->m_nTimeCycle;
	pMusic->m_nFadeCountWork=0;
	pMusic->m_bFadeIn = FALSE;
	pMusic->m_bFadeOut = TRUE;
	return TRUE;
}

//---------------------------------------------------------
//---------------------------------------------------------
BOOL NmMusicSetDevice(NmMusic *pMusic, int nIndex)
{
	pMusic->m_uDevice = nIndex;
	if(NmMusicIsPlay(pMusic))
	{
		NmMusicStop(pMusic);
		DWORDLONG dwlCount = NmMusicGetPlayCount(pMusic);
		NmMusicSearch(pMusic, dwlCount);
	}
	return TRUE;
}

//---------------------------------------------------------
//     テンポを設定
//---------------------------------------------------------
void NmMusicSetTemp(NmMusic *pMusic, int nRelativeTempo)
{
	pMusic->m_nRelativeTemp = nRelativeTempo;
	if(NmMusicIsPlay(pMusic))
		NmMusicInsideSetTimer(pMusic);
}

//---------------------------------------------------------
//     MIDIデバイスの初期化
//---------------------------------------------------------
BOOL NmMusicInsideOpenDevice(NmMusic *pMusic)
{
	BOOL bFlag = pMusic->m_MDevice.Open(pMusic->m_uDevice);
	NmMusicInsideSetNoteOff(pMusic);
	return bFlag;
}


//---------------------------------------------------------
//     MIDI音源に設定を伝える
//---------------------------------------------------------
void NmMusicInsideComeback(NmMusic *pMusic)
{
	int i;
	SMidiCode midiData;
	pMusic->m_fRelativeVolume = (float)0;

	//プログラムナンバー復帰
	midiData.Mdf.bbData[2] = 0;
	midiData.Mdf.bbData[3] = 0;
	for(i=0;i<16;i++)
	{
		midiData.Mdf.bbData[0] = BYTE(0xc0 + i);
		midiData.Mdf.bbData[1] = pMusic->m_playInfo.Chn[i].bbPC;
		pMusic->m_MDevice.Out(midiData.Mdf.dwMsg);
	}
	//コントロールナンバー復帰
	midiData.Mdf.bbData[1] = 0;
	midiData.Mdf.bbData[3] = 0;
	for(i=0;i<16;i++)
	{
		midiData.Mdf.bbData[0] = BYTE(0xb0 + i);
		midiData.Mdf.bbData[2] = pMusic->m_playInfo.Chn[i].bbCC;
		pMusic->m_MDevice.Out(midiData.Mdf.dwMsg);
	}
	//パン復帰
	midiData.Mdf.bbData[1] = 0x0a;
	midiData.Mdf.bbData[3] = 0;
	for(i=0;i<16;i++)
	{
		midiData.Mdf.bbData[0] = BYTE(0xb0 + i);
		midiData.Mdf.bbData[2] = pMusic->m_playInfo.Chn[i].bbPan;
		pMusic->m_MDevice.Out(midiData.Mdf.dwMsg);
	}
	//リバーブ復帰
	midiData.Mdf.bbData[1] = 0x05b;
	midiData.Mdf.bbData[3] = 0;
	for(i=0;i<16;i++)
	{
		midiData.Mdf.bbData[0] = BYTE(0xb0 + i);
		midiData.Mdf.bbData[2] = pMusic->m_playInfo.Chn[i].bbRev;
		pMusic->m_MDevice.Out(midiData.Mdf.dwMsg);
	}
	//コーラス復帰
	midiData.Mdf.bbData[1] = 0x05d;
	midiData.Mdf.bbData[3] = 0;
	for(i=0;i<16;i++)
	{
		midiData.Mdf.bbData[0] = BYTE(0xb0 + i);
		midiData.Mdf.bbData[2] = pMusic->m_playInfo.Chn[i].bbCos;
		pMusic->m_MDevice.Out(midiData.Mdf.dwMsg);
	}
	//ボリューム復帰
	NmMusicInsideSetVolume(pMusic);
}

//---------------------------------------------------------
//     ボリューム設定
//---------------------------------------------------------
void NmMusicInsideSetVolume(NmMusic *pMusic, int nSVolume)
{
	int i;
	SMidiCode midiData;
	if(pMusic->m_bThrough)
		return;
	midiData.Mdf.bbData[1] = (BYTE)0x07;
	midiData.Mdf.bbData[3] = 0;
	for(i=0;i<16;i++)
	{
		midiData.Mdf.bbData[0] = (BYTE)(0xb0 + i);
		int nVolume = pMusic->m_playInfo.Chn[i].bbVolume+(int)pMusic->m_fRelativeVolume+nSVolume;
		if(nVolume < 0)
			nVolume = 0;
		else if(nVolume > 127)
			nVolume = 127;
		midiData.Mdf.bbData[2] = (BYTE)nVolume;
		pMusic->m_MDevice.Out(midiData.Mdf.dwMsg);
	}
}

//---------------------------------------------------------
//     ボリューム設定
//---------------------------------------------------------
void NmMusicInsideSetNoteOff(NmMusic *pMusic)
{
	int i;
	SMidiCode midiData;
	if(pMusic->m_bThrough)
		return;
	midiData.Mdf.bbData[1] = (BYTE)0x78;
	midiData.Mdf.bbData[2] = 0;
	midiData.Mdf.bbData[3] = 0;
	for(i=0;i<16;i++)
	{
		midiData.Mdf.bbData[0] = (BYTE)(0xb0 + i);
		pMusic->m_MDevice.Out(midiData.Mdf.dwMsg);
	}
}


//---------------------------------------------------------
//     タイマー設定
//---------------------------------------------------------
BOOL NmMusicInsideSetTimer(NmMusic *pMusic)
{
	if(pMusic->m_bStopFlag)
		return FALSE;

	pMusic->m_nTimeCycle = 1;
	pMusic->m_wTemp = (WORD)(60000000 / pMusic->m_dwTempParam);
	if(pMusic->m_nRelativeTemp)
		pMusic->m_nWaitCycle = (NmMusicGetRCount(pMusic)*(pMusic->m_wTemp+pMusic->m_nRelativeTemp))/60000;
	else
		pMusic->m_nWaitCycle = NmMusicGetRCount(pMusic)*1000;
	pMusic->m_timePlay = 0;
	pMusic->m_dwTempCount = pMusic->m_nWaitCycle/pMusic->m_dwTempParam;
	pMusic->m_dwTempChengeCount = 0;
	if(pMusic->m_bThrough)
		return TRUE;
	pMusic->m_MTimer.Stop();
	return pMusic->m_MTimer.Start(pMusic->m_nTimeCycle, NmMusictimerCallBackMCB,(DWORD)(LPVOID)pMusic);
}

//---------------------------------------------------------
//     タイマー呼び出し部分
//---------------------------------------------------------
DWORD WINAPI NmMusicthreadCallBack( LPVOID pParam)
{
	NmMusic* pObject = (NmMusic*)pParam;
	pObject->m_bPlayOut = TRUE;
	while(!pObject->m_bStopFlag)
	{
		NmMusicInsidePlay(pObject);
		pObject->m_Elapse.Sleep(1);
	}
	pObject->m_bPlayOut = FALSE;
	return 0;
}

void CALLBACK NmMusictimerCallBackMCB(UINT uID,UINT uMsg,DWORD dwUser, DWORD dw1, DWORD dw2)
{
	NmMusic* pObject = (NmMusic*)dwUser;
	if(pObject->m_bStopFlag)
		::timeKillEvent(uID);
	else if(NmMusicGetTimerID(pObject) == uID)
	{
		NmMusicIncPlayCount( pObject );
		if(!pObject->m_bPlayOut)
		{
			pObject->m_bPlayOut = TRUE;
			NmMusicInsidePlay(pObject);
			pObject->m_bPlayOut = FALSE;
		}
	}
}

//---------------------------------------------------------
//     演奏出力一歩手前
//---------------------------------------------------------
void NmMusicInsidePlay(NmMusic *pMusic)
{	

	//フェードアウト
	if(pMusic->m_bFadeOut)
	{
		if(pMusic->m_nFadeCountWork < pMusic->m_nFadeCount)
		{
			pMusic->m_fRelativeVolume -= (float)90.0 / pMusic->m_nFadeCount;
			if((int)pMusic->m_fRelativeVolume != pMusic->m_nRelativeVolumeWork)
				NmMusicInsideSetVolume(pMusic);
			pMusic->m_nRelativeVolumeWork = (int)pMusic->m_fRelativeVolume;
			pMusic->m_nFadeCountWork++;
		}
		else
		{
			pMusic->m_fRelativeVolume = (float)0;
			pMusic->m_bFadeOut = FALSE;
			pMusic->m_bPlayOut = FALSE;
			NmMusicStop(pMusic);
			return;
		}
	}
	// フェードイン
	if(pMusic->m_bFadeIn)
	{
		if(pMusic->m_nFadeCountWork < pMusic->m_nFadeCount)
		{
			pMusic->m_nFadeCountWork++;
			pMusic->m_fRelativeVolume = float(pMusic->m_nFadeVolume-pMusic->m_nFadeVolume*pMusic->m_nFadeCountWork / pMusic->m_nFadeCount);
			if((int)pMusic->m_fRelativeVolume != pMusic->m_nRelativeVolumeWork)
				NmMusicInsideSetVolume(pMusic);
			pMusic->m_nRelativeVolumeWork = (int)pMusic->m_fRelativeVolume;
		}
		else
		{
			pMusic->m_fRelativeVolume = (float)0;
			pMusic->m_bFadeIn = FALSE;
			NmMusicInsideSetVolume(pMusic);
		}
	}
	if(pMusic->m_MidiData.IsPlay())
	{
		int i;
		pMusic->m_dwNowTime++;
		if(pMusic->m_nRelativeTemp)
			pMusic->m_dwTempCount = (DWORD)((DWORDLONG)(pMusic->m_timePlay*NmMusicGetRCount(pMusic)*(pMusic->m_wTemp+pMusic->m_nRelativeTemp))/60000);
		else
			pMusic->m_dwTempCount = (DWORD)((DWORDLONG)pMusic->m_timePlay*pMusic->m_nWaitCycle/pMusic->m_dwTempParam);
		for(i=0;i<10 && pMusic->m_dwTempChengeCount<pMusic->m_dwTempCount;i++,pMusic->m_dwTempChengeCount++)
			NmMusicInsideMidiOut(pMusic);
	}
	else
	{	
		if(pMusic->m_bLoop)
		{
			pMusic->m_MTimer.Stop();
			pMusic->m_MidiData.Init();
			pMusic->m_dwNowCount=0;
	
			if(pMusic->m_dwlLoopSearch)
			{
				pMusic->m_bThrough = TRUE;
				while(pMusic->m_MidiData.IsPlay() && pMusic->m_MidiData.GetNowCount() < pMusic->m_dwlLoopSearch)
				{
					pMusic->m_dwNowCount = 0;
					NmMusicInsideMidiOut(pMusic);
				}
				pMusic->m_bThrough = FALSE;
			}
			NmMusicCont(pMusic);
		}
		else
		{
			pMusic->m_bPlayOut = FALSE;
			NmMusicStop(pMusic);
		}
	}
}


//---------------------------------------------------------
//     実演奏処理
//---------------------------------------------------------
void NmMusicInsideMidiOut(NmMusic *pMusic)
{
	int nWork;
	BYTE bbStat,bbStat2,bbStat3;
	UMidiData MData;
	while(pMusic->m_dwNowCount==0 && pMusic->m_MidiData.IsPlay())
	{
		MData.dwMsg = pMusic->m_MidiData.GetDWord();
		bbStat = MData.bbData[0];					//ステータスバイト（ｂｙ浅田）
		bbStat2 = MData.bbData[0] &  (BYTE)0xf0;	//ステータスバイトの上位4ビット（ノートオンなどを表す）
		bbStat3 = MData.bbData[0] &  (BYTE)0x0f;	//ステータスバイトの下位4ビット。チャンネルを表す
		if(bbStat == 0xf0)	//エクスクルーシブ
		{
			pMusic->m_MDevice.Out(pMusic->m_MidiData.GetEData());
		}
		else if(bbStat == 0xff)	//テンポ
		{
			((LPWORD)&pMusic->m_dwTempParam)[0] = pMusic->m_MidiData.GetDataWord();
			pMusic->m_MidiData.Next();
			((LPWORD)&pMusic->m_dwTempParam)[1] = pMusic->m_MidiData.GetDataWord();
			NmMusicInsideSetTimer(pMusic);
		}
		else				//通常データ
		{
			switch(bbStat2)	//ステータスの判定
			{
			case 0x90:						//ノートオン（ex鍵盤を押す）
				if(MData.bbData[2])			//ベロシティ（どのくらいの強さ）0(偽)のときノートオフと同じ
				{
					if( pMusic->m_cKey!=0 ){
						MData.bbData[1] += pMusic->m_cKey;		//MData.bbData[1]はノートナンバ（どの鍵盤）0x00〜0x7f
						pMusic->m_playInfo.Chn[bbStat3].bbNote[MData.bbData[1]>>3] |=	
							(BYTE)(1<<(MData.bbData[1]&0x07));
						// ↑の説明
						// >>3により鍵盤を8個ずつのグループに分ける。（全部で16グループできる）16はbbData[]の要素数
						// 何番目のグループかは添え字で表現する
						// １つの要素は1バイト＝8ビット。各ビットが鍵盤のキーを表す
						break;
					}else{
						MData.bbData[1] += pMusic->m_cKey;		//MData.bbData[1]はノートナンバ（どの鍵盤）0x00〜0x7f
						pMusic->m_playInfo.Chn[bbStat3].bbNote[MData.bbData[1]>>3] |=
							(BYTE)(1<<(MData.bbData[1]&0x07));
						break;
					}
				}
			case 0x80:						//ノートオフ
				if( pMusic->m_cKey!=0 ){
					MData.bbData[1] += pMusic->m_cKey;
					pMusic->m_playInfo.Chn[bbStat3].bbNote[MData.bbData[1]>>3] &=
							(BYTE)~(1<<(MData.bbData[1]&7));
					break;
				}else{
					MData.bbData[1] += pMusic->m_cKey;
					pMusic->m_playInfo.Chn[bbStat3].bbNote[MData.bbData[1]>>3] &=
							(BYTE)~(1<<(MData.bbData[1]&7));
					break;
				}
			case 0xc0:						//プログラムチェンジ
				pMusic->m_playInfo.Chn[bbStat3].bbPC = MData.bbData[1];
				break;
			case 0xb0:
				switch(MData.bbData[1])
				{
				case 0x00:					//コントロールチェンジ
					pMusic->m_playInfo.Chn[bbStat3].bbCC = MData.bbData[2];
					break;
				case 0x07:					//ボリューム
					pMusic->m_playInfo.Chn[bbStat3].bbVolume = MData.bbData[2];
                    nWork = MData.bbData[2] + pMusic->m_nRelativeVolume + (int)pMusic->m_fRelativeVolume;
					if(nWork < (BYTE)0)
						nWork = 0;
					else if(nWork > 127)
						nWork = 127;
                    pMusic->m_playInfo.Chn[bbStat3].bbVolume2 = (BYTE)nWork;
					MData.bbData[2] = (BYTE)nWork;
					break;
				case 0x5b:					//リバーブ
					pMusic->m_playInfo.Chn[bbStat3].bbRev = MData.bbData[2];
					break;
				case 0x5d:					//コーラス
					pMusic->m_playInfo.Chn[bbStat3].bbCos = MData.bbData[2];
					break;
				case 0x0a:					//パン
					pMusic->m_playInfo.Chn[bbStat3].bbPan = MData.bbData[2];
					break;
				}
			}
			if(!pMusic->m_bThrough)
				pMusic->m_MDevice.Out(MData.dwMsg);
		}
		pMusic->m_MidiData.Next();
		pMusic->m_dwNowCount = pMusic->m_MidiData.GetNextCount();
	}
	pMusic->m_dwNowCount--;
}



void NmMusicSetThreadMode(NmMusic *pMusic, BOOL bFlag){pMusic->m_bThread = bFlag;}
BOOL NmMusicIsData(NmMusic *pMusic){return pMusic->m_MidiData.IsData();}
void NmMusicSetLoop(NmMusic *pMusic, BOOL bLoop){pMusic->m_bLoop=bLoop;}
void NmMusicSetLoopTop(NmMusic *pMusic, DWORDLONG dwlLoopSearch){pMusic->m_dwlLoopSearch = dwlLoopSearch;}

void NmMusicSetKey(NmMusic *pMusic, char cKey){
	int i;																	//by浅田
	for(i=0;i<16;i++)														//by浅田
		ZeroMemory(pMusic->m_playInfo.Chn[i].bbNote,sizeof(BYTE[16]));		//by浅田
	pMusic->m_cKey = cKey;
}
BOOL NmMusicIsPlay(NmMusic *pMusic){return pMusic->m_MTimer.IsTimer();}
WORD NmMusicGetTemp(NmMusic *pMusic){return pMusic->m_wTemp;}
WORD NmMusicGetRCount(NmMusic *pMusic){return pMusic->m_MidiData.GetRCount();}
DWORDLONG NmMusicGetAllCount(NmMusic *pMusic){return pMusic->m_MidiData.GetAllCount();}
DWORDLONG NmMusicGetPlayCount(NmMusic *pMusic){return pMusic->m_MidiData.GetNowCount();}
DWORDLONG NmMusicGetAllTime(NmMusic *pMusic){return pMusic->m_MidiData.GetAllTime();}
DWORDLONG NmMusicGetPlayTime(NmMusic *pMusic){return pMusic->m_dwNowTime;}
LPCSTR NmMusicGetTitle(NmMusic *pMusic){return pMusic->m_MidiData.GetTitle();}
SPlayInfo* NmMusicGetPlayInfo(NmMusic *pMusic){return &pMusic->m_playInfo;}

BOOL NmMusicOutMidiDevice(NmMusic *pMusic, DWORD dwData){return pMusic->m_MDevice.Out(dwData);}
int NmMusicGetDeviceCount(NmMusic *pMusic){return pMusic->m_MDevice.GetDeviceCount();}
void NmMusicGetDeviceName(NmMusic *pMusic, int nIndex,LPSTR pString){pMusic->m_MDevice.GetDeviceName(nIndex,pString);}

UINT NmMusicGetTimerID(NmMusic *pMusic){return (UINT)pMusic->m_MTimer.IsTimer();}
void NmMusicIncPlayCount(NmMusic *pMusic){pMusic->m_timePlay++;}
BOOL NmMusicIsPlayOut(NmMusic *pMusic){return pMusic->m_bPlayOut;}
BOOL NmMusicStdMidi(NmMusic *pMusic, LPBYTE pData){return pMusic->m_MidiData.Read(pData);	}













