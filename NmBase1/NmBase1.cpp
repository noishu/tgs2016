/********************************************************/
/* DirectX  ゲームプログラミング						*/
/*						ゲームライブラリー				*/
/*														*/
/* NmBase.cpp - ウィンドウ処理							*/
/*														*/
/*					沼津情報専門学校　Atsushi Ohba		*/
/*						Copyright (c) 2005-2005			*/
/*						All Rights Reserved.			*/
/********************************************************/
#include "NmBase1.h"
#include "GameMain.h"
#include "resource.h"

////////////////////////////////////////////////////////////////////////////
//                        グローバル変数の宣言
////////////////////////////////////////////////////////////////////////////
static HWND 	 g_hwnd;					// ウィンドウハンドル
static HINSTANCE g_Instance;				// インスタンスハンドル

static int 		 g_MousPosX;				// マウス座標保存用変数
static int 		 g_MousPosY;
static BOOL		g_MouseButtonR = FALSE;
static BOOL		g_MouseButtonL = FALSE;
static BOOL		g_OldMouseButtonR = FALSE;
static BOOL		g_OldMouseButtonL = FALSE;

static long		 g_FrameTime = 0;			// 処理にかかった時間を格納
static long		 g_PreTime;

////////////////////////////////////////////////////////////////////////////
//                     ファイル間インターフェース関数
////////////////////////////////////////////////////////////////////////////
long	GetSysFrmTime(void)  { return g_FrameTime; }
HWND	GetSysHwnd(void) 	 { return g_hwnd; }
int 	GetMouseX(void) { return g_MousPosX; }
int 	GetMouseY(void) { return g_MousPosY; }
BOOL	GetMouseButtonR()	{	return g_MouseButtonR;	}
BOOL	GetMouseButtonL()	{	return g_MouseButtonL;	}
BOOL	GetMouseButtonRWait()	{	return !g_OldMouseButtonR & g_MouseButtonR;	}
BOOL	GetMouseButtonLWait()	{	return !g_OldMouseButtonL & g_MouseButtonL;	}

////////////////////////////////////////////////////////////////////////////
//                          プロトタイプ宣言
////////////////////////////////////////////////////////////////////////////
static long FAR PASCAL WindowProc(HWND, UINT, WPARAM, LPARAM);
static BOOL SysCreateWindow(HINSTANCE hInstance, int nCmdShow);
static void SysMainLoop(void);

void  SysErrMsgDisp(char *msg);
char* SysDirGet(void);
void  SysKeyGet(unsigned int code,int* state);

	
//	ウインドウメイン関数
int PASCAL WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance, LPSTR lpCmdLine,int nCmdShow)
{
	// 二重起動防止処理
	if (FindWindow(WINDOW_NAME, NULL) != NULL) return FALSE;

	//======================================================================
	//						  ウインドウ生成処理
	//======================================================================
	if (SysCreateWindow(hInstance, nCmdShow) == FALSE) return FALSE;

	//======================================================================
	//							  初期化処理
	//======================================================================
	// ダイレクトグラフィックス初期化処理
	if (GrpInit(g_hwnd) == false) {
		SysErrMsgDisp("ダイレクトグラフィックスの初期化に失敗しました");
		goto ERR_ESC;
	}

	// マウスカーソル非表示
#if !WINDOW_MODE
	ShowCursor(FALSE);
#endif
	// 乱数系列初期化
	srand(timeGetTime());
	
	// タイマーの精度を１ｍｓに変更
	timeBeginPeriod(1);

	//	メインループ
	SysMainLoop();

	//  終了処理
ERR_ESC:
	GrpRelease();							// ダイレクトグラフィック開放処理
	ShowCursor(TRUE);						// マウスカーソル表示
	timeEndPeriod(1);						// タイマーの精度を復帰

	return TRUE;
}


//	メッセージ処理関数
long FAR PASCAL WindowProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	switch (msg)
	{
		// マウス座標取得 --------------------------------------------------
		case WM_MOUSEMOVE:
			g_MousPosX = LOWORD(lParam);
			g_MousPosY = HIWORD(lParam);
			break;
		//マウスボタンが押された
		case WM_LBUTTONDOWN:
			g_MouseButtonL = TRUE;
			break;
		case WM_RBUTTONDOWN:
			g_MouseButtonR = TRUE;
			break;
		case WM_LBUTTONUP:
			g_MouseButtonL = FALSE;
			break;
		case WM_RBUTTONUP:
			g_MouseButtonR = FALSE;
			break;
		// 何かキーが押された場合 ------------------------------------------
		case WM_KEYDOWN:
			switch (wParam) {
			//ＥＳＣが押された時のイベント
			case VK_ESCAPE:
				//プログラム終了
				PostMessage(hwnd, WM_CLOSE, 0, 0);
				break;
			}
			break;
		// ウィンドウが閉じられる場合 --------------------------------------
		case WM_CLOSE:
			DestroyWindow(hwnd);
			return 0L;
		// ウィンドウが破棄される場合 --------------------------------------
		case WM_DESTROY:
			PostQuitMessage(0); // 終了
			break;
	}
	
	return DefWindowProc(hwnd,msg,wParam,lParam);
}
//これをやることで、マウスが押された瞬間とかをとれるようになるのかもね。
void updateMouseState(){
	g_OldMouseButtonL = g_MouseButtonL;
	g_OldMouseButtonR = g_MouseButtonR;
}

//	ウインドウ生成関数
BOOL SysCreateWindow(HINSTANCE hInstance, int nCmdShow)
{
	//======================================================================
	//						 ウインドウクラスの作成
	//======================================================================
	WNDCLASS    wc;	
	wc.style		 = CS_CLASSDC;
	wc.lpfnWndProc   = WindowProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = hInstance;
	wc.hIcon         = LoadIcon( hInstance, MAKEINTRESOURCE(IDI_NMBASE) );
	wc.hCursor       = LoadCursor(NULL,IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName  = "Sample";
	wc.lpszClassName = WINDOW_NAME;
	RegisterClass(&wc);
	
	//======================================================================
	//						   メインウィンドウ作成
	//======================================================================
#if WINDOW_MODE	
	RECT rc;
	SetRect(&rc,0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
	DWORD      dwStyle = WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION;
	AdjustWindowRect(&rc,dwStyle,FALSE);
	g_hwnd = CreateWindowEx(0, WINDOW_NAME, APP_NAME,dwStyle,
						CW_USEDEFAULT, CW_USEDEFAULT, 
						rc.right - rc.left, rc.bottom - rc.top,
						NULL, NULL, hInstance, NULL);

#else
	g_hwnd = CreateWindowEx(0, WINDOW_NAME, APP_NAME,WS_POPUP|WS_SYSMENU,0,0,
						GetSystemMetrics(SM_CXSCREEN),
						GetSystemMetrics(SM_CYSCREEN),
						NULL,NULL,hInstance,NULL);	
#endif	

	// ウインドウ生成に失敗した時プログラム終了
	if (!g_hwnd) return FALSE;
	
	// インスタンスハンドルをグローバル変数にコピー
	g_Instance = hInstance;

	// ウインドウ作成
	ShowWindow(g_hwnd, nCmdShow);
	UpdateWindow(g_hwnd);
	
	// ウインドウ作成成功
	return TRUE;
}


//	ゲームループ処理数
void SysMainLoop(void)
{
	MSG msg;
	
	
	// ゲームループ前初期化
	if( !InitGame() ) return;

	// フレームタイムの初期化
	g_PreTime = timeGetTime();

	while (1)
	{
		//==================================================================
		// メッセージを処理する場合
		//==================================================================
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {	
			if (msg.message == WM_QUIT) break;	// アプリケーション終了時

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} else	{
		//==================================================================
		// ゲームメイン処理
		//==================================================================
			// フレームタイムの更新処理
			g_FrameTime = (timeGetTime() - g_PreTime);
			g_PreTime = timeGetTime();
			
			FrameMove(g_FrameTime);
			updateMouseState();
			// バックバッファを初期化
			GetD3DDevice()->Clear(0,NULL,D3DCLEAR_TARGET| D3DCLEAR_ZBUFFER,D3DCOLOR_XRGB(0,0,0),1.0,0);
			
			// ゲームメイン処理
			GetD3DDevice()->BeginScene();
			RenderScene();
			GetD3DDevice()->EndScene();

			// バックバッファ転送
			GetD3DDevice()->Present(NULL, NULL, NULL, NULL);
		}

		// 他のアプリケーションのための処理
		Sleep( 1 );
	}

	// ゲーム終了時の初期化
	GameRelease();
}

//	エラーメッセージ表示
void SysErrMsgDisp(char *msg)
{
	MessageBox(g_hwnd, msg, "", MB_OK);
}


