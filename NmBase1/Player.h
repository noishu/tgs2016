#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "NmBase1.h"
#include "Unit.h"


#define SummonMax 200
#define MpStart 10000
#define HpStart 5000
#define Mpadd 1
#define MonsterMax 20
#define NO_MONSTER 0


typedef struct _PLAYER_DATA{
	int PlayerHp;	//プレイヤーのHP
	int PlayerMp;	//プレイヤーのMP
	int PlayerCnt;	//プレイヤーのユニット召喚数
	Nm2DSurface PlayerBaceSurf;
}player;


//プレイヤーの初期化
void playerInit();
//プレイヤーの更新
void PlayerUpdata(int time);
//プレイヤーの表示（下地）
void PlayerRenderUnder();
//プレイヤーの表示（上枠）
void PlayerRenderUp();
//プレイヤーの消去
void PlayerDelete();
//プレイヤーの召喚
void PlayerSumon();
//プレイヤーのhp管理
int Playerhp(int y);
//モンスターの数をチェック
int PlayerCheckMax(int unit);

#endif