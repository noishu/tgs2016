#include "NmBase1.h"
#include "GameMain.h"
#include "HenseiScene.h"
#include "GouseiScene.h"
#include "menber.h"
#include "Unit.h"

//メニュー
#define MENU_MAX 7
extern SCENE_NO Hmenu[7] = { SCENE_MENU,SCENE_GAME1,SCENE_GAME2,SCENE_YUNIT };

//編成用枠背景と編成用枠
Nm2DSurface HBmenuSurf;
Nm2DSurfaceArray HWackSurf;
//背景画面
Nm2DSurface HBackSurf;
Nm2DSurface HColrsolSurf;
Nm2DSurface HRenternSurf;
Nm2DSurface HTWakuSurf;
//モンスター画像
Nm2DSurfaceArray HSlimeSurf;
Nm2DSurface HGoblinSurf;
Nm2DSurface HGolemSurf;
Nm2DSurface HDoragnSurf;
//Nm2DSurface HMonsterSurf;
//ツール
Nm2DSurface HToolSurf;
Nm2DSurface HwindouSurf;//枠
int tool_x, tool_y, tool_w,tool_h;
int HColrsol_Gap_X, HColrsol_Gap_Y;
//選択中のブロックの位置
int MapTipx, MapTipy;
Nm2DSurface NowMonsterSurf;
//セーブ確認フラグ
int SaveFlag;
int SaveTime;
//使用中モンスター確認フラグ
int MenbreFlag;
//編集中のチップ
int TipIndex = 0;
int newmon;
extern int giantF;
extern int MenberData[SCREEN_H][SCREEN_W];
int Htool_gap;
extern int monF[4][9];
//デバック用フォント
NmFont debugFont;
//曲の設定
static NmMusic MainMusic;
//初期化
BOOL initHenseiScene(void){
	newmon = 800;
	//データ読み込み
	MenberInit();
	menberLoad("TimeData.dat");
	//背景の初期化
	Nm2DInit(&HBackSurf);
	Nm2DOpen(&HBackSurf, "res/UnitBack.png", 1280, 720);
	//編成用枠背景と編成用枠の初期化
	Nm2DInit(&HBmenuSurf);
	Nm2DOpen(&HBmenuSurf, "res/Bmenu.png", 1280, 200,1,1,D3DCOLOR_RGBA(0,0,0,0));
	Nm2DArrayInit(&HWackSurf);
	Nm2DArrayOpen(&HWackSurf, "res/waku.png", 160, 130, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&HTWakuSurf);
	Nm2DOpen(&HTWakuSurf, "res/topwaku.png", 1280, 140, D3DCOLOR_RGBA(255, 255, 255, 255));
	//モンスター画像の初期化
	Nm2DArrayInit(&HSlimeSurf);
	Nm2DArrayOpen(&HSlimeSurf, "res/MIcon.png", 160, 160,9,4, D3DCOLOR_RGBA(0, 255, 0, 0));

	Nm2DInit(&NowMonsterSurf);
	Nm2DOpen(&NowMonsterSurf, "res/MIcon.png", 160, 160,8,4,D3DCOLOR_RGBA(0, 255, 0, 0));
	//ツール枠の初期化
	Nm2DInit(&HwindouSurf);
	Nm2DCreateSurface(&HwindouSurf, 1280, 380, D3DCOLOR_RGBA(255, 255, 255, 128));
	Nm2DInit(&HRenternSurf);
	Nm2DOpen(&HRenternSurf, "res/modoru2.png", 150, 120);
	//カーソルの初期化
	Nm2DInit(&HColrsolSurf);
	Nm2DOpen(&HColrsolSurf, "res/crasol.png", 50, 50);
	//変数の初期化
	tool_w = 1280;
	tool_h = 720;
	tool_x = 0;
	tool_y = 140;
	Htool_gap = 0;
	HColrsol_Gap_X = 80;
	HColrsol_Gap_Y = 80;
	MapTipx = 0;
	MapTipy = 0;//セーブ用フラグ
	SaveFlag = 0;
	SaveTime = 0;
	//使用中確認フラグ
	MenbreFlag = 0;
	//デバックフォント
	NmFontInit(&debugFont); 
	NmFontCreate(&debugFont, 48, "");
	
	
	//最初にロードする仕組み
	menberLoad("res/TimeDat.dat");
	getUnitLoad("res/UnitGetNow.dat");
	//曲の初期化
	NmMusicInit(&MainMusic);
	NmMusicOpen(&MainMusic, "res/MOUSIC/MEIN/game_maoudamashii_7_event34.mid", 0);
	NmMusicPlay(&MainMusic);
	return TRUE;
}
//フレーム処理
void moveHenseiScene(DWORD dwTickDiff){

	//ひとつ前の画面に戻る
	if (GetMouseButtonLWait()){
		if (GetMouseX() > 1050 && GetMouseX()<1050 + 150 &&
			GetMouseY()>10 && GetMouseY() < 10 + 120){
			//	SaveFlag = 1;
			//	SaveTime = 60;
			menberSave("TimeData.dat");
			changeScene(Hmenu[SCENE_MENU]);
		}
	}
	//ユニットの選択
	if (GetMouseButtonRWait()){
		if (tool_x < GetMouseX() && GetMouseX() < tool_x + tool_w &&
			tool_y < GetMouseY() && GetMouseY() < tool_y + tool_h){
			TipIndex = (GetMouseX() - tool_x) / TIP_SIZE2 + (GetMouseY() - Htool_gap - tool_y) / TIP_SIZE2 * 8;
			if (monF[(GetMouseY() - Htool_gap - tool_y) / TIP_SIZE2][(GetMouseX() - tool_x) / TIP_SIZE2] == 0){
				TipIndex = 0;
			}
		}
		
	}
	//もう一度押してマップデータに入れる
	if (GetMouseButtonLWait()
		&& GetMouseX()>200 && GetMouseX()<200 + 1080 &&
		GetMouseY()>555 && GetMouseY() < 555 + 130){
		if (TipIndex == skeletonGeneral){
			TipIndex = skeletonGeneral2;
		}
		MenbreSet(GetMouseX(), GetMouseY(), TipIndex);
		

		//モンスター編成に置いたので、今持っているモンスターを消去（Index0）にする
		TipIndex = 0;
	}
	if (GetMouseY() >= 480 && 540 >= GetMouseY()){
		Htool_gap-=6;
	}
	else if (GetMouseY() <= 140&&Htool_gap!=0){
		Htool_gap += 6;
	}
}
//レンダリング処理
void renderHenseiScene(void){
	int i;
	int j;
	Nm2DRender(&HBackSurf);
	Nm2DSet(&HwindouSurf, 0, 140);
	Nm2DRender(&HwindouSurf);
	for (i = 0; i < 4; i++){
		for (j = 0; j < 9; j++){
			Nm2DArrayAddSurface(&HSlimeSurf, j*TIP_SIZE2, ((i + 1)*TIP_SIZE2));
			Nm2DArraySet(&HSlimeSurf, j + (i * 9), j*TIP_SIZE2, ((i + 1)*TIP_SIZE2 + Htool_gap));
			Nm2DArraySetIndex(&HSlimeSurf, j + (i * 9), j + (i * 9));
			if (monF[i][j] == 0){
				Nm2DArraySetIndex(&HSlimeSurf, j + (i * 9), 0);
			}
		}
	}
	Nm2DArrayRender(&HSlimeSurf);
	Nm2DSet(&HBmenuSurf, 0, 520);
	Nm2DRender(&HBmenuSurf);
	for (int i = 1, j = 3; i < SCREEN_W; i++){
		Nm2DArrayAddSurface(&HWackSurf, i*TIP_SIZE+30, j*TIP_SIZE -45);
	}
	MenberRender();
	Nm2DArrayRender(&HWackSurf);
	Nm2DSet(&NowMonsterSurf, GetMouseX()-HColrsol_Gap_X, GetMouseY()-HColrsol_Gap_Y);
	Nm2DSetIndex(&NowMonsterSurf, TipIndex);
	Nm2DRender(&NowMonsterSurf);
	Nm2DRender(&HTWakuSurf);
	Nm2DSet(&HRenternSurf, 1050, 10);
	Nm2DRender(&HRenternSurf);
	Nm2DSet(&HColrsolSurf, GetMouseX(), GetMouseY());
	Nm2DRender(&HColrsolSurf);
	/*for (int i=0; i <= 6; i++){
		for (int j=0; j <= 4; j++){
			char buf[256];
			sprintf(buf, "%d", MenberData[j][i]);
			NmFontDrawText(
				&debugFont, buf, i*100, j*100,
				D3DCOLOR_RGBA(255, 255, 255, 255));
		}
	}
	*/
	//デバック用に、マップの位置を表示する
	//for (int i = 0; i < SCREEN_W + 1; i++){
	//	char buf[256];
	//	sprintf(buf, "%03d", i);
	//	NmFontDrawText(&debugFont, buf, i*TIP_SIZE, 0, D3DCOLOR_RGBA(255, 255, 255, 255));
	//}
	//for (int j = 0; j < SCREEN_H + 1; j++){
	//	char buf[256];
//		sprintf(buf, "%03d", j);
	//	NmFontDrawText(&debugFont, buf, 0, j*(TIP_SIZE + 30), D3DCOLOR_RGBA(255, 255, 255, 255));
	//}
}
//シーン終了後処理
void releaseHenseiScene(void){
	Nm2DDeleteSurface(&HBackSurf);
	Nm2DDeleteSurface(&HBmenuSurf);
	Nm2DArrayDeleteSurface(&HWackSurf);
	Nm2DDeleteSurface(&HRenternSurf);
	Nm2DDeleteSurface(&HTWakuSurf);
	Nm2DArrayDeleteSurface(&HSlimeSurf);	
	Nm2DDeleteSurface(&NowMonsterSurf);
	Nm2DDeleteSurface(&HColrsolSurf);
	NmMusicDelete(&MainMusic);
}
//あたり判定
void HenseiSceneCollideCallback(int nSrc, int nTarget, int nCollideID){
	//if (GetMouseButtonLWait()){
	//	if (GetMouseX() > 1050 && GetMouseX()<1050 + 150 &&
	//		GetMouseY()>20 && GetMouseY() < 20 + 120){
	//		changeScene(Hmenu[SCENE_YUNIT]);
	//	}
	//}
}