#include "GameMain.h"
//２全てのシーンのヘッダファイルをインクルードする
#include "Game1Scene.h"
#include "Game2Scene.h"
#include "Game3Scene.h"
#include "MenuScene.h"
#include "unitScene.h"
#include "HenseiScene.h"
#include "Kurezit.h"
#include "GouseiScene.h"

//このプログラム全体で使用する変数の宣言
// インプット変数の宣言
NmInput input;

//このファイル内だけで使用する関数のプロトタイプ宣言
//現在のシーンの初期化処理
void initCurrentScene( void );			
//現在のシーンのフレーム処理
void moveCurrentScene( DWORD dwTickDiff );
//現在のシーンのレンダリング処理
void renderCurrentScene( void );
//現在のシーンの削除処理
void releaseCurrentScene( void );

//このファイル内だけで使用する変数の宣言（staticをつけて宣言する）
static SCENE_NO sceneNo = SCENE_NONE;	// 現在のシーン番号（必ず初期値はSCENE_NONE）
static SCENE_NO prevScene = SCENE_NONE;	// 1つ前のシーン番号（必ず初期値はSCENE_NONE）
static SCENE_NO nextScene = SCENE_NONE;	// 次のシーン番号（必ず初期値はSCENE_NONE）

//３ゲーム開始前の初期化を行う
BOOL InitGame(void){
	// 全てのシーンで共有するモノを初期化する
	// インプット変数の初期化
	NmInputCreate( &input, 0, 0);

	//３(1) 初めのシーン番号の設定
	changeScene(SCENE_MENU);
	return TRUE;
}

//フレーム処理
void FrameMove(DWORD dwTickDiff){
	// 全てのシーンで共用する処理
	NmInputGet( &input );

	// 次のシーンに変更するかどうか判断する
	if( sceneNo != nextScene ){
		//現在のシーンの削除処理
		releaseCurrentScene();
		//現在のシーンを新規シーンに変更する
		sceneNo = nextScene;
		//新しいシーンの初期化処理
		initCurrentScene();
	}

	//現在のシーンのフレーム処理
	moveCurrentScene(dwTickDiff);
}

//レンダリング処理
void RenderScene(){
	//現在のシーンのレンダリング処理
	renderCurrentScene();
}

//ゲーム終了時の後処理
void GameRelease(void){
	//現在のシーンの削除処理
	releaseCurrentScene();
	// 全てのシーンで共有するモノの削除処理をする

}

//３(2) 当り判定コールバック 　　　ここでは要素を削除しないこと！！
void  CollideCallback( int nSrc, int nTarget, int nCollideID ){
	switch( sceneNo ){
		case SCENE_NONE:	break;
		case SCENE_MENU:	MenuSceneCollideCallback( nSrc, nTarget, nCollideID );		break;
		case SCENE_GAME1:	Game1SceneCollideCallback( nSrc, nTarget, nCollideID );		break;
		case SCENE_GAME2:	Game2SceneCollideCallback( nSrc, nTarget, nCollideID );		break;
		case SCENE_GAME3:	Game3SceneCollideCallback( nSrc, nTarget, nCollideID );		break;
		case SCENE_YUNIT:   UnitSceneCollideCallback(nSrc, nTarget, nCollideID);		break;
		case SCENE_HENSEI:  HenseiSceneCollideCallback(nSrc, nTarget, nCollideID);      break;
		case SCENE_GOUSEI:	GouseiSceneCollideCallback(nSrc, nTarget, nCollideID);		break;
		case SCENE_SONTA:   KurezitCollideCallback(nSrc, nTarget, nCollideID);			break;
		case SCENE_MAX:		break;
		default:
			MessageBox( NULL, "まだそのシーンはない", "作成途中", 0 );
			changeScene( prevScene );
	}
}

//シーンを変更する関数
void changeScene( SCENE_NO no ){
	// 現在のシーンと同じときは何もしない
	if( sceneNo == no )return;
	// 正しくないシーン番号の時は無視する
	if( no >= SCENE_MAX ) return;
	if( no <= SCENE_NONE ) return;
	// シーンを変更する
	prevScene = sceneNo;
	nextScene = no;
}

//３(3) 現在のシーンの初期化処理
void initCurrentScene( void ){
	switch( sceneNo ){
		case SCENE_NONE:	break;
		case SCENE_MENU:	initMenuScene();	break;
		case SCENE_GAME1:	initGame1Scene();	break;
		case SCENE_GAME2:	initGame2Scene();	break;
		case SCENE_GAME3:	initGame3Scene();	break;
		case SCENE_YUNIT:	initUnitScene();	break;
		case SCENE_HENSEI:  initHenseiScene();  break;
		case SCENE_GOUSEI:	initGouseiScene();  break;
		case SCENE_SONTA:	initKurezit();		break;
		case SCENE_MAX:		break;
		default:
			MessageBox( NULL, "まだそのシーンはない", "作成途中", 0 );
			changeScene( prevScene );
	}
}
//３(4) 現在のシーンのフレーム処理
void moveCurrentScene( DWORD dwTickDiff ){
	switch( sceneNo ){
		case SCENE_NONE:	break;
		case SCENE_MENU:	moveMenuScene(dwTickDiff);		break;
		case SCENE_GAME1:	moveGame1Scene(dwTickDiff);		break;
		case SCENE_GAME2:	moveGame2Scene(dwTickDiff);		break;
		case SCENE_GAME3:	moveGame3Scene(dwTickDiff);		break; 
		case SCENE_YUNIT:	moveUnitScene(dwTickDiff);		break;
		case SCENE_HENSEI:  moveHenseiScene(dwTickDiff);    break;
		case SCENE_GOUSEI:  moveGouseiScene(dwTickDiff);	break;
		case SCENE_SONTA:	moveKurezit(dwTickDiff);		break;
		case SCENE_MAX:		break;
		default:
			MessageBox( NULL, "まだそのシーンはない", "作成途中", 0 );
			changeScene( prevScene );
	}
}
//３(5) 現在のシーンのレンダリング処理
void renderCurrentScene( void ){
	switch( sceneNo ){
		case SCENE_NONE:	break;
		case SCENE_MENU:	renderMenuScene();		break;
		case SCENE_GAME1:	renderGame1Scene();		break;
		case SCENE_GAME2:	renderGame2Scene();		break;
		case SCENE_GAME3:	renderGame3Scene();		break;
		case SCENE_YUNIT:	renderUnitScene();		break;
		case SCENE_HENSEI:  renderHenseiScene();    break;
		case SCENE_GOUSEI:  renderGouseiScene();	break;
		case SCENE_SONTA:	renderKurezit();		break;
		case SCENE_MAX:		break;
		default:
			MessageBox( NULL, "まだそのシーンはない", "作成途中", 0 );
			changeScene( prevScene );
	}
}
//３(6) 現在のシーンの削除処理
void releaseCurrentScene( void ){
	switch( sceneNo ){
		case SCENE_NONE:	break;
		case SCENE_MENU:	releaseMenuScene();		break;
		case SCENE_GAME1:	releaseGame1Scene();	break;
		case SCENE_GAME2:	releaseGame2Scene();	break;
		case SCENE_GAME3:	releaseGame3Scene();	break;
		case SCENE_YUNIT:	releaseUnitScene();		break;
		case SCENE_HENSEI:  releaseHenseiScene();   break;
		case SCENE_GOUSEI:	releaseGouseiScene();	break;
		case SCENE_SONTA:	releaseKurezit();		break;
		case SCENE_MAX:		break;
		default:
			MessageBox( NULL, "まだそのシーンはない", "作成途中", 0 );
			changeScene( prevScene );
	}
}
