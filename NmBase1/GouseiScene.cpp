#include "NmBase1.h"
#include "GameMain.h"
#include "GouseiScene.h"
#include "menber.h"
#include "Unit.h"

//メニュー
#define MENU_MAX 7
extern SCENE_NO Gmenu[7] = { SCENE_MENU, SCENE_GAME1, SCENE_GAME2, SCENE_YUNIT };

//背景画面
Nm2DSurface GBackSurf;
Nm2DSurface GColrsolSurf;
Nm2DSurface GRenternSurf;
Nm2DSurface GBmenuSurf;
Nm2DSurface GTWakuSurf;
//モンスター画像
Nm2DSurfaceArray GSlimeSurf;
Nm2DSurface GGoblinSurf;
Nm2DSurface GGolemSurf;
Nm2DSurface GDoragnSurf;
Nm2DSurface Waku1Surf;
Nm2DSurface Waku2Surf;
Nm2DSurface GMonstersFaceSurf1;
Nm2DSurface GMonstersFaceSurf2;
Nm2DSurface Gobulin_swords;
Nm2DSurface Gobulin_Sorcerer;
Nm2DSurface Giant;
Nm2DSurface gWindowSurf;
Nm2DSurfaceArray NoMonSurf;
Nm2DSurface mobSel1;
Nm2DSurface mobSel2;
extern Nm2DSurface goblinMSurf;
//Nm2DSurface HMonsterSurf;
//ツール
Nm2DSurface GToolSurf;
Nm2DSurface GwindouSurf;//枠
int g_tool_x, g_tool_y, g_tool_w, g_tool_h;
int GColrsol_Gap_X, GColrsol_Gap_Y;
//選択中のブロックの位置
int GMapTipx, GMapTipy;
Nm2DSurface G_NowMonsterSurf;
//使用中モンスター確認フラグ
int G_MenbreFlag;
//編集中のチップ
int G_TipIndex = 0;
//デバック用フォント
NmFont g_debugFont;
//合成用モンスターベース
int G_mons_Bace;
//合成用モンスターパートナー
int G_mons_Pal;
//ベースモンスター選択フラグ
int BaceSFlag;
//パートナーモンスター選択フラグ
int PalSFlag;
//
int resultFlag;
//
int ketteiFlag;

int Gtool_gap;

int ErrorF;

Nm2DSurface kakunin1Surf;
Nm2DSurface kakunin2Surf;

//曲の設定
static NmMusic g_MainMusic;
int weit;
int giantF;
extern int monF[4][9];

//各種初期化
BOOL initGouseiScene(void){
	int i; 
	int j;
	ErrorF = 0;
	giantF = 0;
	//背景の初期化
	Nm2DInit(&GBackSurf);
	Nm2DOpen(&GBackSurf, "res/UnitBack.png", 1280, 720);
	Nm2DInit(&GBmenuSurf);
	Nm2DOpen(&GBmenuSurf, "res/Bmenu.png", 1280, 200);
	Nm2DInit(&GTWakuSurf);
	Nm2DOpen(&GTWakuSurf,"res/topwaku.png",1280,140,D3DCOLOR_RGBA(255,255,255,255));
	//モンスター画像の初期化
	Nm2DArrayInit(&GSlimeSurf);
	Nm2DArrayOpen(&GSlimeSurf, "res/MIcon.png", 160, 160,9,4, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DArrayInit(&NoMonSurf);
	Nm2DArrayOpen(&NoMonSurf, "res/MIcon.png", 160, 160,1,1, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&Waku1Surf);
	Nm2DOpen(&Waku1Surf, "res/waku.png", 160, 130, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&Waku2Surf);
	Nm2DOpen(&Waku2Surf, "res/waku.png", 160, 130, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&G_NowMonsterSurf);
	Nm2DOpen(&G_NowMonsterSurf, "res/MIcon.png", 160, 160, 8, 4, D3DCOLOR_RGBA(0, 255, 0, 0));
	//ツール枠の初期化
	Nm2DInit(&GwindouSurf);
	Nm2DCreateSurface(&GwindouSurf, 1280, 380, D3DCOLOR_RGBA(255, 255, 255, 128));
	Nm2DInit(&GRenternSurf);
	Nm2DOpen(&GRenternSurf, "res/modoru2.png", 150, 120);
	//カーソルの初期化
	Nm2DInit(&GColrsolSurf);
	Nm2DOpen(&GColrsolSurf, "res/crasol.png", 50, 50);
	//モンスターの顔画像
	Nm2DInit(&GMonstersFaceSurf1);
	Nm2DOpen(&GMonstersFaceSurf1, "res/kao.png", 145, 115, 5, 1, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&GMonstersFaceSurf2);
	Nm2DOpen(&GMonstersFaceSurf2, "res/kao.png", 145, 115, 5, 1, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&Gobulin_swords);
	Nm2DOpen(&Gobulin_swords, "res/mob/Sgoburin1.png", 56, 48, 1, 1, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&Gobulin_Sorcerer);
	Nm2DOpen(&Gobulin_Sorcerer, "res/mob/Mgobulin.png", 48, 72, 1, 1, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&Giant);
	Nm2DOpen(&Giant, "res/mob/Giant1.png", 80, 80, 1, 1, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&gWindowSurf);
	Nm2DOpen(&gWindowSurf, "res/window2.png", 1180, 620, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 100));
	Nm2DInit(&kakunin1Surf);
	Nm2DOpen(&kakunin1Surf, "res/YES.png", 136, 68, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 100));
	Nm2DInit(&kakunin2Surf);
	Nm2DOpen(&kakunin2Surf, "res/NO.png", 203, 70, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 100));
	Nm2DInit(&mobSel1);
	Nm2DOpen(&mobSel1, "res/mobsel(1).png", 1050, 77, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 100));
	Nm2DInit(&mobSel2);
	Nm2DOpen(&mobSel2, "res/mobsel(2).png", 913, 77, 1, 1, D3DCOLOR_RGBA(255, 255, 255, 100));

	//変数の初期化
	g_tool_w = 12880;
	g_tool_h = 720;
	g_tool_x = 0;
	g_tool_y = 120;
	GMapTipx = 0;
	GMapTipy = 0;
	//使用中確認フラグ
	G_MenbreFlag = 0;
	//デバックフォント
	NmFontInit(&g_debugFont);
	NmFontCreate(&g_debugFont, 48, "");
	//曲の初期化
	NmMusicInit(&g_MainMusic);
	NmMusicOpen(&g_MainMusic, "res/MOUSIC/MEIN/game_maoudamashii_7_event34.mid", 0);
	NmMusicPlay(&g_MainMusic);
	int resultFlag = 0;
	//UnitInit();
	getUnitLoad("res/UnitGetNow.dat");
	return TRUE;
}
//フレーム処理
void moveGouseiScene(DWORD dwTickDiff){
	
	//ひとつ前の画面に戻る
	if (GetMouseButtonLWait()){
		if (GetMouseX() > 1050 && GetMouseX()<1050 + 150 &&
			GetMouseY()>10 && GetMouseY() < 10 + 120){
			//	SaveFlag = 1;
			//	SaveTime = 60;
			getUnitSave("res/UnitGetNow.dat");
			changeScene(Gmenu[SCENE_GAME2]);
		}
	}
	//ユニットの選択
	if (GetMouseButtonRWait()){

		if (g_tool_x < GetMouseX() && GetMouseX() < g_tool_x + g_tool_w &&
			g_tool_y < GetMouseY() && GetMouseY() < g_tool_y + g_tool_h){
			G_TipIndex = (GetMouseX() - g_tool_x) / TIP_SIZE2 + (GetMouseY() - Gtool_gap - g_tool_y) / TIP_SIZE2 * 8;
			if (monF[(GetMouseY() - Gtool_gap - g_tool_y) / TIP_SIZE2][(GetMouseX() - g_tool_x) / TIP_SIZE2] == 0){
				G_TipIndex = 0;
			}
		}
	}
	//もう一度押してマップデータに入れる
	//ベースモンスター配置
	if (BaceSFlag == 0){
		if (G_TipIndex != 0 && GetMouseButtonLWait()
			&& GetMouseX()>300 && GetMouseX()<300 + 160 &&
			GetMouseY()>555 && GetMouseY()<555 + 130){
			//モンスター編成に置いたので、今持っているモンスターを消去（Index0）にする
			BaceSFlag = G_TipIndex;
			G_MenbreFlag = G_TipIndex;
			G_TipIndex = 0;
			
		}
	}

	//もう一度押してマップデータに入れる
	//パートナーモンスター配置
	if (BaceSFlag != 0 && PalSFlag == 0){
		if (G_MenbreFlag!=G_TipIndex){
			if (G_TipIndex != 0 && GetMouseButtonLWait()
				&& GetMouseX() > 800 && GetMouseX()<800 + 130 &&
				GetMouseY()>555 && GetMouseY() < 555 + 130){
				//モンスター編成に置いたので、今持っているモンスターを消去（Index0）にする
				PalSFlag = G_TipIndex;
				G_TipIndex = 0;
			}
		}
	}
	if (BaceSFlag != 0 && PalSFlag != 0){
		if (GetMouseButtonLWait()&& GetMouseX()>300 && GetMouseX()<300 + 136 &&
			GetMouseY()>200 && GetMouseY()<330 + 68){
			ketteiFlag = 1;
		}
		if (GetMouseButtonLWait()
			&& GetMouseX() > 800 && GetMouseX()<800 + 203 &&
			GetMouseY()>200 && GetMouseY() < 330 + 70){
			BaceSFlag = 0;
			PalSFlag = 0;
		}
	}
	if (BaceSFlag != 0 && PalSFlag != 0 && ketteiFlag == 1){
		resultFlag = Gouseiresult(BaceSFlag, PalSFlag);
	}
	if (GetMouseY() >= 500 && 520 >= GetMouseY()){
		Gtool_gap -= 6;
	}
	else if (GetMouseY() <= 140 && Gtool_gap != 0){
		Gtool_gap += 6;
	}
	
}
//レンダリング処理
void renderGouseiScene(void){
	int i;
	int j;
	Nm2DRender(&GBackSurf);
	Nm2DSet(&GwindouSurf, 0, 140);
	Nm2DRender(&GwindouSurf);
	for (i = 0; i < 4; i++){
		for (j = 0; j < 9; j++){
			Nm2DArrayAddSurface(&GSlimeSurf, j*TIP_SIZE2, ((i + 1)*TIP_SIZE2));
			Nm2DArraySet(&GSlimeSurf, j + (i * 9), j*TIP_SIZE2, ((i + 1)*TIP_SIZE2 + Gtool_gap));
			Nm2DArraySetIndex(&GSlimeSurf, j + (i * 9), j + (i * 9));
			if (monF[i][j] == 0){
				Nm2DArraySetIndex(&GSlimeSurf, j + (i * 9), 0);
			}
		}
	}
	Nm2DArrayRender(&GSlimeSurf);
	Nm2DSet(&GBmenuSurf, 0, 520);
	Nm2DRender(&GBmenuSurf);
	Nm2DSet(&GMonstersFaceSurf1, 313, 563);
	Nm2DSetIndex(&GMonstersFaceSurf1, BaceSFlag);
	Nm2DRender(&GMonstersFaceSurf1);
	Nm2DSet(&GMonstersFaceSurf2, 813, 563);
	Nm2DSetIndex(&GMonstersFaceSurf2, PalSFlag);
	Nm2DRender(&GMonstersFaceSurf2);
	Nm2DSet(&Waku1Surf, 300, 555);
	Nm2DRender(&Waku1Surf);
	Nm2DSet(&Waku2Surf, 800, 555);
	Nm2DRender(&Waku2Surf);
	Nm2DSet(&G_NowMonsterSurf, GetMouseX(), GetMouseY());
	Nm2DSetIndex(&G_NowMonsterSurf, G_TipIndex);
	Nm2DRender(&G_NowMonsterSurf);
	if (BaceSFlag != 0 && PalSFlag != 0){
		Nm2DSet(&kakunin1Surf, 300, 330);
		Nm2DRender(&kakunin1Surf);
		Nm2DSet(&kakunin2Surf, 800, 330);
		Nm2DRender(&kakunin2Surf);
	}
	if (ketteiFlag == 1){
		Nm2DSet(&gWindowSurf, 50, 50);
		Nm2DRender(&gWindowSurf);
		ResultRender();
	}
	Nm2DRender(&GTWakuSurf);
	Nm2DSet(&GRenternSurf, 1050, 10);
	Nm2DRender(&GRenternSurf);
	Nm2DSet(&GColrsolSurf, GetMouseX(), GetMouseY());
	Nm2DRender(&GColrsolSurf);

	

}
//シーン終了時の処理
void releaseGouseiScene(void){
	BaceSFlag = 0;
	PalSFlag = 0;
	G_MenbreFlag = 0;
	G_TipIndex = 0;
	resultFlag = 0;
	ketteiFlag = 0;
	Nm2DDeleteSurface(&GBackSurf);
	Nm2DDeleteSurface(&GRenternSurf);
	Nm2DDeleteSurface(&GBmenuSurf);
	Nm2DDeleteSurface(&GTWakuSurf);
	Nm2DArrayDeleteSurface(&GSlimeSurf);
	Nm2DDeleteSurface(&G_NowMonsterSurf);
	Nm2DDeleteSurface(&GColrsolSurf);
	NmMusicDelete(&g_MainMusic);
	
}
//あたり判定
void GouseiSceneCollideCallback(int nSrc, int nTarget, int nCollideID){

}

int Gouseiresult(int bace, int pal){
	switch (bace)
	{
	case Slime:
		switch (pal)
		{
		case giant:
			monF[2][1] = 1;
			return Daemon;
			break;
		default:
			ErrorF = 1;
			break;
		}
		break;
	case goblin:
		switch (pal)
		{
		case golem:
			monF[1][3] = 1;
			return giant;
			break;
		case wolf:
			monF[0][5] = 1;
			return goblinR;
			break;
		default:
			ErrorF = 1;
			break;
		}
		break;
	case goblinM:
		switch (pal){
		case Sherman:
			monF[3][4] = 1;
			break;
		case skeleton:
			monF[3][1] = 1;
			break;
		case witch:
			monF[2][5] = 1;
		default:
			break;
		}
	case goblinS:
		switch (pal){
		case giant:
			monF[2][4] = 1;
			break;
		case Sherman:
			monF[2][6] = 1;
			break;
		case zombie:
			monF[3][7] = 1;
			break;
		default:
			break;
		}
		break;
	case wolf:
		switch (pal){
		case goblin:
			monF[0][5] = 1;
			break;
		case golem:
			monF[1][6] = 1;
			break;
		case Daemon:
			monF[1][5] = 1;
			break;
		default:
			break;
		}
		break;
	case golem:
		switch (pal)
		{
		case giant:
			monF[0][7] = 1;
			break;
		case Cerberus:
			monF[2][2] = 1;
			break;
		case skeleton:
			monF[3][2] = 1;
			break;
		default:
			ErrorF = 1;
			break;
		}
		break;
	case Sherman:
		switch (pal){
		case Daemon:
			monF[3][5] = 1;
			break;
		default:
			break;
		}
		break;
	case witch:
		switch (pal)
		{
		case skeleton:
			monF[3][5] = 1;
			break;
		case zombie:
			monF[3][5] = 1;
			break;
		case Daemon:
			monF[3][5] = 1;
			break;
		default:
			break;
		}
		break; 
	case orthros:
		switch (pal)
		{
		case Daemon:
			monF[1][2] = 1;
			break;
		case golem:
			monF[2][1] = 1;
			break;
		default:
			break;
		}
		break;
	case Cerberus:
		switch (pal)
		{
		case behemoth:
			monF[2][2] = 1;
			break;
		case baihu:
			monF[1][2] = 1;
			break;
		default:
			break;
		}
		break;
	case Tdragon:
		switch (pal)
		{
		case baihu:
			monF[1][1] = 1;
			break;
		default:
			break;
		}
	}
}

void ResultRender(){
	switch (resultFlag)
	{
	case goblinM:
		Nm2DSetScale(&Gobulin_Sorcerer, 5);
		Nm2DSet(&Gobulin_Sorcerer, 592, 248);
		Nm2DRender(&Gobulin_Sorcerer);
		break;
	case goblinS:
		Nm2DSetScale(&Gobulin_swords, 5);
		Nm2DSet(&Gobulin_swords, 596, 288);
		Nm2DRender(&Gobulin_swords);
		break;
	case giant:
		Nm2DSetScale(&Giant, 4);
		Nm2DSet(&Giant, 400, 200);
		Nm2DRender(&Giant);
		break;
	default:
		break;
	}
}