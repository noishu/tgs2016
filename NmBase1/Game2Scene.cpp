#include "NmBase1.h"
#include "GameMain.h"
#include "Game2Scene.h"
#include "Drop.h"

//アニメーション関連の変数
int Sanimation(Nm2DSurface* surf, int end, int reverse);
#define BIAS 10
int SaCnt, SgCnt;
//独自の変数
extern NmInput input;
extern int DropF;
//メニュー背景
static Nm2DSurface menu3BackSurf;
//メニュー項目
static Nm2DSurface BATOL1Surf;
static Nm2DSurface return1Surf;
static Nm2DSurface MapSurf;
static Nm2DSurface BwakuSurf;
//確認用項目
static Nm2DSurface chekSurf01;
static Nm2DSurface chekSurf02;
static Nm2DSurface chekSurf03;
static Nm2DSurface chekSurf04;
static Nm2DSurface chekSurf05;
static Nm2DSurface chekSurf06;
static Nm2DSurface chekSurf07;
static Nm2DSurface chekSurf08;
static Nm2DSurface chekSurf09;
static Nm2DSurface chekSurf10;
static Nm2DSurface chekSurf11;
static Nm2DSurface chekSurf12;
static Nm2DSurface chekSurf13;
static Nm2DSurface chekSurf14;
static Nm2DSurface chekSurf15;
static Nm2DSurface chekSurf16;
static Nm2DSurface chekSurf17;
static Nm2DSurface chekSurf18;
//ステージ選択用矢印
static Nm2DSurface scerctSurf1;
static Nm2DSurface scerctSurf2;
static Nm2DSurface scerctSurf3;
static Nm2DSurface scerctSurf4;
static Nm2DSurface scerctSurf5;
static Nm2DSurface scerctSurf6;
static Nm2DSurface scerctSurf7;
static Nm2DSurface scerctSurf8;
static Nm2DSurface scerctSurf9;
static Nm2DSurface scerctSurf10;
static Nm2DSurface scerctSurf11;
static Nm2DSurface scerctSurf12;
static Nm2DSurface scerctSurf13;
static Nm2DSurface scerctSurf14;
static Nm2DSurface scerctSurf15;
static Nm2DSurface scerctSurf16;
static Nm2DSurface scerctSurf17;
static Nm2DSurface scerctSurf18;
//カーソル
static Nm2DSurface corasol3Surf;
#define MENU_MAX 5
SCENE_NO menu3[4] = { SCENE_GAME1,SCENE_GAME1,SCENE_GAME3 ,};
static NmMusic MainMusic;
// シーン開始前の初期化を行う
BOOL initGame2Scene(void){
	Nm2DInit(&menu3BackSurf);
	Nm2DOpen(&menu3BackSurf, "res/haikei.png", 1280, 720);
	//メニュー項目
	Nm2DInit(&BATOL1Surf);
	Nm2DOpen(&BATOL1Surf, "res/BATOLMenu.png", 1200, 200, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&return1Surf);
	Nm2DOpen(&return1Surf, "res/modoru2.png", 150, 120, D3DCOLOR_RGBA(0, 255, 0, 0));
	Nm2DInit(&BwakuSurf);
	Nm2DOpen(&BwakuSurf, "res/Bwaku.png", 1280, 140, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&MapSurf);
	Nm2DOpen(&MapSurf, "res/MAP.png", 1000, 600, D3DCOLOR_RGBA(255, 255, 255, 255));
	//確認画面
	Nm2DInit(&chekSurf01);
	Nm2DOpen(&chekSurf01, "res/Stage/stage1.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf02);
	Nm2DOpen(&chekSurf02, "res/Stage/stage2.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf03);
	Nm2DOpen(&chekSurf03, "res/Stage/stage3.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf04);
	Nm2DOpen(&chekSurf04, "res/Stage/stage4.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf05);
	Nm2DOpen(&chekSurf05, "res/Stage/stage5.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf06);
	Nm2DOpen(&chekSurf06, "res/Stage/stage6.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf07);
	Nm2DOpen(&chekSurf07, "res/Stage/stage7.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf08);
	Nm2DOpen(&chekSurf08, "res/Stage/stage8.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf09);
	Nm2DOpen(&chekSurf09, "res/Stage/stage9.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf10);
	Nm2DOpen(&chekSurf10, "res/Stage/stage10.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf11);
	Nm2DOpen(&chekSurf11, "res/Stage/stage11.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf12);
	Nm2DOpen(&chekSurf12, "res/Stage/stage12.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf13);
	Nm2DOpen(&chekSurf13, "res/Stage/stage13.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf14);
	Nm2DOpen(&chekSurf14, "res/Stage/stage14.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf15);
	Nm2DOpen(&chekSurf15, "res/Stage/stage15.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf15);
	Nm2DOpen(&chekSurf15, "res/Stage/stage15.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf16);
	Nm2DOpen(&chekSurf16, "res/Stage/stage16.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf17);
	Nm2DOpen(&chekSurf17, "res/Stage/stage17.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&chekSurf18);
	Nm2DOpen(&chekSurf18, "res/Stage/stage18.png", 600, 300, D3DCOLOR_RGBA(255, 255, 255, 255));
	//矢印
	Nm2DInit(&scerctSurf1);
	Nm2DOpen(&scerctSurf1, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf2);
	Nm2DOpen(&scerctSurf2, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf3);
	Nm2DOpen(&scerctSurf3, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf4);
	Nm2DOpen(&scerctSurf4, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf5);
	Nm2DOpen(&scerctSurf5, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf6);
	Nm2DOpen(&scerctSurf6, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf7);
	Nm2DOpen(&scerctSurf7, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf8);
	Nm2DOpen(&scerctSurf8, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf9);
	Nm2DOpen(&scerctSurf9, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf10);
	Nm2DOpen(&scerctSurf10, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf11);
	Nm2DOpen(&scerctSurf11, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf12);
	Nm2DOpen(&scerctSurf12, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf13);
	Nm2DOpen(&scerctSurf13, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf14);
	Nm2DOpen(&scerctSurf14, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf15);
	Nm2DOpen(&scerctSurf15, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf16);
	Nm2DOpen(&scerctSurf16, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf17);
	Nm2DOpen(&scerctSurf17, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));
	Nm2DInit(&scerctSurf18);
	Nm2DOpen(&scerctSurf18, "res/SIKON.png", 60, 70, 9, 1, D3DCOLOR_RGBA(255, 255, 255, 255));

	//カーソル
	Nm2DInit(&corasol3Surf);
	Nm2DOpen(&corasol3Surf, "res/crasol.png", 50, 50, D3DCOLOR_RGBA(0, 255, 0, 0));
	NmMusicInit(&MainMusic);
	NmMusicOpen(&MainMusic,"res/MOUSIC/MEIN/game_maoudamashii_7_event34.mid",0);
	NmMusicCont(&MainMusic);

	return TRUE;
}
//	フレーム処理
void moveGame2Scene(DWORD dwTickDiff){
	//シーン移動
	if (GetMouseButtonLWait()){
		if (GetMouseX() > 235 && GetMouseX()<235 + 60 &&
		GetMouseY()>365 && GetMouseY() < 365 + 70){
			DropF = 1;
			changeScene(menu3[SCENE_GAME3]);
	}
		if (GetMouseButtonLWait()){
			if (GetMouseX()>415 && GetMouseX()<415 + 60 &&
				GetMouseY()>215 && GetMouseY()<215 + 70){
				DropF = 2;
				changeScene(menu3[SCENE_GAME3]);
			}
		}
		if (GetMouseButtonLWait()){
			if (GetMouseX()>325 && GetMouseX()<325 + 60 &&
				GetMouseY()>465 && GetMouseY()<465 + 70){
				DropF = 3;
				changeScene(menu3[SCENE_GAME3]);
			}
		}
		if (GetMouseX()>1050 && GetMouseX()<1050 + 150 &&
			GetMouseY()>10 && GetMouseY() < 10 + 120){
			changeScene(menu3[SCENE_GAME1]);
		}
	}
	//アニメーションを動かす
	if (SgCnt++>5){
		SgCnt = 0;
		SaCnt++;
	}
	Nm2DSetIndex(&scerctSurf1, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf2, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf3, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf4, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf5, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf6, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf7, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf8, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf9, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf10, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf11, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf12, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf13, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf14, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf15, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf16, BIAS + SaCnt);
	Nm2DSetIndex(&scerctSurf17, BIAS + SaCnt);
	//Nm2DSetIndex(&scerctSurf18, BIAS + SaCnt);

}
//	レンダリング処理
void renderGame2Scene(void){
	//メニュー背景の表示
	Nm2DRender(&menu3BackSurf);
	Nm2DRender(&BwakuSurf);
	Nm2DSet(&MapSurf, 80, 120);
	Nm2DRender(&MapSurf);
	//矢印
	Nm2DSet(&scerctSurf1, 235, 365);
	Nm2DRender(&scerctSurf1);
	Nm2DSet(&scerctSurf2, 415, 215);
	Nm2DRender(&scerctSurf2);
	Nm2DSet(&scerctSurf3, 325, 465);
	Nm2DRender(&scerctSurf3);
	Nm2DSet(&scerctSurf4, 372, 362);
	//Nm2DRender(&scerctSurf4);
	Nm2DSet(&scerctSurf5, 450, 330);
	//Nm2DRender(&scerctSurf5);
	Nm2DSet(&scerctSurf6, 548, 330);
	//Nm2DRender(&scerctSurf6);
	Nm2DSet(&scerctSurf7, 335, 180);
	//Nm2DRender(&scerctSurf7);
	Nm2DSet(&scerctSurf8, 593, 380);
	//Nm2DRender(&scerctSurf8);
	Nm2DSet(&scerctSurf9, 615, 295);
	//Nm2DRender(&scerctSurf9);
	Nm2DSet(&scerctSurf10, 580, 245);
	//Nm2DRender(&scerctSurf10);
	Nm2DSet(&scerctSurf11, 630, 445);
	//Nm2DRender(&scerctSurf11);
	Nm2DSet(&scerctSurf12, 745, 410);
	//Nm2DRender(&scerctSurf12);
	Nm2DSet(&scerctSurf13, 950, 250);
	//Nm2DRender(&scerctSurf13);
	Nm2DSet(&scerctSurf14, 840, 170);
	//Nm2DRender(&scerctSurf14);
	Nm2DSet(&scerctSurf15, 730, 130);
	//Nm2DRender(&scerctSurf15);
	Nm2DSet(&scerctSurf16, 725, 300);
	//Nm2DRender(&scerctSurf16);
	Nm2DSet(&scerctSurf17, 773, 250);
	//Nm2DRender(&scerctSurf17);
	Nm2DSet(&scerctSurf18, 375, 530);
	//Nm2DRender(&scerctSurf18);


	//メニューの表示
	//Nm2DSet(&BATOL1Surf, 50, 150);
	//Nm2DRender(&BATOL1Surf);
	//確認画面の表示
		if (GetMouseX() > 235 && GetMouseX()<235 + 60 &&
			GetMouseY()>365 && GetMouseY() < 365 + 70){
			Nm2DSet(&chekSurf01, 340, 150);
			Nm2DRender(&chekSurf01);
		}
		if (GetMouseX() > 415 && GetMouseX()<415 + 60 &&
			GetMouseY()>215 && GetMouseY()<215 + 70){
			Nm2DSet(&chekSurf02, 340, 150);
			Nm2DRender(&chekSurf02);
		}
		if (GetMouseX()>325 && GetMouseX()<325 + 60 &&
			GetMouseY()>465 && GetMouseY() < 465 + 70){
			Nm2DSet(&chekSurf03, 340, 150);
			Nm2DRender(&chekSurf03);
		}
	//戻るボタンの表示
	Nm2DSet(&return1Surf, 1050, 10);
	Nm2DRender(&return1Surf);
	Nm2DSet(&corasol3Surf, GetMouseX(), GetMouseY());
	Nm2DRender(&corasol3Surf);


}

//	シーン終了時の後処理
void releaseGame2Scene(void){
	Nm2DDeleteSurface(&menu3BackSurf);
	Nm2DDeleteSurface(&BATOL1Surf);
	Nm2DDeleteSurface(&return1Surf);
	Nm2DDeleteSurface(&corasol3Surf);
	NmMusicDelete(&MainMusic);
	//確認画面
	Nm2DDeleteSurface(&chekSurf01);
	Nm2DDeleteSurface(&chekSurf02);
	Nm2DDeleteSurface(&chekSurf03);
	Nm2DDeleteSurface(&chekSurf04);
	Nm2DDeleteSurface(&chekSurf05);
	Nm2DDeleteSurface(&chekSurf06);
	Nm2DDeleteSurface(&chekSurf07);
	Nm2DDeleteSurface(&chekSurf08);
	Nm2DDeleteSurface(&chekSurf09);
	Nm2DDeleteSurface(&chekSurf10);
	Nm2DDeleteSurface(&chekSurf11);
	Nm2DDeleteSurface(&chekSurf12);
	Nm2DDeleteSurface(&chekSurf13);
	Nm2DDeleteSurface(&chekSurf14);
	Nm2DDeleteSurface(&chekSurf15);
	Nm2DDeleteSurface(&chekSurf16);
	Nm2DDeleteSurface(&chekSurf17);
	Nm2DDeleteSurface(&chekSurf18);
	//カーソル
	Nm2DDeleteSurface(&scerctSurf1);
	Nm2DDeleteSurface(&scerctSurf2);
	Nm2DDeleteSurface(&scerctSurf3);
	Nm2DDeleteSurface(&scerctSurf4);
	Nm2DDeleteSurface(&scerctSurf5);
	Nm2DDeleteSurface(&scerctSurf6);
	Nm2DDeleteSurface(&scerctSurf7);
	Nm2DDeleteSurface(&scerctSurf8);
	Nm2DDeleteSurface(&scerctSurf9);
	Nm2DDeleteSurface(&scerctSurf10);
	Nm2DDeleteSurface(&scerctSurf11);
	Nm2DDeleteSurface(&scerctSurf12);
	Nm2DDeleteSurface(&scerctSurf13);
	Nm2DDeleteSurface(&scerctSurf14);
	Nm2DDeleteSurface(&scerctSurf15);
	Nm2DDeleteSurface(&scerctSurf16);
	Nm2DDeleteSurface(&scerctSurf17);
	Nm2DDeleteSurface(&scerctSurf18);

}

// 当り判定コールバック 　　　ここでは要素を削除しないこと！！
void  Game2SceneCollideCallback( int nSrc, int nTarget, int nCollideID ){
	if (GetMouseButtonLWait()){
		if (GetMouseX() > 100 && GetMouseX()<100 + 400 &&
			GetMouseY()>100 && GetMouseY() < 100 + 200){
			changeScene(menu3[SCENE_GAME3]);
		}
		if (GetMouseX()>800 && GetMouseX()<800 + 150 &&
			GetMouseY()>10 && GetMouseY() < 10 + 100){
			changeScene(menu3[SCENE_GAME1]);
		}

	}


}
//アニメーション関数
int Sanimation(Nm2DSurface* surf, int end, int reverse){
	//インデックスを加算
	Nm2DSetIndex(surf, Nm2DGetIndex(surf) + 1);
	//エンドに指定したIndex番号に来たらループ先に行く
	if (Nm2DGetIndex(surf) > end){
		Nm2DSetIndex(surf, reverse);
	}
	return Nm2DGetIndex(surf);
}
